import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, AsyncStorage, KeyboardAvoidingView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import config from '../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import { useForm, Controller } from 'react-hook-form';
//loader
import Loader from '../loader/loader';
import { useHeaderHeight } from '@react-navigation/stack';

export default function ConfirmSmsScreen({ route }) {

    const { _id } = route.params;

    console.log('recibiendo el id', _id);

    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    navigation.setOptions({
        title: 'Codigo de Validación',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    })

    const [refreshing , setRefreshing ] = useState(true);
    const [isShow , setisShow ] = useState(false);
    const [user , setUser ] = useState('');

    useEffect(() => {
        
    }, []);


    //Guardar
    const onSubmit = data => {
        
        //Si la validacion falla data debe ser nulo
        if (data.code) {
            console.log(JSON.stringify(data.code))
            //Mostrar velo de loading
            setisShow(true);

            const url = config.BASE_URL_API+'api/v1/verify-code';

            console.log('validando codigo', data.code);
            
            axios.post(url, {
                userId: _id,
                verificationCode: data.code
            },
            {
                
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    navigation.navigate('Account');
                    //Mostrar mensaje de perfil
                    let userData = response.data.user;
                    AsyncStorage.setItem('userData', JSON.stringify(userData));

                    Alert.alert('Codigo validado correctamente')

                }else{
                    //Error
                    Alert.alert('Error validando, intente nuevamente')
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Código incorrecto, intente nuevamente')
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
        value: args[0].nativeEvent.text,
        };
    };

    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding"
        keyboardVerticalOffset={
            Platform.OS === "ios" ? headerHeight : (headerHeight)
        }>
        <Loader loading={isShow} />
        <Text>Codigo</Text>
        <Controller
            as={<TextInput style={styles.input}/>}
            control={control}
            name="code"
            onChange={onChange}
            rules={{ required: 'Ingrese el numero de telefono' }}
            defaultValue=""
        />
        {errors.code && <Text style={styles.error}>{errors.names.message}</Text>}
        <View style={styles.bottom}>
            <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                <Text style={styles.buttonText}>Validar</Text>
            </TouchableOpacity>
        </View>
        </KeyboardAvoidingView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    }
});

