import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Alert, KeyboardAvoidingView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import config from '../../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import colors from '../../../assets/styles/colors';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form'
//loader
import Loader from '../../loader/loader';
import { useHeaderHeight } from '@react-navigation/stack';

export default function Phone() {
    
    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/update-password/';

    navigation.setOptions({
        title: 'Contraseña',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false);
    const [user , setUser ] = useState('');

    useEffect(() => {
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        // Unmount
        return focused;

    }, []);
    
    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    //Guardar
    const onSubmit = data => {
       
        //Si la validacion falla data debe ser nulo
        if (data) { 
            //Mostrar velo de loading
            setisShow(true);
            let token = user.token;

            axios.post(url, {
                oldPassword: data.password,
                password: data.userpassword,
                confirmPassword: data.reenterpassword,
                id: user._id
            },{
                headers: {
                    'token': token,
                }
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);
                if(response.data.status){
                    navigation.navigate('Settings');
                }else{
                   
                    if(response.data.code == 1 || response.data.code == 2){
                        Alert.alert('Alguno de los datos son incorrectos, verifique e intente nuevamente');
                    }else{
                        Alert.alert('Error actualizando, intente nuevamente');
                    }
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, errors, handleSubmit, watch } = useForm({});
  
    const onChange = args => {
        return { value: args[0].nativeEvent.text };
    };

    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding"
            keyboardVerticalOffset={
                Platform.OS === "ios" ? headerHeight : (headerHeight)
            }>
            <Loader loading={isShow} />
            <Text style={styles.label}>Contraseña Anterior</Text>
            <Controller
                as={<TextInput style={styles.input} secureTextEntry={true}/>}
                control={control}
                name="password"
                onChange={onChange}
                rules={{ 
                    required: "Ingrese contraseña", 
                    minLength: {
                        value: 6,
                        message: "El contraseña debe tener al menos 6 caracteres"
                    }
                }} 
                defaultValue=""
            />
            {errors.password && <Text style={styles.error}>{errors.password.message}</Text>}
            <Text style={styles.label}>Contraseña Nueva</Text>
            <Controller
                as={<TextInput style={styles.input} secureTextEntry={true}/>}
                control={control}
                name="userpassword"
                onChange={onChange}
                rules={{ 
                    required: "Ingrese contraseña nueva", 
                    minLength: {
                        value: 6,
                        message: "El contraseña debe tener al menos 6 caracteres"
                    }
                }} 
                defaultValue=""
            />
            {errors.userpassword && <Text style={styles.error}>{errors.userpassword.message}</Text>}
            <Text style={styles.label}>Confirmar contraseña</Text>
            <Controller
                as={<TextInput style={styles.input} secureTextEntry={true}/>}
                control={control}
                name="reenterpassword"
                onChange={onChange}
                rules={{ 
                    required: "Ingrese confirmación de contraseña", 
                    minLength: {
                        value: 6,
                        message: "El contraseña debe tener al menos 6 caracteres"
                    },
                    validate : (value) => value === watch('userpassword') || "Las contraseñas no coinciden"
                }} 
                defaultValue=""
            />
            {errors.reenterpassword && <Text style={styles.error}>{errors.reenterpassword.message}</Text>}
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Guardar</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    },
    label:{
        color:colors.TEXT,
        marginTop:10,
        fontWeight:'bold',
        fontSize:14,
    }
});
