import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import config from '../../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import colors from '../../../assets/styles/colors';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form';
import PhoneInput from 'react-phone-number-input';
//loader
import Loader from '../../loader/loader';

// import 'react-phone-number-input/style.css';

export default function PhoneScreen2() {
    
    const navigation = useNavigation();

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/user-update/';

    navigation.setOptions({
        title: 'Teléfono',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false);
    const [user , setUser ] = useState('');
    const [saved , setSaved ] = useState(false);


    useEffect(() => {
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        //actualizar storage si guardo el nombre
        if(saved) {
            updateAsyncStorage();
        }

        // Unmount
        return focused;

    }, [saved]);
    
    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
        
        let userfield = JSON.parse(userData);

        //colocar dato en el input
        setValue('phone',userfield.phone);
    };
    
    //Actualizar storage con los datos actualizados
    const updateAsyncStorage = async () => {

        AsyncStorage.setItem('userData', JSON.stringify(user)).then(()=>{
            navigation.navigate('Settings');
        }).catch((error)=>{
            console.log(error);
        });
    }

    //Guardar
    const onSubmit = data => {
        //Si la validacion falla data debe ser nulo
        if (data) { 
            //Mostrar velo de loading
            setisShow(true);

            let urlUpdate = `${url}${user._id}/phone`;
            let token = user.token

            axios.put(urlUpdate, {
                phone: data.phone,
            },
            {
                headers: {
                    'token': token,
                }
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    let newUserState = user;
                    //actualizar nombre en objeto
                    newUserState.phone = data.phone;
                    setSaved(true);
 
                }else{
                    //Error
                    Alert.alert('Error actualizando, intente nuevamente')
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    };

    return (
        <View style={styles.container}>
            <Loader loading={isShow} />
            <Text>Teléfono</Text>
            <Controller
                as={<TextInput style={styles.input}/>}
                control={control}
                name="phone"
                onChange={onChange}
                rules={{ required: 'Ingrese teléfono' }}
                defaultValue=""
            />
            {errors.phone && <Text style={styles.error}>{errors.phone.message}</Text>}
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Guardar</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    }
});
