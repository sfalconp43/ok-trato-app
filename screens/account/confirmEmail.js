import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, AsyncStorage, KeyboardAvoidingView} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const colors = require('../../assets/styles/colors');
import config from '../../config/config';
import { useForm, Controller } from 'react-hook-form'
//loader
import Loader from '../loader/loader';
import axios from 'axios';
import { useHeaderHeight } from '@react-navigation/stack';


export default function AccountScreen({ route }) {

    const { _id } = route.params;
    const { email } = route.params;
    const { names } = route.params;

    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    navigation.setOptions({
        title: 'Confirmar Email',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false);
    const [saved , setSaved ] = useState(false);
    const [user , setUser ] = useState('');


    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        //actualizar storage si guardo el nombre
        if(saved) {
            updateAsyncStorage();
        }

        // //colocar dato en el input
        // setValue('email',email);

        // Unmount
        return focused
    
        
    }, [saved]);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
        
        let userfield = JSON.parse(userData);

        //colocar dato en el input
        setValue('email',userfield.email);
    };
    
    //Actualizar storage con los datos actualizados
    const updateAsyncStorage = async () => {

        AsyncStorage.setItem('userData', JSON.stringify(user)).then(()=>{
            navigation.navigate('Account');
        }).catch((error)=>{
            console.log(error);
        });
    }


    const onSubmit = data => {

        //Si la validacion falla data debe ser nulo
        if (data) {
            //Mostrar velo de loading
            setisShow(true);
            
            //construir ruta de servidor
            const url = config.BASE_URL_API+'api/v1/send-confirm-email/'+_id;

            axios.post(url)
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    Alert.alert('Se ha enviado el correo de confirmacion')
                    navigation.navigate('Account');
                    setSaved(true);
                }else{
                    Alert.alert('Error enviando confiración, intente nuevamente')
                } 
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    };

    return (
        <KeyboardAvoidingView style={styles.container}
        keyboardVerticalOffset={
            Platform.OS === "ios" ? headerHeight : (headerHeight)
        }>
            <Loader loading={isShow} />
            <Text>Email</Text>
            <Controller
                as={<TextInput style={styles.input}/>}
                control={control}
                name="email"
                onChange={onChange}
                rules={{ required: 'Ingrese email' }}
                defaultValue=""
            />
            {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Enviar</Text>
                </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    },
});
