import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Alert, TouchableOpacity, Image } from 'react-native';
//loader
import Loader from '../loader/loader';
import config from '../../config/config';
import colors from '../../assets/styles/colors';

export default function confirmPic({ route, navigation }) {

    //obtener parametros
    const { image } = route.params;
    const { profileOrCover } = route.params;

    navigation.setOptions({
        title: 'Confirmar',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    //url actualizar imagen de perfil
    const urlProfile = config.BASE_URL_API+'api/v1/user-update-image/';
    //url actualizar imagen de portada
    const urlCover = config.BASE_URL_API+'api/v1/user-update-cover/';

    const [user , setUser ] = useState('');
    const [isShow , setisShow ] = useState(false);
    //1 -> Foto de perfil. 2 -> Foto de portada
    const [type , setType ] = useState(0);
    const [saved , setSaved ] = useState(false);
    
    useEffect(() => {
        getStorage();
        setType(profileOrCover);

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        //actualizar storage si guardo el nombre
        if(saved) {
            //updateAsyncStorage();
        }

        // Unmount
        return focused;

    }, []);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    //guardar
    const saveImage = async () => {

        //Mostrar velo de loading
        setisShow(true);

		var data = new FormData();
        let filename = image.split("/").pop(); 
        data.append('userimage',{uri:image, name: filename, type:'image/jpg'});

        let urlUpdate;
        //construir url de acuerdo al tipo de actualizacion
        if(type == 1){
            urlUpdate = urlProfile+user._id+'/1';
        }else{
            urlUpdate = urlCover+user._id+'/2';
        }

        let token = user.token;

        fetch(urlUpdate, {
            method: 'POST',
            body: data,
            headers: new Headers({
                'token': token, 
            }), 
        })
        .then((response) => response.json())
        .then((data) => {
            //Ocultar velo de loading
            setisShow(false);

            if (data.status) {
                let newUserState = user;
                //actualizar nombre en objeto

                if(type == 1){
                    newUserState.image = data.image;
                }else{
                    newUserState.banner = data.image;
                }
                
                //Actualizar storage con los datos actualizados y regresar
                AsyncStorage.setItem('userData', JSON.stringify(newUserState));
                navigation.navigate('Account');

            } else {
                Alert.alert(data.message);
                navigation.navigate('Account');
            }

        })
        .catch((error) => {
            //Mostrar velo de loading
            setisShow(false);
            console.log(error.response);
        }); 
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Loader loading={isShow} />
				{/* Foto Portada */}
				<View style={{ flex: 0.3 }} >
					<View style={{height:150,backgroundColor: colors.PRIMARY ,alignItems: 'center', justifyContent: 'center'}}>
                    {type == 2 ?
                                <Image source={{ uri: image }} style={{ height: '100%', width:'100%' }} />:
                                <Image source={{ uri: config.BASE_URL_API+'covers/'+ user.banner }} style={{ height: '100%', width:'100%' }} />} 
                        
                        <View style={{ elevation: 7, width: 140, height: 140, borderRadius: 70,  backgroundColor: 'white', position: 'absolute', top: 90,
							flexDirection:'row', alignItems:'center',justifyContent: 'center'
						}}>
							{/* Imagen de perfil */}
							{type == 1 ?
                                <Image source={{ uri: image }} style={{ width: 130, height: 130, borderRadius: 130/2 }} />:
                                <Image source={{ uri: config.BASE_URL_API+'users/'+ user.image }} style={{ width: 130, height: 130, borderRadius: 130/2 }} />} 	
						</View>
					</View>
				</View>

				{/* Botones */}
				<View style={{  flex:0.3 }} >

                    <View style={styles.container}>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button} onPress={()=>navigation.goBack()}>
                                <Text style={styles.buttonText}>CANCELAR</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button} onPress={saveImage}>
                                <Text style={styles.buttonText}>GUARDAR</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
				</View>

			</View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
        padding:20
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
});
