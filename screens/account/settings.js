import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const colors = require('../../assets/styles/colors');
import AuthContext from '../authContext'
import Loader from '../loader/loader';
import { TextInput } from 'react-native-gesture-handler';
import {  MaterialCommunityIcons } from '@expo/vector-icons';

export default function AccountScreen() {
    const navigation = useNavigation();

    const { signOut } = useContext(AuthContext);

    navigation.setOptions({
        title: 'Configuración',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false)
    const [user , setUser ] = useState('');

    useEffect(() => {
        getStorage();
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        // Unmount
        return focused;

    }, []);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };
    
    //Cerrar sesion
	const Logout = async () => {
        setisShow(true);
        await AsyncStorage.removeItem('userData');
        await AsyncStorage.removeItem('userToken');
        setisShow(false);
        signOut();
	}

    return (
        <View style={styles.container} >
            <Loader loading={isShow} />
            <Text style= {styles.data}>DATOS</Text>

            <View style={styles.dataContainer}>
                <View style={{flex:1}}>
                    <Text style = {styles.label}>Nombres</Text>
                    <Text numberOfLines={1} style = {styles.fieldText}>{user.names} </Text>
                </View>
                <View style={styles.editButton}>
                    <TouchableOpacity style={styles.editStyle} onPress={() => { navigation.navigate('Names')}}>
                        <MaterialCommunityIcons name="pencil-circle" color={colors.PURPLE_BTN} size={35}/>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.dataContainer}>
                <View style={{flex:1}}>
                    <Text style = {styles.label}>Apellidos</Text>
                    <Text numberOfLines={1} style = {styles.fieldText}>{user.lastnames}</Text>
                </View>
                <View style={styles.editButton}>
                    <TouchableOpacity style = {styles.editStyle} onPress={() => { navigation.navigate('Surname')}}>
                        <MaterialCommunityIcons name="pencil-circle" color={colors.PURPLE_BTN} size={35}/>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.dataContainer}>
                <View style={{flex:1}}>
                    <Text style = {styles.label}>Dirección</Text>
                    <Text numberOfLines={1} style = {styles.fieldText}>{user.address}</Text>
                </View>
                <View style={styles.editButton}>
                <TouchableOpacity style = {styles.editStyle} onPress={() => { navigation.navigate('Address')}}>
                    <MaterialCommunityIcons name="pencil-circle" color={colors.PURPLE_BTN} size={35}/>
                </TouchableOpacity>
                </View>
            </View>

            <View style={styles.dataContainer}>
                <View style={{flex:1}}>
                    <Text style = {styles.label}>Teléfono</Text>
                    <Text numberOfLines={1} style = {styles.fieldText}>{user.phone}</Text>
                </View>
                <View style={styles.editButton}>
                <TouchableOpacity style = {styles.editStyle} onPress={() => { navigation.navigate('Phone')}}>
                    <MaterialCommunityIcons name="pencil-circle" color={colors.PURPLE_BTN} size={35}/>
                </TouchableOpacity>
                </View>
            </View>

            <View style={styles.dataContainer}>
                <View style={{flex:1}}>
                    <Text style = {styles.label}>Contraseña</Text>
                    <TextInput secureTextEntry={true} style = {styles.fieldText} editable={false}>Contraseña</TextInput>
                </View>
                <View style={styles.editButton}>
                <TouchableOpacity style = {styles.editStyle} onPress={() => { navigation.navigate('Password')}}>
                <MaterialCommunityIcons name="pencil-circle" color={colors.PURPLE_BTN} size={35}/>
                </TouchableOpacity>
                </View>
            </View>

            <View style={styles.sendButton}>
                <TouchableOpacity onPress={()=> Logout()} style={styles.logOutButton}> 
                    <Text style={styles.logOutText}> Cerrar Sesión </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding:20
    },
    dataContainer:{
        backgroundColor:colors.GRAY_ALT, 
        padding:10, borderRadius:10, 
        marginBottom:10, 
        flexDirection:'row',
        elevation:2
    },
    editButton:{
        flex: 1, 
        alignItems:'center', 
        justifyContent:'center', 
        alignContent:'center'
    },
    data: {
        marginBottom: 10, 
        marginTop: 10, 
        color: colors.TEXT,
        fontWeight: "bold"
    },
    label: {
        color: colors.TEXT,
        fontWeight: "bold",
        marginBottom: 2,
    },
    fieldText: {
        marginTop: 5, 
        alignItems: 'flex-start',
        color:colors.TEXT
    },
    editStyle: {
        alignSelf: 'flex-end'
    },
    sendButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 45
    },
    logOutButton: {
        borderColor: '#FF9602', 
        borderWidth: 1, 
        borderRadius: 4, 
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row', 
        height: 44, 
        width: 130
    },
    logOutText: {
        fontSize: 16, 
        color : '#FF9602', 
        textAlign: 'center', 
        backgroundColor: 'transparent'
    },
});

