import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Image, Alert, ImageBackground, Dimensions} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';

//Api de Facebook
import * as Facebook from 'expo-facebook';

import axios from 'axios';

// Dimensiones de la pantalla
const screenWidth = Math.round(Dimensions.get('window').width);


export default function AccountScreen() {
    const navigation = useNavigation();

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/update-facebook';

    navigation.setOptions({
        title: 'Mi cuenta',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Settings')}  style={{width: 40, height: 40, flexDirection: "column",justifyContent: "center", alignItems:'center'}}>
				<Ionicons name="md-settings" color="white" size={25} />
			</TouchableOpacity>
        ),
    })

    const [user , setUser ] = useState('');
    const [isShow , setisShow ] = useState(false);
    const [email , setEmail ] = useState(false);
    const [saved , setSaved ] = useState(false);
    const [conta , setConta ] = useState(0);
    const [progressBar , setProgressBar ] = useState(0);
    const [image , setImage ] = useState('');

    useEffect(() => {

        getStorage();

        //setEmail(user.emailchecked);

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });
        
        
        // // Unmount
        return focused;

    }, []);
    

    useEffect(() => {

        //console.log(user);

      
        

    }, []);


    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    var progress = 0;

    var counter = 0;

    if (user.image) {
        progress = progress + 20;
        counter = counter + 1;
    }

    if (user.emailchecked) {
        progress = progress + 20;
        counter = counter + 1;


    }

    if (user.phoneNumberChecked) {
        progress = progress + 20;
        counter = counter + 1;
    
    }

    if (user.facebookchecked) {
        progress = progress + 20;
        counter = counter + 1;
    }

    //Conectar con Facebook
	const facebook = async () => {

        // Alert.alert('TODO facebook');
        // return;
        try {
            
            await Facebook.initializeAsync('234199600917730'); //reemplazar por facebook id de ok-trato
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync({
                permissions: ['public_profile'],
            });
            if (type === 'success') {

                const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,about,picture`);
                const json = await response.json();
                
                const userId = user._id;
    
                const idFace = json.id;


            //update en bd
				//update en bd
                axios.post(url, {
                    _id:userId,
                    idfacebook: idFace,
                },
            )
            .then((response) => {
				//Cancelar velo de loading
                setisShow(false);
                if(response.data.status){
                    Alert.alert(
                        'Se ha conectado con facebook!'
                        )
                        navigation.navigate('Account');
                }else
                {
                    Alert.alert(
                        'Error conectando con Facebook, intente nuevamente'
                    )
                } 
            })
            .catch((error) => {
                console.log(error);
                // //Cancelar velo de loading
                setisShow(false);
                
                Alert.alert(
                    'Error de red, intente nuevamente'
                )
            })
            } else {
                // type === 'cancel'
            }
        } catch ({ message }) {
            Alert.alert(`Facebook Login Error: ${message}`);
        }
    }

    return (

        <View style={{ flex: 1,  backgroundColor: '#fff' }}>
            <Loader loading={isShow} />
            <View style={{ flex: .5}}>
                <View style={{height:150,backgroundColor: colors.PRIMARY ,alignItems: 'center', justifyContent: 'center'}}>
                    {/* Imagen de portada */}
                    {user.banner &&
                            <Image source={{ uri: config.BASE_URL_API+'covers/'+user.banner }} style={{ height: '100%', width:'100%' }} />} 	
                    {/* Imagen de perfil */}
                    <View style={{ elevation: 7, width: 140, height: 140, borderRadius: 70,  backgroundColor: 'white', position: 'absolute', top: 90,
                        flexDirection:'row', alignItems:'center',justifyContent: 'center'
                    }}>
                        {user.image &&
                            <Image source={{ uri: config.BASE_URL_API+'users/'+user.image }} style={{ width: 130, height: 130, borderRadius: 130/2 }} />} 	
                    </View>

                    {/* Boton de editar imagen de portada */}
                    <TouchableOpacity onPress={ () => navigation.navigate('ProfileImg',{ editProfileOrCover: 2 })} style={{position:'absolute', right:5, bottom:5,
                        width: 50, height: 50, borderRadius: 25, backgroundColor: colors.PURPLE, flexDirection:'row', alignItems:'center',justifyContent: 'center'}}>
                        <Ionicons name="md-camera" color="white" size={30} />
                    </TouchableOpacity>
                
                </View>
                
                {/* Boton de editar imagen de perfil */}
                <View style={{alignItems: 'center' }}>
                    <View style={{ width: 140, height: 140, borderRadius: 70}}>
                        <TouchableOpacity onPress={ () => navigation.navigate('ProfileImg',{ editProfileOrCover: 1 })} style={{position:'absolute', right:0, bottom:140/2,
                            width: 50, height: 50, borderRadius: 25, backgroundColor: colors.PURPLE, flexDirection:'row', alignItems:'center',justifyContent: 'center'}}>
                            <Ionicons name="md-camera" color="white" size={30} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>		

            {/* Barra de Progreso */}

            <View style={styles.container}>
                <View style={{ flexDirection:'row', justifyContent:"center", alignItems: "center" , marginTop: 145, marginBottom: 2}}>
                
                {counter === 0 ? (
                    <ImageBackground source={require('../../assets/images/barra0.png')} style={styles.img}></ImageBackground>
                    
                        ) : (null)}       

                {counter === 1 ? (
                    <ImageBackground source={require('../../assets/images/barra1.png')} style={styles.img}></ImageBackground>
                        ) : (null)}        

                {counter === 2 ? (
                    <ImageBackground source={require('../../assets/images/barra2.png')} style={styles.img}></ImageBackground>
                        ) : (null)}

                {counter === 3 ? (
                    <ImageBackground source={require('../../assets/images/barra3.png')} style={styles.img}></ImageBackground>
                        ) : (null)}

                {counter === 4 ? (
                    <ImageBackground source={require('../../assets/images/barra4.png')} style={styles.img}></ImageBackground>
                        ) : (null)}

                {counter === 5 ? (
                    <ImageBackground source={require('../../assets/images/barra5.png')} style={styles.img}></ImageBackground>
                        ) : (null)}             
                                        
                </View>
                <View style = {{marginBottom: 20}}>
                <Text style = {{alignSelf: 'center', color: colors.GRAY_lyrics}} >{progress} %</Text>
                </View>

                <TouchableOpacity style={{ flexDirection:'row', marginTop:5}} disabled={user.emailchecked } onPress={ () => navigation.navigate('ConfirmEmail', {_id: user._id, email: user.email, names: user.names})}>
                    <View style={[user.emailchecked ? styles.styleinactivo: styles.styleactivo]}>
                        <Ionicons name="md-mail" color="white" size={15} />
                    </View>
                    <Text style={[user.emailchecked ? styles.TextStyleChecked: styles.TextStyle]}>Confirmar Email</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ flexDirection:'row', marginTop:5 }} disabled={user.facebookchecked } onPress={ () => facebook() }>
                    <View style={[user.facebookchecked ? styles.styleinactivo: styles.styleactivo]}>
                    <Ionicons name="logo-facebook" color="white" size={15} />
                    </View>
                    <Text style={[user.facebookchecked ? styles.TextStyleChecked: styles.TextStyle]}>Confirmar Facebook</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ flexDirection:'row', marginTop:5 }} disabled={user.phoneNumberChecked } onPress={ () => navigation.navigate('ValidatePhone', {_id: user._id, phone: user.phone})}>
                    <View style={[user.phoneNumberChecked ? styles.styleinactivo: styles.styleactivo]}>
                    <Ionicons name="md-phone-portrait" color="white" size={15} />
                    </View>
                    <Text style={[user.phoneNumberChecked ? styles.TextStyleChecked: styles.TextStyle]}>Confirmar Teléfono</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ flexDirection:'row', marginTop:5 }} disabled={user.image } onPress={ () => navigation.navigate('ProfileImg',{ editProfileOrCover: 1 })}>
                    <View style={[user.image ? styles.styleinactivo: styles.styleactivo]}>
                    <Ionicons name="md-camera" color="white" size={15} />
                    </View>
                    <Text style={[user.image ? styles.TextStyleChecked: styles.TextStyle]}>Imagen de Perfil Agregada</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding:20,
    },
    img: {

		width: 0.8*screenWidth,
        height: 35,
        resizeMode: "cover"
        
    },
    styleactivo: {

        left:0,
        width: 30, 
        height: 30, 
        borderRadius: 25, 
        backgroundColor: colors.PURPLE, 
        alignItems:'center',justifyContent: 'center'
        
    },
    styleinactivo: {

        left:0,
        width: 30, 
        height: 30, 
        borderRadius: 25, 
        backgroundColor: colors.GRAY_ALT, 
        alignItems:'center',justifyContent: 'center'
        
    },
    TextStyle: {
		color: colors.TEXT,
		marginVertical: 5,
        marginLeft: 10,
        fontWeight:'bold'
        
    },
    TextStyleChecked: {
		color: colors.GRAY_lyrics,
		marginVertical: 5,
        marginLeft: 10,
        fontWeight:'bold'
        
    }
});