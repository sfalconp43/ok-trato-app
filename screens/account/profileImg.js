import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TouchableOpacity, Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import config from '../../config/config';
import colors from '../../assets/styles/colors';

export default function ProfileImg({ route, navigation }) {

    //obtener parametros
    const { editProfileOrCover } = route.params;

    navigation.setOptions({
        title: 'Fotos de Perfil',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [user , setUser ] = useState('');
    const [image , setImage ] = useState(null);
    //1 -> Foto de perfil. 2 -> Foto de portada
    const [type , setType ] = useState(0);
    
    useEffect(() => {
        getPermissionAsync();
        getStorage();
        setType(editProfileOrCover);

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        // Unmount
        return focused;

    }, []);
    
     // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    const getPermissionAsync = async () => {
		await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
    }

    //Seleccionar imagen de galeria
    const selectImage = async () => {
		let customAspect;
		
		if(type == 1){
			customAspect = [1,1];
		}else{
			customAspect = [4,3];
		}
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
			aspect: customAspect,
			quality: 0.3,
		});
	
		if (!result.cancelled) {
            setImage(result.uri);
			navigation.navigate('ConfirmImg',{ image: result.uri, profileOrCover: type } );
		}
    }; 

	//Tomar imagen con la camara
    const takeImage = async () => {

		let customAspect;

		//aspect ratio si es cover o foto perfil
		if(type == 1){
			customAspect = [1,1];
		}else{
			customAspect = [4,3];
		}

		let result = await ImagePicker.launchCameraAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			allowsEditing: true,
			aspect: customAspect,
        });
	  
        if (!result.cancelled) {
            setImage(result.uri);
			navigation.navigate('ConfirmImg',{ image: result.uri, profileOrCover: type } );
        }
    }
    
    //alterna tipo de actualizacion 1 -> Foto de perfil. 2 -> Foto de portada
	const changeTypeUdate = (type) => {
		setType(type);
	}

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
				{/* Foto Portada */}
				<View style={{ flex: 0.3}} >
					<TouchableOpacity activeOpacity={1.0} onPress={() => changeTypeUdate(2)} style={[type == 2 ? styles.BorderEdit : '' ,{ height:150,backgroundColor: colors.PRIMARY ,alignItems: 'center', justifyContent: 'center'}]}>
					{user.banner &&
						<Image source={{ uri: config.BASE_URL_API+'covers/'+user.banner }} style={{ height: '100%', width:'100%' }} />}
					</TouchableOpacity>
					<TouchableOpacity activeOpacity={1.0} onPress={() => changeTypeUdate(1)} style={[type == 1 ? styles.BorderEdit : '',{ elevation: 7, width: 140, height: 140, borderRadius: 70,  backgroundColor: 'white', position: 'absolute', top: 90,
							flexDirection:'row', alignItems:'center',justifyContent: 'center', alignSelf:'center'
						}]}>
					{/* Imagen de perfil */}
							{user.image &&
								<Image source={{ uri: config.BASE_URL_API+'users/'+user.image }} style={{ width: 130, height: 130, borderRadius: 130/2 }} />} 	
					</TouchableOpacity>
				</View>

				{/* Texto editar */}
				<View style={{  flex:0.2, alignItems: 'center',justifyContent: 'center' }} >
						<Text style={{ color:colors.TEXT  }} >{type == 2 ? 'Cambiar foto de portada':'Cambiar fotor de perfil'}</Text>
				</View>

	
				{/* Botones */}
				<View style={{  flex:0.1, alignItems: 'center',justifyContent: 'center' }} >

					<View style={styles.container}>
						<View style={{ flex: 1, alignItems: 'center'}}>
							<TouchableOpacity onPress={selectImage} style={styles.CustomButton} activeOpacity={0.5}>
								<View style={{backgroundColor:colors.PURPLE, width:40, height:40 ,borderRadius:20, marginLeft:5, alignItems:'center',justifyContent: 'center'}}>
									<MaterialCommunityIcons name="upload" color="white" size={20}/>
								</View>
								<View  style={{flex: 1, alignItems:'center'}}>
									<Text style={styles.TextStyle}> GALERÍA </Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={{ flex: 1, alignItems: 'center'}}>
							<TouchableOpacity onPress={takeImage} style={styles.CustomButton} activeOpacity={0.5}>
								<View style={{backgroundColor:colors.PURPLE, width:40, height:40 ,borderRadius:20, marginLeft:5, alignItems:'center',justifyContent: 'center'}}>
									<MaterialCommunityIcons name="camera" color="white" size={18}/>
								</View>
								<View  style={{flex: 1, alignItems:'center'}}>
									<Text style={styles.TextStyle}> CÁMARA </Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>

				</View>

			</View>		
    );
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		position:'absolute',
	},

	CustomButton: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: colors.GRAY,
		height: 50,
		width: 150,
		borderRadius: 25,
		margin: 5,
	},
	
	TextStyle: {
	  	color: 'black',
	  	marginBottom: 4,
	  	marginRight: 20,
	},

	BorderEdit:{
		borderColor: colors.PURPLE,
		borderWidth:3
	},
});
