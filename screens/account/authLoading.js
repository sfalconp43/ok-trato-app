import React from 'react';
import {
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
const colors = require('../../assets/styles/colors');

export default function AuthLoading() {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color={colors.PRIMARY}/>
            <StatusBar barStyle="default" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
