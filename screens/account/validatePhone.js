import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, AsyncStorage, KeyboardAvoidingView, Dimensions} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import config from '../../config/config';
import { TextInput, PhoneInput } from 'react-native-gesture-handler';
import { useForm, Controller } from 'react-hook-form';
//loader
import Loader from '../loader/loader';
import { useHeaderHeight } from '@react-navigation/stack';

import {  MaterialCommunityIcons } from '@expo/vector-icons';

export default function SellingScreen({ route }) {
    
    const { _id } = route.params;
    const { phone } = route.params;
    const headerHeight = useHeaderHeight();

    // console.log(phone);

    const navigation = useNavigation();

    navigation.setOptions({
        title: 'Verificar Telefono',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    })

    const [refreshing , setRefreshing ] = useState(true);
    const [isShow , setisShow ] = useState(false);
    const [user , setUser ] = useState('');
    const [saved , setSaved ] = useState(false);


    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    const updateAsyncStorage = async () => {

        AsyncStorage.setItem('userData', JSON.stringify(user)).then(()=>{
            navigation.navigate('ConfirmSms');
        }).catch((error)=>{
            console.log(error);
        });
    }
    

    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        })

        //actualizar storage si guardo el nombre
        if(saved) {
            updateAsyncStorage();
        }

        //getStorage();
        //colocar dato en el input
        setValue('phone',phone);

        // Unmount
        return focused;
        
    }, [saved]);


    //Guardar
    const onSubmit = data => {

        var finalPhone = data.countryCode+data.operatorCode+data.phoneNumber;
        
        //Si la validacion falla data debe ser nulo
        if (finalPhone) { 
            //Mostrar velo de loading
            setisShow(true);

            const url = config.BASE_URL_API+'api/v1/send-verification-sms';

            console.log('enviando a confirm sms', user._id );
            
            axios.post(url, {
                userId: _id,
                phoneNumber: finalPhone,
                // phoneNumber: data.phone
            }, 
            {
                
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    navigation.navigate('ConfirmSms', {_id: user._id});
                    setSaved(true);
                    Alert.alert('Mensaje enviado correctamente')

                }else{
                    //Error
                    Alert.alert('Error enviando, intente nuevamente')
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de envío, intente nuevamente')
            })
        }
    }


    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
        value: args[0].nativeEvent.text,
        };
    };

    return (

    <KeyboardAvoidingView style={styles.container} behavior="padding"
        keyboardVerticalOffset={
            Platform.OS === "ios" ? headerHeight : (headerHeight)
        }>
        <Loader loading={isShow} />
        <Text style = {{color: colors.GRAY_lyrics, fontWeight: 'bold'}}>Numero de Teléfono</Text>
        <View style = {{flexDirection: 'row'}}>
        <MaterialCommunityIcons name="phone" style = {{marginTop: 20, marginRight: 10, color: colors.PURPLE_BTN }}  size={20}/>
        <Controller
            as={<TextInput style={styles.inputCode}  keyboardType="numeric" 
            /> }
            control={control}
            name="countryCode"
            onChange={onChange}
            rules={{ required: "Ingrese código del país", pattern: {
                    value: /^[+]+[0-9]/,
                    message: "Ingrese un codigo válido"
                }}}
            maxLength={3}
            // rules={{ required: 'Ingrese el nuero de telefono' }}
            defaultValue="+58"
        />
        {errors.countryCode && <Text style={styles.error}>{errors.countryCode.message}</Text>}
        <Controller
            as={<TextInput style={styles.inputOperator} keyboardType="numeric" />}
            control={control}
            name="operatorCode"
            onChange={onChange}
            rules={{ required: "Ingrese el número de la operadora", pattern: {
                    value: /^[0-9]/,
                    message: "Ingrese un número de operadora válido"
                }}}
            maxLength={3}
            // rules={{ required: 'Ingrese el nuero de telefono' }}
            defaultValue=""
        />
        {errors.operatorCode && <Text style={styles.error}>{errors.operatorCode.message}</Text>}
        <Text style = {{marginTop: 10, marginLeft: 4, color: colors.GRAY_lyrics}}>_</Text>
        <Controller
            as={<TextInput style={styles.inputPhone} keyboardType="numeric" />}
            control={control}
            name="phoneNumber"
            onChange={onChange}
            rules={{ required: "Ingrese el número de teléfono", pattern: {
                    value: /^[0-9]/,
                    message: "Ingrese un número de telefono válido"
                }}}
            maxLength={7}
            // rules={{ required: 'Ingrese el nuero de telefono' }}
            defaultValue=""
        />
        {errors.phoneNumber && <Text style={styles.error}>{errors.phoneNumber.message}</Text>}
        </View>
        <View style={styles.bottom}>
            <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                <Text style={styles.buttonText}>Enviar</Text>
            </TouchableOpacity>
        </View>
    </KeyboardAvoidingView>


    );
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
        alignContent: 'space-between'
        //flexDirection: 'row'
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    inputCode: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 35,
        width: 45,
        marginTop: 10,
        textAlign: 'center',
        borderRadius: 5,
        color: colors.GRAY_lyrics

    },
    inputOperator: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 35,
        width: 80,
        marginTop: 10,
        marginLeft: 5,
        textAlign: 'center',
        borderRadius: 5,
        borderWidth: 1,
        color: colors.GRAY_lyrics


    },
    inputPhone: {
        marginBottom: 10,
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 35,
        width: 140 ,
        marginLeft: 6,
        marginTop: 10,
        textAlign: 'center',
        borderRadius: 5,
        borderWidth: 1,
        color: colors.GRAY_lyrics

    
    },


    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0,
        elevation: 2
    },
    error:{
        color:'red'
    }
});

