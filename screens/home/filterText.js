import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, TouchableOpacity, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Loader from '../loader/loader';
const colors = require('../../assets/styles/colors');
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { SearchBar } from 'react-native-elements';

export default function FilterCategoryScreen() {
    const navigation = useNavigation();

    const [showLoader, setShowLoader] = useState(false);

    navigation.setOptions({
        headerShown: false,
        title: '',
    })

    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            (async () => {
                let searchStorage = await AsyncStorage.getItem('searches');
                if(searchStorage)
                    setSearches(JSON.parse(searchStorage));
            })();
        });

        return focused;
    
    }, []);


    const [edit, setEdit] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [searches, setSearches] = useState([]);

    //Eliminar item de la lista o efectuar consulta si no esta en modo edición
    const handleAction = async (text, index) => {
        if(edit){
            let newArray = [...searches];
            let remove = newArray.splice(index, 1);
            setSearches(newArray);
            await AsyncStorage.setItem('searches', JSON.stringify(newArray));
        }else{
            //Realizar busqueda en el home
            const items = [['doSearch', '1'],['filterText', text]]
            AsyncStorage.multiSet(items, () => {
                navigation.goBack();
            });
          
        }  
        
    }

    //Listado de busquedas
	const renderItem = ({ item, index }) => {
		return (
			<View>
			<TouchableOpacity style={styles.options} onPress={()=>handleAction(item.search, index)}>
				<View style={{flexDirection:'row', alignItems:'center'}}>
                    {edit && 
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',marginTop:1, marginRight:5}}>
                            <Ionicons name="ios-close" color={colors.PURPLE} size={25} />
                        </View>
                    }
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                        <Text numberOfLines={1} style={styles.TextSearch}>{item.search}</Text>
                    </View>
				</View>
			</TouchableOpacity>
			</View>
		);
    };

    const MakeSearch = async () =>{
        //Buscar texto para validar que no esté repetido
        let found = searches.find(o => o.search === searchText);

        //Si encuentra el mismo texto no guarda la busqueda
        if(found){
            return;
        }else{
            //guardar busqueda
            let data = {
                search: searchText
            }
            //Añadir data al inicio del array
            let newArray = [...searches];
            newArray.unshift(data);
            setSearches(newArray);

            //Multi set 
            const items = [['doSearch', '1'],['filterText', searchText], ['searches', JSON.stringify(newArray)]]
            AsyncStorage.multiSet(items, () => {
                navigation.goBack();
            });

        }
        
    }

    const updateSearch = text => {
        setSearchText(text);
    };

    const deleteAll = () => {
        setEdit(false);
        AsyncStorage.removeItem('searches');
        AsyncStorage.removeItem('doSearch');
        navigation.goBack();
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white', }}>
            <SearchBar
                //autoFocus={true}
                containerStyle={{backgroundColor:colors.PRIMARY, borderWidth:0,
                    borderBottomColor: 'transparent',
                    borderTopColor: 'transparent'
                }}
                inputContainerStyle={{backgroundColor:colors.FILTER_TEXT}}
                inputStyle={{ backgroundColor: colors.FILTER_TEXT,
                    height: 40,
                    padding: 10,
                }}
                searchIcon={() => <TouchableOpacity onPress={()=>navigation.goBack()}><Ionicons name="ios-arrow-round-back" size={30}/></TouchableOpacity>}
                rightIconContainerStyle={{backgroundColor:'#EEEEEE'}}
                onChangeText={updateSearch}
                value={searchText}
                onSubmitEditing ={MakeSearch}
            />
            <View style={styles.container}>
                { searches.length > 0 ?
                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <View>
                            <Text style={styles.label}>Búsquedas recientes</Text>
                        </View>
                        <View>
                            {!edit ?
                                <View>
                                    <TouchableOpacity onPress={()=>setEdit(true)}>
                                        <Text style={styles.labelBtn}>Editar</Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity onPress={deleteAll}>
                                        <Text style={styles.labelBtn}>Eliminar todo</Text>
                                    </TouchableOpacity>                    
                                    <Text style={[styles.labelBtn,{marginHorizontal:5}]}>|</Text>
                                    <TouchableOpacity onPress={()=>setEdit(false)}>
                                        <Text style={styles.labelBtn}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>:null
                }
                
                <FlatList
                    data={searches}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
                
            </View>
            <Loader loading={showLoader} />
        </SafeAreaView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
    },
    label: {
		color: colors.TEXT,
		marginVertical: 3,
        fontSize:14,
    },
    labelBtn:{
        color: colors.PRIMARY,
		marginVertical: 3,
        fontSize:14,
    },
    options:{
        marginVertical:10
    },
    TextSearch: {
		color: colors.TEXT,
        fontSize:16,
    },
});
