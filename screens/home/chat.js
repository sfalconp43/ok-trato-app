import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Alert, KeyboardAvoidingView, TextInput, Platform, FlatList, TouchableOpacity, ActivityIndicator, Image, ImageBackground } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import Message from '../../components/messages';
import io from "socket.io-client";
import { LinearGradient } from 'expo-linear-gradient';

import { useNavigation } from '@react-navigation/native';

export default function Chat({ route }) {

    const navigation = useNavigation();
    const socket = io(config.BASE_URL_API);

    navigation.setOptions({
        title: 'Enviar mensaje',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    //obtener parametros
    const { product } = route.params;
    const { chatid } = route.params;

    //data del usuario vendedor
    const { userdata } = route.params;
    
    const [user , setUser ] = useState(null);
    const [isShow , setisShow ] = useState(true);
    const [chat , setChat ] = useState(null);
    const [chatMessages , setChatMessages ] = useState([]);

    //ocultar/mostrar boton de enviar mensaje
    const [showSend , setShowSend ] = useState(true);
    //texto del mensaje
    const [newMessage , setNewMessage ] = useState('');
    const [messageReceived , setMessageReceived ] = useState(null);

     //limite de mensajes
	const [limit , setLimit ] = useState(50);
	//paginacion
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
    const [endData, setEndData ] = useState(false);

    const [quickResponse, setQuickResponse] = useState(false);

    useEffect(() => {
    
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            getStorage();
        });

        if(user){
            socket.on("message", data => {
                //LLega mensaje de otro usuario
                if(data.idsender !== user._id) {
                    var newMessage = {
                        _id: data._id,
                        idsender: data.idsender,
                        message: data.message,
                        createdate: data.createdate,
                        seen:true,
                    }
                    //Setear mensaje y actualizar lista
                    setMessageReceived(newMessage);
                    //aumentar cantidad de mesajes para paginacion
                    setOffset(offset+1);
                }
            });
        }
        

        // Unmount
        return ()=> {
            socket.off('message');
            socket.off('msgc');
			socket.close();
            focused;
        }
    }, [user]);

    //Obtener mensajes y escuchar mensajes entrantes si hay usuario
    useEffect(() => {
        if(user){
            getMessages();
        }
    }, [user]);

    //Actualizar la lista de mensajes a visto si se recibe algun mensaje
    useEffect(() => {

        if(messageReceived){

            var allMessages = [messageReceived, ...chatMessages];
            //Al recibir mensajes colocar como visto los mensajes anteriores
            for(i=0; i< allMessages.length; i++) {
                if(allMessages[i].seen == false){
                    allMessages[i].seen = true;
                    break;
                }
            }
            
            setChatMessages(allMessages);
        }
    }, [messageReceived]);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    //Conectarse a io socket basado en el namespace
	const goOnline = (namespace) => {
		//conectarse al namespace del chat
		socket.emit('msgc',namespace);
    }

    //obtener mensajes 
	const getMessages = async () => {

        //Consultar mensajes de chat por namespace y id product
        const getMessageUrl = config.BASE_URL_API+'api/v1/get-messages-chat';
        //Consultar mensajes de chat por id del chat
        const getMessageUrlbyId = config.BASE_URL_API+'api/v1/get-messages-chat-id';

		try {

            let queryByIdChat = false;
            let user_product = null;
            let namespace = '';
            var data = {};
            //parametros de busqueda 

            //si hay producto consulta por diferentes parametros
            if(product){
                //valores default
                user_product = product.iduser._id;
                //construir namespace ( iduserfrom+iduserto+idproduct )
                namespace = `${user._id}${user_product}${product._id}`;
                data = {
                    namespace: namespace,
                    idproduct: product._id,
                    iduser:user._id,
                    limit,
                    offset
                };
            }else{
                //si esta el id del chat consultar por dicho id
                queryByIdChat = true;
            }
		
			var url = getMessageUrl;
			// se consulta por el chat id directamente
			if(queryByIdChat){
				data = {
					idchat: chatid,
                    iduser:user._id,
                    limit,
                    offset
				}
				url = getMessageUrlbyId;
            }

			axios.post(url,data)
			.then((response) => {
				//Cancelar velo de loading
				setisShow(false);
                //si la consulta es true
				if(response.data.status){

                    setFooter(false);
                    //Cantidad de tableros obtenidos
                    let count = response.data.messages.length;
                    //Nuevo offset
                    setOffset(offset+count);
                    //No consultar mas si ha llegado al final de la data
                    if(count<limit){
                        setEndData(true);
                    }

					//verificar si hay previa conversacion y guardar/actualizar en el state
					if(response.data.messages.length > 0){
                        setChat(response.data.chat);
                        setChatMessages(chatMessages.concat(response.data.messages));
                    }

                    if(response.data.chat){
                        goOnline(response.data.chat.namespace); //llamar funcion online en el namespace
                    }
				}else{
                    //mostrar respuestas rápidas
                    setQuickResponse(true);
                }
			})
			.catch((error) => {
				console.log(error);
				//Cancelar velo de loading
				setisShow(false);
				Alert.alert('Error de red, intente nuevamente');
			})
		}
		catch (e) {
			setisShow(false);
		   	console.error(e.message);
		}

    }

    //enviar mensaje
	const sendMessage = async () => {
        //Insertar mensaje  
        const urlInsert = config.BASE_URL_API+'api/v1/insert-chat';

		//validar si es nuevo chat
		var isnew = true;

		//datos de producto, namespace y usuario
        let userid = user._id;
        let userto = null;
        //mensaje a guardar
        let messageToSend = newMessage;
        let namespace = '';
        let idproduct = '';
        let chatid = ''

        if (product){
            //setear parametros si el producto existe
            userto = product.iduser._id;
            idproduct = product._id;
            //construir namespace ( iduserfrom+iduserto+idproduct )
            namespace = `${userid}${userto}${idproduct}`;
            chatid = null;
        }

        if(chat){
            //setear parametros si es por chat id 
            //validar a quien se le envia el mensaje
            if(userid == chat.iduser1){
                userto = chat.iduser2;
            }else{
                userto = chat.iduser1;
            }
            namespace = chat.namespace;
            idproduct = chat.idproduct;
            chatid = chat._id;
            isnew = false;
        }
        
		//Mostrar velo de loading
        setisShow(true);
		
		//si es primera vez se crea registro de chat y chat message
        //de lo contrario guarda solo el mensaje
		await axios.post( urlInsert ,{
			isnew:isnew,
			iduserfrom: userid,
			iduserto: userto,
			message: messageToSend,
			namespace: namespace,
			idproduct: idproduct,
			chatid:chatid
        })
		.then((response) => {

			if(response.data.status){

                if(isQuick || chatMessages.length == 0){
                    setIsQuick(false);
                    navigation.goBack();
                }

				//Enviar mensaje por socket al chat
				socket.emit('send message', {
                    room: namespace,
                    id: response.data.id,//id del mensaje
                    idsender: userid,
					message: messageToSend,
                    createdate: response.data.createdate
				});

				//Enviar mensaje por socket a la lista de chats del usuario que recibe
				var userToEmitMessage = userto;

				socket.emit('send alert', {
					//namespace usuario que recibe.
					room: `user${userToEmitMessage}`,
					message: messageToSend,
					chatid: chatid,//id del chat
				});
				
				//contruir objeto nuevo mensaje
				let newMessage = {
					_id: response.data.id, //id chat messages
					idsender: userid, //usuario que envia
                    message: messageToSend,//mensaje
                    chatid,//id de chat
                    createdate: response.data.createdate,// fecha
                    seen:false,
				}

				//actualizar solo mensajes
				let updateMessage = [newMessage, ...chatMessages];
                setChatMessages(updateMessage);
                //aumentar offset de paginación
                setOffset(offset+1);
                //limpiar input y ocultar boton de enviar
                setNewMessage('');
                setShowSend(false);

                //Si es nuevo el mensaje 
				if(isnew){
                    //setear chat en el state
                    setChat(response.data.chat);
                    //si ya inserto chat activar el socket
                    goOnline(namespace); //llamar funcion online en el namespace
				}
                //Cancelar velo de loading
                setisShow(false);
			}else{
				Alert.alert('Error de red, intente nuevamente')
			}

		})
		.catch((error) => {
            console.log(error);
			//Cancelar velo de loading
            setisShow(false);
			Alert.alert('Error de red, intente nuevamente')
		}) 
		
    }
    
    //Componente de mensajes
	//Se formatean los mensajes de diferente forma dependiendo de quien recibe o envia
	const renderItem = ({ item }) => {
		let userid = user._id;
		const message = item.message;
		const date = item.createdate;
		const seen = item.seen;
		//verificar lado del mensaje 
		const side = item.idsender === userid ? 'right' : 'left'

		return (
			<Message side={side} message={message} date={date} seen={seen}/>
		)
    };
    
    //actualizar mensaje en el estado
	const updateMessageState = (text) => {
        setNewMessage(text);
		//Activar/desactivar boton de envio
		if (text.trim() === ""){
            setShowSend(false);
		}else{
			setShowSend(true);
		}
    }
    
    //Añadir mas elementos al flatlist
	const addMoreData = () => {
        //si no ha llegado al final de la data seguir consultando
		if(!endData && chatMessages.length>=limit){
			setFooter(true);
			getMessages();
		}
    }

    //Loading footer
	const renderFooter = () => {
		return (
			<View>
				{footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
			</View>
		)
    }

    //Navegar al perfil del usuario (vendedor)
    //id: id de usuario cliente
	const userProfile = async (id) => {
        if(user){
            await AsyncStorage.setItem('idseller', id.toString())
            navigation.navigate('UserDetail', { id });
        }else{
            navigation.navigate('User');
        }
    }

    const [isQuick, setIsQuick] = useState(false);
    //actualizar mensaje en el estado
	const sendQuickResponse = (text) => {
        setQuickResponse(false);
        setIsQuick(true);
        setNewMessage(text);
    }

     //Enviar mensaje rapido
     useEffect(() => {
        if(isQuick){
            sendMessage();
        }
    }, [isQuick, newMessage]);

    return (
        <KeyboardAvoidingView style={{flex:1, backgroundColor: '#ffffff'}}>
            <Loader loading={isShow} />	
            {/**Profile user */}
            <TouchableOpacity style={{marginBottom:10}} onPress={()=>userProfile(userdata.id)}>
                <View style={styles.containerProd}>
                    <View>
                        <Image source={{ uri: `${config.BASE_URL_API}users/${userdata.image}` }} style={styles.img} />
                    </View>
                    <View style={styles.middleMsg}>
                        <Text numberOfLines={1} style={styles.username}>{ userdata.names+' '+userdata.lastnames }</Text>
                        <Text numberOfLines={1} style={styles.address}>{ userdata.productaddress }</Text>
                    </View>      
                    <ImageBackground source={{ uri: `${config.BASE_URL_API}${userdata.imageproduct}` }} style={styles.imgProd} >
                        <View style={{alignSelf:'flex-end', marginHorizontal:5, marginVertical:2}}><Text numberOfLines={1} style={styles.price}>{'$'+userdata.price}</Text></View>
                        <LinearGradient
                            colors={['transparent',colors.PURPLE]}
                            style={styles.gradient}
                        />
                    </ImageBackground>
                </View>
            </TouchableOpacity>
            {/**Respuestas rapidas */}
            {(quickResponse && chatMessages.length == 0) ? 
                <View style={{ padding:10 }}>
                    <Text style={{ color:colors.TEXT, fontSize:16, marginVertical:10 }}>Toca un mensaje para enviar o escribe el tuyo</Text>
                    <View style={styles.sendButton}>
                        <TouchableOpacity style={styles.option} onPress={()=>sendQuickResponse('Hola, ¿está todavía disponible?')}> 
                            <Text style={styles.optionText}> Hola, ¿está todavía disponible? </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.option} onPress={()=>sendQuickResponse('Hola, me gustaría comprar.')}> 
                            <Text style={styles.optionText}> Hola, me gustaría comprar. </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.option} onPress={()=>sendQuickResponse('Hola, ¿podemos reunirnos?')}> 
                            <Text style={styles.optionText}> Hola, ¿podemos reunirnos? </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                :null
            }
            
            <FlatList inverted
                data={chatMessages}
                renderItem={renderItem}
                keyExtractor={item => String(item._id) }
                onEndReached={addMoreData}
                onEndReachedThreshold={0.1}
                ListFooterComponentStyle={styles.flfooter}
				ListFooterComponent={renderFooter}
            />
            <View style={styles.footer}>
                <TextInput
                    value={newMessage}
                    style={styles.input}
                    placeholder="Escribe un mensaje"
                    onChangeText={updateMessageState} 
                />
                {showSend ? 
                <TouchableOpacity  onPress={sendMessage}>
                    <Ionicons name="md-send" style={styles.send} size={25} />
                </TouchableOpacity>
                :null}
                
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
		flexDirection: 'row',
		backgroundColor: '#eee'
	},
	input: {
		fontSize: 16,
		flex: 1,
		padding: 15
	},
	send: {
		alignSelf: 'center',
		color: colors.PURPLE,
		padding: 15
	},
    flfooter:{
		padding:20
    },
    containerProd: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10
    },
    circle: {
		top: 0,
		position:'absolute',
		width: 8,
		height: 8,
		borderRadius: 8/2,
		backgroundColor: 'red'
    },
    middleMsg:{
		marginLeft:10,
		padding:4,
		flex: 1,
		justifyContent: 'center',
    },	
    imgProd: {
		justifyContent: 'flex-end', 
		width: 62,
		height: 62,
        borderRadius:10,
        backgroundColor:'#EEEEEE'
    },
    username:{
		fontSize:16,
        fontWeight:'bold',
        color:colors.TEXT
    },
    address:{
        fontSize:14,
        color:colors.TEXT
    },
    img: {
		justifyContent: 'flex-end', 
		width: 62,
		height: 62,
		borderRadius: 30,
    },
    gradient:{
		position: 'absolute',
		left: 0,
		right: 0,
		bottom: 0,
		height: 20,
    },
    price:{
		color: 'white',
		zIndex:1,
        fontSize:12,
        fontWeight:'bold'
    },
    sendButton: {
        alignItems: 'flex-start',
    },
    option: {
        marginBottom:10,
        borderColor: '#FF9602', 
        borderWidth: 1, 
        borderRadius: 4, 
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row', 
        height: 35, 
    },
    optionText: {
        fontSize: 16, 
        color : '#FF9602', 
        textAlign: 'center', 
        backgroundColor: 'transparent'
    },
});
