import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, AsyncStorage, Alert, ActivityIndicator, Animated, Text, RefreshControl, TouchableOpacity, Dimensions, TouchableHighlight  } from 'react-native';
import { useNavigation } from '@react-navigation/native';
const colors = require('../../assets/styles/colors');
import config from '../../config/config';
import axios from 'axios';
import MasonryList from "react-native-masonry-list";
import { Ionicons } from '@expo/vector-icons';
import { CheckBox } from 'react-native-elements';
import { Input } from 'react-native-elements';
import Popover from 'react-native-popover-view'

export default function HomeScreen() {

    const navigation = useNavigation();

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/find-products/';
    
    navigation.setOptions({
        headerShown: false,
    })

    const [user , setUser ] = useState(null);
    const [products , setProducts ] = useState([]);
    const [refreshing , setRefreshing ] = useState(true);

     //limite de productos
	const [limit , setLimit ] = useState(50);
	//paginacion
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
	const [endData, setEndData ] = useState(false);
	//reload para usar useEffect y recargar data
    const [reload , setReload ] = useState(false);
    
    const [userVerified , setUserVerified ] = useState(false);

    useEffect(() => {

        (async () => {
            //Eliminar storage al inicio
            AsyncStorage.multiRemove(['filterText','filterLocation','filterDistance','filterCategory','findCategory','AllCategory']);

            const userData = await AsyncStorage.getItem('userData');
            setUser(JSON.parse(userData));
            setUserVerified(true);
        })();
        
        const focused = navigation.addListener('focus', () => {
            //Obtener filtros
            getFilters();
        });

        // Unmount
        return focused;

    }, []);

    //Obtener productos una vez verificado si esta logueado el usuario o no
    useEffect(() => {
        if(userVerified){
            GetProducts();
        }
    },[user,userVerified]);

    //Refrescar mosaico
    useEffect(() => {
        if(offset == 0 && products.length == 0 && reload){
            setReload(false);
            //Obtener data nuevamente
            GetProducts();
        }
    }, [offset, products, reload]);

    //parametros de filtros
    const [searchText, setSearchText] = useState(null);
    const [searchLocation, setSearchLocation] = useState(null);
    const [searchDistance, setSearchDistance] = useState(null);
    const [searchCategory, setSearchCategory] = useState(null);

    //Obtener cada filtro y efectuar acciones
    const getFilters = async () => {

        //Variable de storage que indica que debe efectuarse la busqueda 
        const doSearch = await AsyncStorage.getItem('doSearch');

        if(doSearch){

            const keys = ['filterText','filterLocation','filterDistance','filterCategory','findCategory','AllCategory'];

            AsyncStorage.multiGet(keys, (err, stores) => {
                //Eliminar doSearch para evitar re consultas
                AsyncStorage.removeItem('doSearch');

                //filtrado por texto
                let filterText = stores[0][1];
    
                if(filterText){
                    if(filterText == searchText) return;
                    AsyncStorage.removeItem('filterText');
                    setSearchText(filterText);
                    onRefresh();
                }
    
                //filtrado por ubicación
                let filterLocation = stores[1][1];
                let filterDistance = stores[2][1];

                if(filterLocation && filterDistance){
                    setSearchLocation(JSON.parse(filterLocation));
                    setSearchDistance(filterDistance);
                    //Ordenar por lo más cerca
                    setOrder(1);
                    onRefresh();
                }

                //filtrado por categoria
                let filterCategory = stores[3][1];
                let findCategory = stores[4][1];
                if(filterCategory && findCategory){
                    AsyncStorage.removeItem('findCategory');
                    setSearchCategory(JSON.parse(filterCategory));
                    onRefresh();
                }

                //filtrado todas las categorias
                let AllCategory = stores[5][1];
                if(AllCategory){
                    AsyncStorage.removeItem('AllCategory');
                    setSearchCategory(null);
                    onRefresh();
                }
            });
        }

    }

    //Eliminar filtro de texto
    const removeFilterText = () => {
        setSearchText(null);
        onRefresh();
    }

    //Obtener productos por filtros
	const GetProducts = () => {

        setRefreshing(true);
        //Consultar productos 
        axios.post(url, {
            iduser: user ? user._id : null,
            lat: (searchLocation ? searchLocation.latitude : null),
            lng: (searchLocation ? searchLocation.longitude : null),
            category: (searchCategory ? searchCategory._id : null),
            limit,
            offset,
            search:searchText,
            distance:searchLocation ? searchDistance: null,
            min_price:lowPrice ? lowPrice: null,
            max_price:highPrice ? highPrice: null,
            order,
        }).then((response) => {
            if(response.data.status){
                //Cantidad de productos obtenidos
                let count = response.data.results.length;
                //console.log(count);
                
                //Parche
                if(count == 0 && offset == 0 ){
                    setEmptyResult(true);
                }else{
                    setEmptyResult(false);
                }

                if(count > 0){
                    //Nuevo offset
                    setOffset(offset+count);

                    //No consultar mas si ha llegado al final de la data
                    if(count<limit){
                        setEndData(true);
                    }

                    //Concatenar resultados al array
                    setProducts(products.concat(response.data.results));
                    setshowEmpty(false);
                }

            }   
            setFooter(false);
            setRefreshing(false);    
        })
        .catch((error) => {
            console.log(error)
            //Cancelar velo de loading
            setRefreshing(false);
            setFooter(false);
            Alert.alert('Error de red, intente nuevamente');
        })
        
    };
    
    //Refrescar el FlatList
	const onRefresh = async () => {
        //Limpiar data vieja y mostrar loading
        if(errorPrice){
            setHighPrice(null);
            setLowPrice(null);
            setErrorPrice(false);
        }
        setProducts ([]);
		setReload(true)
		setOffset(0);
        setEndData(false);
    }

    //Abrir producto en nueva pantalla
	const onPressImage = product => {
		//Mostrar screen de detalle
		navigation.navigate('ProductDetail', { idproduct: product._id });
    };
    
    //Añadir mas elementos al flatlist
	const addMoreData = () => {
		//si no ha llegado al final de la data seguir consultando
		if(!endData && !refreshing){
			setFooter(true);
			GetProducts();
		}
    }

    //HEADER
    const NAVBAR_HEIGHT = 97;
    const FILTER_BAR_HEIGHT = 35;
    const [scroll] = useState(new Animated.Value(0));
    //NAVBAR_HEIGHT-FILTER_BAR_HEIGHT solo ocultar el header navbar
    const [headerY] = useState(Animated.multiply(Animated.diffClamp(scroll, 0, NAVBAR_HEIGHT-FILTER_BAR_HEIGHT), -1));

    //Header Superior
    const Header = () => {
        return(
            <View style={{flexDirection: 'row', backgroundColor: colors.PRIMARY}}>
                {searchText ? 
                   <View style={{flex: 1, width: '70%', height: 55, padding:10}}>
                        <View style={{backgroundColor:'white', width: '100%', height: '100%', borderRadius:5, justifyContent:'center', flexDirection: 'row' }}>
                            <TouchableHighlight onPress={()=>removeFilterText()} underlayColor="#DDDDDD" activeOpacity={0.6} 
                                style={{width: '15%',justifyContent:'center', alignItems:'center', borderRadius:30}} hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
                                <Ionicons name="ios-arrow-round-back" color={colors.PURPLE} size={30} />
                            </TouchableHighlight >
                            <TouchableOpacity style={{width: '70%', justifyContent:'center'}} onPress={()=>navigation.navigate('FilterText')}>
                                <Text numberOfLines={1} style={{color:colors.TEXT, fontSize:14, fontWeight:'bold'}}>{searchText}</Text>
                            </TouchableOpacity>
                            <View style={{width: '15%',justifyContent:'center'}} >
                            </View>
                        </View>
                    </View>
                    :
                    <TouchableOpacity style={{flex: 1, width: '70%', height: 55, padding:10}} onPress={()=>navigation.navigate('FilterText')}>
                        <View style={{backgroundColor:'white', width: '100%', height: '100%', borderRadius:5, justifyContent:'center', flexDirection: 'row' }}>
                            <View style={{width: '15%',justifyContent:'center', alignItems:'center'}} >
                                <Ionicons name="md-search" color={colors.PURPLE} size={30} />
                            </View>
                            <View style={{width: '70%', justifyContent:'center', alignItems:'center'}} >
                                <Text style={{color:colors.PURPLE, fontSize:14, fontWeight:'bold'}}>OK TRATO</Text>
                            </View>
                            <View style={{width: '15%',justifyContent:'center'}} >
                            </View>
                        </View>
                    </TouchableOpacity>
                }
                
                <View style={{width: '10%', height: 55}} >
                    <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=>navigation.navigate('FilterCategory')}
                            style={{width: 50, height: 50, backgroundColor: colors.PRIMARY, flexDirection:'row', alignItems:'center',justifyContent: 'center'}}>
                        <Ionicons name="md-apps" color="white" size={30} />
                    </TouchableOpacity>
                </View>
                <View style={{width: '12%', height: 55 }} >
                    <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                        onPress={()=>navigation.navigate('FilterLocation')}
                        style={{width: 50, height: 50, backgroundColor: colors.PRIMARY, flexDirection:'row', alignItems:'center',justifyContent: 'center'}}>
                        <Ionicons name="ios-pin" color="white" size={30} />
                    </TouchableOpacity>
                </View>
            </View>
        )  
    }

    //Filter component buttons
    const Filters = () =>{
        return (
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity style={{flex: 1, width: '50%', height: 40, padding:5}} onPress={()=>{setToolTipSortVisible(true)}}>
                    <View style={{ width: '100%', height: '100%', borderRadius:15, justifyContent:'center', flexDirection: 'row' }}>
                        <View style={{backgroundColor:colors.FILTER_GRAY_DARK, width: '100%', height: '100%', borderRadius:15, justifyContent:'center', flexDirection: 'row' }}>
                            <View style={{width: '80%', justifyContent:'center', alignItems:'center'}} >
                                <Text style={{color:'white', fontSize:12}}>{orderText[order]}</Text>
                            </View>
                            <View style={{width: '20%',justifyContent:'center', alignItems:'center'}} >
                                <Ionicons name="ios-arrow-down" color='white' size={20} />
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                {/**Boton de precio */}
                <TouchableOpacity style={{flex: 1, width: '50%', height: 40, padding:5}} onPress={()=>{setToolTipPriceVisible(true)}}>
                    <View style={{ width: '100%', height: '100%', borderRadius:15, justifyContent:'center', flexDirection: 'row' }}>
                        <View style={{backgroundColor:colors.FILTER_GRAY_DARK, borderTopLeftRadius:15, borderBottomLeftRadius:15, width: '40%', justifyContent:'center', alignItems:'center'}} >
                            <Text style={{color:'white', fontSize:12}}>PRECIO</Text>
                        </View>
                        <View style={{backgroundColor:colors.FILTER_GRAY, width: '60%',
                            justifyContent:'center', 
                            alignItems:'center', flexDirection: 'row', 
                            justifyContent: 'space-between', paddingHorizontal:10}} >
                            <Text style={{color:colors.FILTER_GRAY_DARK, fontSize:12}}>{
                                (lowPrice && highPrice)?`$${lowPrice} - $${highPrice}`:
                                (!lowPrice && highPrice)?`HASTA $${highPrice}`:
                                (lowPrice && !highPrice)?`DESDE $${lowPrice}`:
                                'CUALQUIERA'
                            }</Text>
                            <Ionicons name="ios-arrow-down" color={colors.FILTER_GRAY_DARK} size={20} />
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
   
    //HEADER component
    const AnimatedHeader = () => {
        return (
            <Animated.View style={{ width: "100%",position: "absolute", zIndex: 1, backgroundColor: colors.PRIMARY,
                height:NAVBAR_HEIGHT,
                transform: [{ translateY: headerY}]
            }}>
                <View style={{padding:3}}>
                    <Header></Header>
                </View>
                <View style={{backgroundColor: 'white'}}>
                    <Filters></Filters>
                </View>
            </Animated.View>
        )
    }

    /*************/
    /*** Parche masonry list para no mostrar productos cuando no hay resultados ***/
    useEffect(()=>{
        if(emptyResult && !refreshing){
            setshowEmpty(true);
        }
    },[emptyResult,refreshing,showEmpty]);

    const [showEmpty , setshowEmpty ] = useState(false);
    const [emptyResult , setEmptyResult ] = useState(false);
    /*************/

    //Mostrar/Ocultar Tooltip de precios
    const [toolTipPriceVisible,setToolTipPriceVisible] = useState(false);
    //Menor precio
	const [lowPrice, setLowPrice ] = useState(null);
	//Mayor precio
    const [highPrice, setHighPrice ] = useState(null);  
    const [errorPrice, setErrorPrice ] = useState(false); 

    //Referencias Inputs de precios
    const PriceHighInputRef = useRef(null);
    const PriceLowInputRef = useRef(null);

    //Referencias de donde emergen los tooltips
    const fromViewPrice = useRef(null); 
    const fromViewSort = useRef(null); 
    //Consultar por precios
    const onSubmitPrices = () => {
        let maxPrice = highPrice;
        let minPrice = lowPrice;

        if(minPrice && maxPrice){
            if(parseInt(maxPrice) < parseInt(minPrice)){
                setErrorPrice(true);
                return;
            }else{
                setErrorPrice(false);
            }
        }

        setToolTipPriceVisible(false);
        //Limpiar data vieja y mostrar loading
        setProducts ([]);
        setReload(true)
        setOffset(0);
        setEndData(false);
    } 

    //Mostrar/Ocultar Tooltip de orden/sort
    const [toolTipSortVisible,setToolTipSortVisible] = useState(false);

    //Opciones de orden
    //default lo mas nuevo
    const [order, setOrder] = useState(0);
    const orderText = {
        0: 'LO MÁS NUEVO',
        1: 'LO MÁS CERCA',
        2: 'PRECIO: MÁS BAJO',
        3: 'PRECIO: MÁS ALTO',
    }
    
    //Manejo de filtros
    const changeFilter = (option) =>{
        setOrder(option);
        //Cerrar tooltip
        setToolTipSortVisible(false);
        onRefresh();
    }

    return (
        <View style={styles.container}>

            <AnimatedHeader></AnimatedHeader>
            
            <Animated.View style={{  width: "100%",position: "absolute", marginTop:NAVBAR_HEIGHT, height:0, flexDirection:'row', justifyContent:'space-between', backgroundColor:'black',
            transform: [{ translateY: headerY}]}}>
                <View style={{ width:'20%', backgroundColor:'yellow'}} ref={fromViewSort}>
                <Popover
                    placement = {'bottom'}
                    popoverStyle = {{ elevation:4, padding:10, width:'60%', backgroundColor:colors.TOOLTIP}}
                    backgroundStyle = {{backgroundColor:'transparent'}}
                    fromView = {fromViewSort.current}
                    onRequestClose={() => setToolTipSortVisible(false)}
                    isVisible={toolTipSortVisible}>
                        <View style={{width: '100%'}}>

                            <View style={{ flexDirection: 'row', marginBottom:10 }}>
                                <TouchableOpacity style={{flexDirection:'row', width: '100%', alignItems:'center'}} 
                                    onPress={ () => changeFilter(0)}>
                                    <View style={{width: '80%',justifyContent:'flex-start'}}>
                                        <Text style={styles.labelFilter}>{orderText[0]}</Text>
                                    </View>
                                    <View style={{width: '10%',justifyContent:'center'}}>
                                        <CheckBox containerStyle={styles.checkbox}
                                        checked={order == 0 ? true:false} onPress={ () => changeFilter(0)} checkedColor={colors.PURPLE} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        
                            { searchLocation && searchDistance ? 
                                <View style={{ flexDirection: 'row', marginBottom:10 }}>
                                    <TouchableOpacity style={{flexDirection:'row', width: '100%', alignItems:'center'}} 
                                        onPress={ () => changeFilter(1)}>
                                        <View style={{width: '80%',justifyContent:'flex-start'}}>
                                            <Text style={styles.labelFilter}>{orderText[1]}</Text>
                                        </View>
                                        <View style={{width: '10%',justifyContent:'center'}}>
                                            <CheckBox containerStyle={styles.checkbox}
                                            checked={order == 1 ? true:false} onPress={ () => changeFilter(1)} checkedColor={colors.PURPLE} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                :null
                            }
                            
                            <View style={{ flexDirection: 'row', marginBottom:10 }}>
                                <TouchableOpacity style={{flexDirection:'row', width: '100%', alignItems:'center'}} 
                                    onPress={ () => changeFilter(2)}>
                                    <View style={{width: '80%',justifyContent:'flex-start'}}>
                                        <Text style={styles.labelFilter}>{orderText[2]}</Text>
                                    </View>
                                    <View style={{width: '10%',justifyContent:'center'}}>
                                        <CheckBox containerStyle={styles.checkbox}
                                        checked={order == 2 ? true:false} onPress={ () => changeFilter(2)} checkedColor={colors.PURPLE} />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: 'row', marginBottom:10 }}>
                                <TouchableOpacity style={{flexDirection:'row', width: '100%', alignItems:'center'}} 
                                    onPress={ () => changeFilter(3)}>
                                    <View style={{width: '80%',justifyContent:'flex-start'}}>
                                        <Text style={styles.labelFilter}>{orderText[3]}</Text>
                                    </View>
                                    <View style={{width: '10%',justifyContent:'center'}}>
                                        <CheckBox containerStyle={styles.checkbox}
                                        checked={order == 3 ? true:false} onPress={ () => changeFilter(3)} checkedColor={colors.PURPLE} />
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </Popover>
                </View>
                <View style={{ width:'20%',backgroundColor:'green' }} ref={fromViewPrice}>
                <Popover
                    placement = {'bottom'}
                    popoverStyle = {{ elevation:4, padding:10, width:'100%', backgroundColor:colors.TOOLTIP}}
                    backgroundStyle = {{backgroundColor:'transparent'}}
                    fromView = {fromViewPrice.current}
                    onRequestClose={() => setToolTipPriceVisible(false)}
                    isVisible={toolTipPriceVisible}>
                        <View style={{flex:1}}>
                            <View style={{ flexDirection:'row'}}>
                                <View style={{flex:2}}>
                                    <Input
                                        autoFocus={true}
                                        placeholder="Mínimo $"
                                        keyboardType={'number-pad'} 
                                        returnKeyType={'next'}
                                        maxLength={9}
                                        onSubmitEditing={()=>{ PriceHighInputRef.current.focus() }} 
                                        ref={PriceLowInputRef}
                                        inputContainerStyle={{borderBottomWidth: 0}}
                                        containerStyle={styles.inputBtn}
                                        inputStyle={{fontSize:13, color:colors.TEXT}}
                                        rightIcon={
                                            <TouchableOpacity hitSlop={{top: 20, bottom: 20, left: 50, right: 50}} 
                                                onPress={()=>{setLowPrice(null); PriceLowInputRef.current.clear();}}
                                            >
                                                <Ionicons name="ios-close" color={colors.PURPLE} size={30} />
                                            </TouchableOpacity>
                                        }
                                        onChangeText={ (price)=>{
                                            let validPrice = parseInt(price);
                                            validPrice == 0 ? setLowPrice(null):
                                            setLowPrice(price.replace(/[^0-9]/g, ''))
                                        }}
                                        value={lowPrice}
                                    />
                                </View>
                                <View style={{flex:0.3, justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.label}>a</Text>
                                </View>
                                <View style={{flex:2}}>
                                    <Input
                                        placeholder="Máximo $"
                                        keyboardType={'number-pad'} 
                                        ref={PriceHighInputRef}
                                        maxLength={9}
                                        inputContainerStyle={{borderBottomWidth: 0}}
                                        containerStyle={styles.inputBtn}
                                        inputStyle={{fontSize:13, color:colors.TEXT}}
                                        onSubmitEditing={() => onSubmitPrices() } 
                                        rightIcon={
                                            <TouchableOpacity hitSlop={{top: 20, bottom: 20, left: 50, right: 50}} 
                                                onPress={()=>{setHighPrice(null); setErrorPrice(false); PriceHighInputRef.current.clear();}}
                                            >
                                                <Ionicons name="ios-close" color={colors.PURPLE} size={30} />
                                            </TouchableOpacity>
                                        }
                                        onChangeText={ (price)=>{
                                            let validPrice = parseInt(price);
                                            validPrice == 0 ? setHighPrice(null):
                                            setHighPrice(price.replace(/[^0-9]/g, ''))
                                        }}
                                        value={highPrice}
                                    />
                                </View>
                            </View>
                            <View style={{flex: 1}}>
                                {errorPrice && <Text style={styles.error}>Ingrese un valor superior</Text>}
                            </View>
                        </View>
                </Popover>
                </View>     
            </Animated.View>

            {/**Begin: Parche resultado vacio */}
            <Animated.View style={{
                position:"absolute", 
                transform: [{ translateY: headerY}],
                marginTop:NAVBAR_HEIGHT, 
                paddingBottom:50,backgroundColor:'white', 
                width:'100%', height:'100%', zIndex:(showEmpty ? 3 : -1) }}>
                {refreshing && <View style={{marginTop:22}}>
                    <ActivityIndicator size="large" color={colors.PRIMARY}/>
                </View>}
            </Animated.View>
            {/**End */}

            <View style={{flex: 1 }}>
                <MasonryList
                    images={products}
                    onPressImage={onPressImage}
                    columns={3}
                    onEndReachedThreshold={0.005}
                    onEndReached={addMoreData}
                    spacing={2}
                    imageContainerStyle={{borderRadius:3}}
                    listContainerStyle={{paddingTop:NAVBAR_HEIGHT, paddingBottom:50}}
                    //sorted={true}
                    masonryFlatListColProps = {{
                        refreshControl:(<RefreshControl
                            colors={[colors.PRIMARY]}
                            refreshing={refreshing}
                            progressViewOffset={NAVBAR_HEIGHT}
                            onRefresh={onRefresh}
                        />),
                        contentInset:({ top: NAVBAR_HEIGHT }),//IOS
                        bounces:(false),
                        scrollEventThrottle:(1),
                        onScroll:(Animated.event(
                            [{nativeEvent: {contentOffset: {y: scroll}}}],
                        )), 
                        showsVerticalScrollIndicator:(false)
                    }}
                />
            </View>
            <View styles={styles.activityContainer}>
                {footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    activityContainer: {
        marginTop:20,
        justifyContent: 'center', 
        flexDirection:'row',
        alignItems: 'center',
    },
    input: {
        backgroundColor: 'white',
        borderColor: '#EEEEEE',
        borderWidth:2,
        height: 40,
        padding: 10,
        borderRadius: 4,
        width:'100%',
        color:colors.FILTER_GRAY_DARK
    },
    label: {
		color: colors.TEXT,
		marginVertical: 3,
        fontSize:16,
        fontWeight:'bold'
    },
    labelFilter: {
		color:colors.TEXT, fontSize:14, fontWeight:'bold'
    },
    checkbox:{
        backgroundColor: colors.TOOLTIP, 
        borderColor: colors.FILTER_GRAY, 
        height:20
    },
    filterTouch:{
        flexDirection:'row',
        width: '100%', 
        justifyContent:'center', 
        alignItems:'flex-start'
    },
    inputBtn: {
        backgroundColor: 'white',
        borderColor: '#D6D6D6',
        borderWidth:1,
        height: 40,
        borderRadius: 4,
    },
    error:{
        color:'red',
        fontSize:12,
        marginHorizontal:2,
        textAlign: 'right'
    }
});