import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, AsyncStorage, Alert, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Loader from '../loader/loader';
const { width: winWidth, height: winHeight } = Dimensions.get('window');
const colors = require('../../assets/styles/colors');
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { Input } from 'react-native-elements';


export default function FilterCategoryScreen() {
    const navigation = useNavigation();

    const [location, setLocation] = useState(null);
    const [showLoader, setShowLoader] = useState(false);

   
    navigation.setOptions({
        title: 'Ubicación',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            (async () => {
                let filterStorage = await AsyncStorage.getItem('productAddress');
                setLocation(JSON.parse(filterStorage));
            })();

            (async () => {
                let filterDistance = await AsyncStorage.getItem('filterDistance');
                if(filterDistance){
                    setDistance(filterDistance);
                }
                    
            })();
        });

        return focused;

    }, []);

    //Distancia por defecto
    const [distance, setDistance] = useState('4');
    const [availableDistance, setAvailableDistance] = useState({
        0: '5 Kilómetros',
        1: '10 Kilómetros',
        2: '15 Kilómetros',
        3: '20 Kilómetros',
        4: '30 Kilómetros',
        5: 'Más lejano',
    });

    const apply = () => {
        if(location && distance){
            //Realizar busqueda en el home
            const items = [['doSearch','1'],['filterLocation', JSON.stringify(location)], ['filterDistance', distance]]
            AsyncStorage.multiSet(items, () => {
                navigation.goBack();
            });
        }else{
            navigation.goBack();
        }
        
    }

    return (
        <SafeAreaView style={{ flex: 1, paddingTop:10,  backgroundColor: 'white', }}>
            <View style={styles.container}>
                <Text style={styles.label}>UBICACIÓN</Text>
                <Input
                    inputContainerStyle={{borderBottomWidth: 0}}
                    containerStyle={styles.inputBtn}
                    inputStyle={{fontSize:13, color:colors.TEXT}}
                    editable={false}
                    rightIcon={
                        <TouchableOpacity  onPress={() => { navigation.navigate('Location') }}>
                            <MaterialIcons name="location-on" color={colors.PURPLE_BTN} size={25} />
                        </TouchableOpacity>
                    }
                    value={(location ? location.address:'')}
                />
                
                <Text style={styles.label}>DISTANCIA</Text>
                <Input
                    inputContainerStyle={{borderBottomWidth: 0}}
                    containerStyle={styles.inputBtn}
                    inputStyle={{fontSize:13, color:colors.TEXT}}
                    editable={false}
                    value={(availableDistance[parseInt(distance)])}
                />
            </View>
         
            <View style={styles.containerOptions}>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('0')}>
                        <MaterialIcons name="location-on" style={{marginTop:20}} color="white" size={50} />
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>MENOS DE 5 KILÓMETROS</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('1')}>
                        <Text style={styles.distanceText}>10</Text>
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>10 KILÓMETROS</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('2')}>
                        <Text style={styles.distanceText}>15</Text>
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>15 KILÓMETROS</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.containerOptions}>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('3')}>
                        <Text style={styles.distanceText}>20</Text>
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>20 KILÓMETROS</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('4')}>
                        <Text style={styles.distanceText}>30</Text>
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>30 KILÓMETROS</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option} onPress={()=>setDistance('5')}>
                        <Ionicons name="md-car" style={{marginTop:20}} color="white" size={50} />
                        <View style={styles.labelbox}>
                            <Text style={styles.labelText}>MÁS LEJANO</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} underlayColor='#99d9f4' onPress={()=>apply()}>
                    <Text style={styles.buttonText}>APLICAR</Text>
                </TouchableOpacity>
            </View>
            <Loader loading={showLoader} />
        </SafeAreaView>

    );
}

const styles = StyleSheet.create({
    containerOptions: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent:'center',
        height:140,
    },
    box: {
        padding:4,
        width: (winWidth-10)/3
    },
    option:{
        alignItems: 'center',
        borderTopLeftRadius:5,
        borderTopRightRadius:5,
        height:120,
        backgroundColor:colors.PURPLE, 
    },
    labelbox:{
        position: 'absolute',
		left: 0,
		right: 0,
		bottom: 8,
		height: 30,
        backgroundColor:colors.LABEL_BOX,
        alignItems: 'center',
        justifyContent:'center',
    },
    labelText:{
        color:colors.TEXT,
        fontSize:12,
        textAlign:'center'
    },
    distanceText:{
        marginTop:10, 
        color:'white', 
        fontSize:50, 
        fontWeight:'bold'
    },
    container: {
        padding: 10,
    },
    label: {
		color: colors.TEXT,
		marginVertical: 3,
        fontSize:16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        padding:10,
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 5
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation:2
    },
    inputBtn: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        borderRadius: 4,
    },
});
