import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TouchableOpacity, FlatList, RefreshControl, ImageBackground, Alert} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function SaveToBoard({ route }) {

    const navigation = useNavigation();

    //Recibir id de producto de detalle producto,..
    const { idproduct } = route.params;
    const { uri } = route.params;

    navigation.setOptions({
        title: 'Guardar en tablero',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })
    
    const [user , setUser ] = useState('');
    const [isShow , setisShow ] = useState(false);
    const [boards , setBoards ] = useState(null);
    const [refreshing , setRefreshing ] = useState(true);
    const [favorite , setFavorite ] = useState(0);


    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            if (user) {
                getBoards();
            }
        });

        if (user) {
            getBoards();
        }
        return focused;
		
    },[user]);


    useEffect(() => {
        getStorage();
    },[]);

    //Obtener Tableros
    const getBoards = async () => {
        
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-boards-user-check/';

        axios.post(url,{
            iduser: user._id,
            idproduct: idproduct
        })
        .then((response) => {
            if(response.data.status){
                setRefreshing(false);
                setBoards(response.data.boards)
                setFavorite(response.data.countInboard)
            }else{
                //Cancelar loading
                setRefreshing(false);
            }

        })
        .catch((error) => {
            //Cancelar velo de loading
            console.log(error)
            setisShow(false);
            Alert.alert('Error de red, intente nuevamente')
        })

    };

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    //Restar favorito del asyncStorage
	const removeFavorite = async () => {
        if(favorite>0){
            let countInboard = favorite-1;
            await AsyncStorage.setItem('favorite', countInboard.toString())
        }
    };

    //Sumar favorito del asyncStorage
	const addFavorite = async () => {
        let countInboard = favorite+1;
        await AsyncStorage.setItem('favorite', countInboard.toString())
    };

    const saveOrRemoveItem = (idboard, check) =>{
       
        //construir ruta de servidor insertar
        const url = config.BASE_URL_API+'api/v1/insert-product-board/';
        //construir ruta de servidor eliminar
        const urlRemove = config.BASE_URL_API+'api/v1/delete-product-board/';

        //Loading
        setisShow(true);

        //Si el producto no esta en el board se guarda
        //de lo contrario se elimina del board
        if(!check){
            //Insertar producto en board 
            axios.post(url, {
                idBoard: idboard,
                idProduct: idproduct,
            }).then((response) => {
                setisShow(false);
                if(response.data.status){
                    addFavorite();
                    navigation.goBack()
                }else{
                    Alert.alert('Error de red, intente nuevamente');
                }      
            })
            .catch((error) => {
                console.log(error)
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente');
            })
        }else{
            //Eliminar producto de board
            axios.post(urlRemove, {
                idboard: idboard, //Enviar id de usuario para filtrar
                idproduct: idproduct,
                iduser: user._id
            }).then((response) => {
                setisShow(false);
                if(response.data.status){
                    removeFavorite();
                    navigation.goBack()
                }else{
                    Alert.alert('Error de red, intente nuevamente');
                }      
            })
            .catch((error) => {
                console.log(error)
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente');
            })
        }
        
    }

    const renderItem = ({ item }) => {
		const boardname = `${item.name}`;
        const idboard = `${item._id}`;
        const check = item.check;
        return (
            <View>
                <TouchableOpacity onPress={ () => saveOrRemoveItem(idboard, check)}>
                    <View style={styles.containerProd}>
                        <View style={styles.board}>
                            {item.board_image ? (
                                <ImageBackground source={{ uri: config.BASE_URL_API+item.board_image }} style={styles.img} ></ImageBackground>
                            ) : (
                                <ImageBackground source={require('../../assets/images/gray_square.jpg')}  style={styles.img} ></ImageBackground>
                                
                            )}
                            <Text numberOfLines={1} style= {styles.TextStyle} >{boardname}</Text>
                        </View>
                        {
                            check && <Ionicons name="ios-checkmark" style= {styles.icon} size={50}/>
                        }  
                    </View>
                </TouchableOpacity>
                <View style={{borderWidth: 1, borderColor: colors.GRAY_ALT}}></View>
            </View>	
        )
    };

    //Refrescar el FlatList
	const onRefresh = () => {
        setBoards (null);
		//Obtener data nuevamente
		getBoards();
	}

    return (
        <View style={styles.container}>
            <Loader loading={isShow} />
            <TouchableOpacity style={styles.button} onPress={() => {navigation.navigate("SaveBoardProduct",{ idproduct, uri })}}>
			    <Ionicons name="md-add-circle" style= {styles.icon} size={30}/>
			    <Text style= {styles.TextStyle}>Crear nuevo tablero</Text>
		    </TouchableOpacity>
            <View style={ styles.separator }/> 
            <FlatList
				data={boards}
				enableEmptySections={true}
				keyExtractor={item => String(item._id) }
				renderItem={renderItem}
				//refresh control para recargar pantalla
				refreshControl={
					<RefreshControl
					refreshing={refreshing}
					onRefresh={onRefresh}
					/>
				}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    button:{
        flexDirection: 'row',
        padding:10
    },
    TextStyle: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontWeight:'bold',
        fontSize:16,
    },
    icon:{
        alignItems:'center',
        justifyContent: 'center',
        color: colors.PURPLE , 
        width: 40,  
        height: 40  
    },
    separator:{
		borderTopWidth: 2,
		borderTopColor: colors.GRAY_ALT
    },
    img: {
		justifyContent: 'flex-end', 
		width: 62,
		height: 62
	},
    viewsproduct:{
		color: colors.GRAY_lyrics,
		marginLeft: 3,
		zIndex:1,
    },
    containerProd: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10
    },
    board:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        justifyContent:'center'
    },
    loading: {
		flex: 1,
		alignItems: 'center',
        flexDirection: 'column',
        position: "absolute",
    },
    activityIndicatorWrapper: {
		height: 100,
		width: 100,
		borderRadius: 10,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-around'
	},

});
