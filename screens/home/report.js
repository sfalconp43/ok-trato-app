import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, TouchableHighlight , ImageBackground } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function Report({ route }) {

    //obtener parametros iduser cliente
    const { iduser } = route.params;
    //obtener parametros id producto
    const { idproduct } = route.params;
    //obtener nombre del usuario
    const { names } = route.params;
    //titulo de producto
    const { title } = route.params;
    //obtener uri imagen de producto o usuario
    const { uri } = route.params;
    //tipo de reporte
    const { type } = route.params;

    const navigation = useNavigation();

    navigation.setOptions({
        title: type == 0 ? 'Reportar usuario':'Reportar producto',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })
    
    const [isShow , setisShow ] = useState(false);
    const [category , setCategory ] = useState([]);

    useEffect(() => {
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getCategories();
        });

        // Unmount
        return focused;

    }, []);
    
     // Obtener data de usuario del AsyncStorage
	const getCategories = async () => {

        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-report-categories/';

        setisShow(true);
        axios.post(url, { type: type })
        .then((response) => {
            //Cancelar velo de loading
            setisShow(false);
            if(response.data.status){
                setCategory(response.data.reportCategory);
            }else{
                Alert.alert('Error consultando categorias');
            }
        })
        .catch((error) => {
            console.log(error);
            //Cancelar velo de loading
            setisShow(false);
            Alert.alert('Error de red, intente nuevamente')
        })
    };

    //Ir al screen de report de usuario
    const confirmReport = (idreportcat) =>{
        navigation.navigate('SendReport',{ iduser, idproduct, names, title, uri, type, idreportcat})
    }
    

    //Listado de categorias
	const renderItem = ({ item }) => {
        const description = item.description;
        const idreportcat = item._id;
		return (
			<View>
			<TouchableHighlight underlayColor="#DDDDDD" style={styles.options}  onPress={()=>confirmReport(idreportcat)}>
				<View>
					<Text numberOfLines={1} style={styles.TextOption}>{description} </Text>
				</View>
			</TouchableHighlight >
			</View>
		);
    };

    return (
        <SafeAreaView style={styles.container}>
            <Loader loading={isShow} />
            <View style={styles.containerData}>
                <View style={styles.data}>
                <ImageBackground source={{ uri: uri }} style={styles.img} />
                {type == 0 ? (
                        <View> 
                            <Text numberOfLines={1} style= {styles.TextStyle} >{names}</Text>
                        </View>
                    ) : (
                        <View> 
                            <Text numberOfLines={1} style= {styles.ProductText} >{title}</Text>
                            <Text numberOfLines={1} style= {styles.TextStyle} >vendido por {names}</Text>
                        </View> 
                    )
                } 
                </View>
            </View>
            <Text style={styles.question}>{ type == 0 ? '¿Porqué deseas reportar este usuario?':'¿Porqué deseas reportar este producto?'}</Text>
            <FlatList
                data={category}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
    },
    containerData: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom:20,
    },
    data:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        justifyContent:'center'
    },
    img: {
		justifyContent: 'flex-end', 
		width: 40,
		height: 40
    },
    TextStyle: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontSize:16,
    },
    ProductText:{
        color: colors.TEXT,
        marginLeft: 5,
        fontSize:16,
        fontWeight:'bold' 
    },
    question:{
        color: colors.TEXT,
		marginVertical: 15,
        marginLeft: 5,
        fontSize:16,
        fontWeight:'bold'
    },
    options:{
        marginVertical:15
    },
    TextOption: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontSize:16,
    },
});
