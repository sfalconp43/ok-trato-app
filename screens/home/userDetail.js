import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image, Alert } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import { LinearGradient } from 'expo-linear-gradient';
const { width: winWidth } = Dimensions.get('window');
import axios from 'axios';
import moment from 'moment';
import { Ionicons } from '@expo/vector-icons';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Seller from './sellerProducts'; 

export default function UserProfile({ route, navigation }) {

    //obtener parametros iduser cliente
    const { id } = route.params;

    navigation.setOptions({
        title: '',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerTransparent: true,
        headerRight: () => (
            <View style={styles.iconContainer}>
                <TouchableOpacity onPress={ReportUser}>
                    <MaterialCommunityIcons name="flag-variant" color="white" size={25} />
                </TouchableOpacity> 
            </View>
        ),
    })
    
    const [user , setUser ] = useState([])
    const [isShow , setisShow ] = useState(false);
    const [screen, setScreen] = useState({
        index: 0,
        routes: [
            { key: 'profile', title: 'PERFIL' },
            { key: 'offers', title: 'MIS OFERTAS' },
        ]
    });

    //Ir al screen de report de usuario
    const ReportUser = () =>{
        navigation.navigate('Report',{ iduser:id, idproduct: null, names:user.names, title:null, uri: config.BASE_URL_API+'users/'+user.image, type: 0})
    }

    useEffect(() => {
      
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            //obtener usuario y productos
            geUserDetails();
        });

        // Unmount
        return focused;

    }, []);

    //obtener detalle de usuario y producto
    const geUserDetails = async () => {
        //Mostrar velo de loading
        setisShow(true);
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-user-data/';

        axios.post(url, {
            iduser: id,
        })
        .then((response) => {
            //Cancelar velo de loading
            setisShow(false);

            if(response.data.status){
                setUser(response.data.user);
                setScreen({
                    index: 0,
                    routes: [
                        { key: 'profile', title: 'PERFIL' },
                        { key: 'offers', title: `MIS OFERTAS (${response.data.countProducts})` },
                    ]
                });
            }else{
                Alert.alert('Error consultando detalle');
            }
        })
        .catch((error) => {
            console.log(error);
            //Cancelar velo de loading
            setisShow(false);
            Alert.alert('Error de red, intente nuevamente')
        })
    };

    //Datos Confirmados
    const ScreenProfile = () => {
        return (
            <View style={styles.MainContainer}>
                <View style={{ flexDirection: 'row' }}>
                    {user.emailchecked && 
                        <View style={{ flexDirection: 'column', marginHorizontal:5, alignItems:'center',justifyContent: 'center' }}>
                            <View style={{width: 30, height: 30, borderRadius: 25, backgroundColor: colors.PURPLE, alignItems:'center',justifyContent: 'center'}}>
                                <Ionicons name="md-mail" color="white" size={15} />
                            </View >
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>Email</Text>
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>confirmado</Text>
                        </View>  
                    }
                    {user.facebookchecked && 
                        <View style={{ flexDirection: 'column', marginHorizontal:5, alignItems:'center',justifyContent: 'center' }}>
                            <View style={{width: 30, height: 30, borderRadius: 25, backgroundColor: colors.PURPLE, alignItems:'center',justifyContent: 'center'}}>
                                <Ionicons name="md-mail" color="white" size={15} />
                            </View >
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>Email</Text>
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>confirmado</Text>
                        </View>
                    }
                    {user.phoneNumberChecked &&
                        <View style={{ flexDirection: 'column', marginHorizontal:5, alignItems:'center',justifyContent: 'center' }}>
                            <View style={{width: 30, height: 30, borderRadius: 25, backgroundColor: colors.PURPLE, alignItems:'center',justifyContent: 'center'}}>
                                <Ionicons name="md-phone-portrait" color="white" size={15} />
                            </View >
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>Teléfono</Text>
                            <Text style={{ textAlign: 'center', color:colors.TEXT }}>confirmado</Text>
                        </View>
                    }  
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <Loader loading={isShow} />
            <View style={{ marginBottom: 10 }}>
                <View style={styles.banner}>   
                    {user.banner && <Image source={{ uri: config.BASE_URL_API+'covers/'+user.banner }} style={{ height: '100%', width:'100%' }} /> }  
                    <LinearGradient colors={['rgba(0,0,0,0.8)', 'transparent']} style={styles.gradient} />        
                </View>
                <View style={styles.image}>  
                    {user.image &&
                        <Image source={{ uri: config.BASE_URL_API+'users/'+user.image }} style={{ width: 130, height: 130, borderRadius: 130/2 }} />}  
                </View>
                <View style={styles.space}/>
                <View style={{ justifyContent: 'center', alignItems:'center'}}>
                    {user.names &&  <Text style={styles.name}>{ user.names+' '+user.lastnames  }</Text>}
                    {user.createdate &&  <Text style={styles.joined}>Se unió: {moment(user.createdate).format('MMMM, YYYY')}</Text>}
                </View>
            </View>
            <TabView
                renderTabBar={props =>
                    <TabBar {...props} 
                    indicatorStyle={styles.indicator}
                    activeColor={colors.PRIMARY}
                    inactiveColor={colors.PURPLE}
                    style={styles.tabbar}
                    tabStyle={styles.tab}
                    labelStyle={styles.label}
                    />}
                navigationState={screen}
                renderScene={SceneMap({
                    profile: ScreenProfile,
                    offers: Seller,
                    })}
                    onIndexChange={index => setScreen({ ...screen, index })}
                    initialLayout={{width: Dimensions.get('window').width}}
                />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    banner:{
        height:150,
        backgroundColor: colors.PRIMARY,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    image:{
        elevation: 7, 
        width: 140, 
        height: 140, 
        borderRadius: 70,  
        backgroundColor: 'white', 
        position: 'absolute', 
        top: 90,              
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center'
    },
    space:{
        height:90
    },
    iconContainer: {
		flexDirection: "row",
		justifyContent: "space-evenly",
		width: 60
    },
    MainContainer: {
        flex: 1,
        padding:20,
    },
    gradient:{
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 120,
    },
    name:{
        color:colors.PURPLE,
        fontSize:25
    },
    joined:{
        color:colors.TEXT,
        fontSize:14
    },
    scene: {
        flex: 1,
    },
    tabItem: {
		flex: 1,
		alignItems: 'center',
		padding: 16,

	},
	tabbar: {
		backgroundColor: 'white',
	},
	tab: {
		width: winWidth/2,
	},
	indicator: {
		backgroundColor: colors.PRIMARY,
	},
	label: {
		fontWeight: '400',
	},
});
