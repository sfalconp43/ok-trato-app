import React, { useState, useEffect } from 'react';
import { StyleSheet, View, AsyncStorage, Text, Alert, TextInput, TouchableOpacity, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from 'react-hook-form';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import { useHeaderHeight } from '@react-navigation/stack';
//loader
import Loader from '../loader/loader'

export default function BoardsCreateScreen({ route }) {

    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    //Recibir id de producto de detalle producto,..
    const { idproduct } = route.params;
    const { uri } = route.params;

    navigation.setOptions({
        title: 'Crear Tablero',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

   
    const [isShow , setisShow ] = useState(false)
    const [user , setUser ] = useState('')

    // Obtener data de usuario del AsyncStorage
	const getUserStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    }

    useEffect(() => {
        getUserStorage()
    },[])

    //Sumar favorito del asyncStorage
	const addFavorite = async () => {
        let countInboard = 1;
        await AsyncStorage.setItem('favorite', countInboard.toString())
    };

    const onSubmit = async data => { 
        
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/create-board-product';

        if (data) {
            //Mostrar velo de loading
            setisShow(true);

            axios.post(url, {
                name: data.name,
                iduser: user._id,
                idproduct
            }).then((response) => {
                //Cancelar velo de loading
                setisShow(false)
                if(response.data.status){
                    addFavorite();
                    navigation.goBack();
                }else{
                    Alert.alert('Ocurrio un error, intente nuevamente')
                }
            })
            .catch((error) => {
                console.log(error);
                //Ocultar velo de loading
                setisShow(false)
                Alert.alert('Ha ocurrido un error con la creacion del tablero')
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    }

    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding"
            keyboardVerticalOffset={
                Platform.OS === "ios" ? headerHeight : (headerHeight)
            }>
            <Loader loading={isShow} />
            <View style={styles.containerProd}>
                <View style={styles.board}>
                    <ImageBackground source={{ uri: uri }} style={styles.img} />
                    <Text numberOfLines={1} style= {styles.TextStyle} >Guardar producto en:</Text>
                </View>
            </View>
            <Text style={styles.label}>Nombre del Tablero</Text>
            <Controller
                as={<TextInput style={styles.input}/>}
                control={control}
                name="name"
                onChange={onChange}
                rules={{ required: true }}
                defaultValue=""
            />
            {errors.name && <Text>Por favor ingrese el nombre del tablero.</Text>}
           
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Guardar</Text>
                </TouchableOpacity>
            </View>
           
           
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 5
    },
    error:{
        color:'red'
    },
    label:{
        marginTop:4,
        color:colors.TEXT
    },
    containerProd: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
    },
    board:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        justifyContent:'center'
    },
    TextStyle: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontSize:14,
    },
    img: {
		justifyContent: 'flex-end', 
		width: 40,
		height: 40
	},
});

