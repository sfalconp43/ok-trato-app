import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, TouchableOpacity, AsyncStorage, Alert, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import Loader from '../loader/loader';
const { width: winWidth, height: winHeight } = Dimensions.get('window');
const colors = require('../../assets/styles/colors');
const config = require('../../config/config');
import { Ionicons, MaterialIcons } from '@expo/vector-icons';


export default function FilterCategoryScreen() {
    const navigation = useNavigation();

    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState(null);
    const [showLoader, setShowLoader] = useState(false);

    navigation.setOptions({
        title: 'Categorías',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {

        (async () => {
            let filterStorage = await AsyncStorage.getItem('filterCategory');
            setCategory(JSON.parse(filterStorage));
        })();

        setShowLoader(true);

        axios.get(`${config.BASE_URL_API}api/v1/get-categories/`)
            .then((response) => {
                setCategories(response.data.categories);
                setShowLoader(false);
            })
            .catch((error) => {
                setShowLoader(false);
                Alert.alert(
                    'Eror',
                    'Ha ocurrido un error al obtener las categorías de los productos',
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
            })
    }, []);

    let selectCategory = async (category) => {
        let categoryAux = {
            _id: category._id,
            name: category.name,
        }
        try {
            //Realizar busqueda en el home
            const items = [['doSearch', '1'],['findCategory', '1'],['filterCategory', JSON.stringify(categoryAux)]]
            AsyncStorage.multiSet(items, () => {
                navigation.goBack();
            });
        } catch (error) {
            console.log(error);
        }
    }

    let renderCategory = (item) => {

        if (category) {
            if (category._id == item._id) {
                return (
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                        onPress={() => { selectCategory(item) }}
                    >
                        <View style={{ backgroundColor: colors.PURPLE, height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', marginTop: 10, borderRadius: 20 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                );
            } else {
                return (
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                        onPress={() => { selectCategory(item) }}
                    >
                        <View style={{ backgroundColor: 'white', height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: colors.PURPLE, marginTop: 10, marginBottom: 1, borderRadius: 20 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        } else {
            return (
                <TouchableOpacity
                    style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                    onPress={() => { selectCategory(item) }}
                >
                    <View style={{ backgroundColor: 'white', height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: colors.PURPLE, marginTop: 10, marginBottom: 1, borderRadius: 20 }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>{item.name}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
    }

    let allCategory = () => {
        const items = [['doSearch', '1'],['AllCategory', '1']]
        AsyncStorage.multiSet(items, () => {
            AsyncStorage.removeItem('filterCategory');
            navigation.goBack();
        });
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:'white' }}>
            {
            /** 
            <View style={styles.containerOptions}>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option}>
                        <Ionicons name="md-search" style={{marginTop:20}} color="white" size={50} />
                        <View style={styles.label}>
                            <Text style={styles.labelText}>BUSCAR DE TODO</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option}>
                        <MaterialIcons name="stars" style={{marginTop:20}} color="white" size={50} />
                        <View style={styles.label}>
                            <Text style={styles.labelText}>LO MÁS POPULAR</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.option}>
                        <MaterialIcons name="location-on" style={{marginTop:20}} color="white" size={50} />
                        <View style={styles.label}>
                            <Text style={styles.labelText}>CERCA DE TI</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            */}
            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}  onPress={() => { allCategory() }}>
                <View style={{ flexDirection:'row', backgroundColor: 'white', height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: colors.PURPLE, marginTop: 10, marginBottom: 1, borderRadius: 20 }}>
                    <Ionicons name="md-search" style={{ marginRight:10 }} color={colors.PURPLE} size={25} />
                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>Buscar de todo </Text>
                </View>
            </TouchableOpacity>
            <FlatList
                data={categories}
                renderItem={({ item }) => renderCategory(item)}
                keyExtractor={item => item._id}
            />
            <Loader loading={showLoader} />
        </SafeAreaView>

    );
}

const styles = StyleSheet.create({
    containerOptions: {
        marginTop:10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent:'center',
        height:140,
        marginBottom:10,
    },
    box: {
        padding:4,
        width: (winWidth-10)/3
    },
    option:{
        alignItems: 'center',
        borderTopLeftRadius:5,
        borderTopRightRadius:5,
        height:120,
        backgroundColor:colors.PURPLE, 
    },
    label:{
        position: 'absolute',
		left: 0,
		right: 0,
		bottom: 8,
		height: 30,
        backgroundColor:colors.LABEL_BOX,
        alignItems: 'center',
        justifyContent:'center',
    },
    labelText:{
        color:colors.TEXT,
        fontSize:12
    }

});
