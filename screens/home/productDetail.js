import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Alert, ImageBackground, TouchableOpacity, Image, SafeAreaView, ScrollView, TextInput,ActivityIndicator } from 'react-native';
import axios from 'axios';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';
import TimeAgo from 'react-native-timeago';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function ProductDetail({ route }) {

    const navigation = useNavigation();

    //Recibir id de producto de home,..
    const { idproduct } = route.params;

    navigation.setOptions({
        title: '',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerTransparent: true,
        headerRight: () => (
            <View style={styles.iconContainer}>
				<TouchableOpacity>
					<Ionicons name="md-share" color="white" size={25} />
				</TouchableOpacity>
                <TouchableOpacity onPress={()=>{user ? navigation.navigate('SaveToBoard',{idproduct: product._id, uri: product.URI}): navigation.navigate('User')}} >
                    {/** Si esta en uno o mas tableros */}
                    {favorite > 0 ?
                        (<Ionicons name="ios-heart" color="red" size={25} />):
                        (<Ionicons name="ios-heart-empty" color="white" size={25} />)
                    }
                </TouchableOpacity> 
            </View>
          ),
    })
    
    const [user , setUser ] = useState('')
    const [isShow , setisShow ] = useState(false)
    const [product , setProduct ] = useState(false)
    const [favorite , setFavorite ] = useState(0);
    const [images, setImages] = useState(null);
    const [imageSelected, setImageSelected] = useState(null);

    const [condition, setCondition] = useState({
        1: 'Otro',
        2: 'Para reparación',
        3: 'Usado',
        4: 'Caja Abierta (nunca usado)',
        5: 'Reacondicionado/Certificado',
        6: 'Nuevo',
    })

    useEffect(() => {
        getProductDetail();
    }, []);

    useEffect(() => {
        AsyncStorage.removeItem('favorite');
        //al hacer foco sobre la pantalla verificar si esta en favoritos
        const focused = navigation.addListener('focus', () => {
            getDataFavorite()
        });

        return focused;
		
    },[]);

    //Obtener data de favoritos
    //Si el producto esta en uno o mas tableros
    const getDataFavorite = async() =>{
        const countInboard = await AsyncStorage.getItem('favorite');
        if(countInboard){
            setFavorite(countInboard);
        }
    }

    //obtener detalle de producto
    const getProductDetail = async () => {

        //obtener usuario
        const userData = await AsyncStorage.getItem('userData');
        let userfield = null;
        if(userData){
            setUser(JSON.parse(userData));
            userfield = JSON.parse(userData);
        }

        let data = {
            idProduct: idproduct,
        }

        if (userfield){
            data.iduser = userfield._id;
        }

        //Mostrar velo de loading
        setisShow(true);

        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/find-product-detail/';
        axios.post(url, data)
        .then((response) => {
            //Cancelar velo de loading
            setisShow(false);

            if(response.data.status){
                //console.log(response.data.product)
                setProduct(response.data.product);
                setImages(response.data.product.images);
                setImageSelected(0);
                
                //En cuantos tableros esta el producto ->
                setFavorite(response.data.countInboard);
            }else{
                Alert.alert('Error consultando detalle');
            }
        })
        .catch((error) => {
            console.log(error);
            //Cancelar velo de loading
            setisShow(false);
            Alert.alert('Error de red, intente nuevamente')
        })
    };

    //Navegar a chat de pregunta
	const Chat = () => {
        if(user){

            //informacion del vendedor
            let userdata = {
                id: product.iduser._id,
                image: product.iduser.image,
                names: product.iduser.names,
                lastnames: product.iduser.lastnames,
                imageproduct: images[0],
                price: product.price,
                productaddress: product.productaddress
            }

            navigation.navigate('Chat', { product, userdata });
        }else{
            navigation.navigate('User');
        }
    }
    
    //Navegar al perfil del usuario (vendedor)
    //id: id de usuario cliente
	const userProfile = async (id) => {
        if(user){
            await AsyncStorage.setItem('idseller', id.toString())
            navigation.navigate('UserDetail', { id });
        }else{
            navigation.navigate('User');
        }
       
    }

    //Ir al screen de report de usuario
    const ReportProduct = () =>{
        if(user){
            navigation.navigate('Report',{ iduser:product.iduser._id, idproduct: idproduct, names:product.iduser.names, title:product.title, uri: product.URI, type: 1})
        }else{
            navigation.navigate('User');
        }
        
    }

    const MakeOfferOrBuy = () =>{
        if(user){
            //TODO:Hacer oferta o comprar
        }else{
            navigation.navigate('User');
        }
    }

    const [imageLoaded, setImageLoaded] = useState(true);

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
                <Loader loading={isShow} />
                <View style={ styles.image }>
                    {images &&
                        <ImageBackground onLoadEnd={()=>setImageLoaded(false)} source={{uri:`${config.BASE_URL_API}${images[imageSelected]}`}} style={ styles.imageContainer }>
                            <View style={styles.price}>
                                {product ? <TextInput style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{`$${product.price}`}</TextInput>:null}
                                
                            </View>
                            { imageLoaded && <ActivityIndicator  size="large" color={colors.PURPLE}
                                style={{ top:110, height: 50, width: 100,  justifyContent: 'center', alignItems: 'center', alignSelf:'center',position: 'absolute'}}/>
                            }
                        </ImageBackground>
                    }
                    <LinearGradient colors={['rgba(0,0,0,0.5)', 'transparent']} style={styles.gradient} />
                </View>
                {/* Listado de imagenes */}
                {product ? 
                    <ScrollView horizontal={true} style={{marginTop:10, marginBottom:5}} showsHorizontalScrollIndicator={false}>
                        {product.images.map((image, index) => {
                            return (
                                <View key={index}>
                                    <TouchableOpacity style={styles.galleryImageContainer} onPress={()=>setImageSelected(index)}>
                                        <Image source={{ uri: `${config.BASE_URL_API}${image}` }} style={styles.galleryImage}/>
                                    </TouchableOpacity>
                                </View>
                            )
                        })}
                    </ScrollView>:null
                }
                {/* Titulo, dirección y fecha */}
                <View style={ styles.product }>
                    <Text style={ styles.title }>{product.title}</Text>
                    <Text style={ styles.subText }>{product.productaddress}</Text>
                    {product.idcategory ? 
                    <Text style={ styles.subText }>Publicado <TimeAgo style={styles.messagedate} time={product.createdate} interval={20000}/> en {product.idcategory.name}</Text>
                    :null}
                    
                </View>
                <View style={ styles.separator }/> 
                {/* Imagen de usuario, nombre y puntuacion */}
                <View>
                    { product.iduser ? 
                        <TouchableOpacity onPress={()=>userProfile(product.iduser._id)} >
                            <View style={styles.row}>
                                <View style={[styles.box, styles.box2]}>
                                    <Image source={{ uri: config.BASE_URL_API+'users/'+ product.iduser.image }} style={styles.profileImg}/>
                                </View>
                                <View style={[styles.box, styles.two]}>
                                    <Text style={ styles.userName }>{ product.iduser.names }</Text>
                                    {/** TODO: consultar puntuacion de usuario */}
                                    <View style={{flexDirection: "row"}}>
                                        <MaterialIcons name="star" color={colors.PRIMARY} size={20} />
                                        <MaterialIcons name="star" color={colors.PRIMARY} size={20} />
                                        <MaterialIcons name="star" color={colors.PRIMARY} size={20} />
                                        <MaterialIcons name="star" color={colors.PRIMARY} size={20} />
                                        <MaterialIcons name="star" color={colors.PRIMARY} size={20} />
                                    </View>
                                </View>
                                <View style={[styles.box, styles.box3]}>
                                    <Ionicons name="ios-arrow-forward" color={colors.PURPLE} size={25} />
                                </View>
                             </View>
                        </TouchableOpacity>
                    :null
                    }
                </View>
                <View style={ styles.separator }/> 
                {/* Descripción del producto */}
                <View style={ styles.description }>
                    <Text style={ styles.descriptionText }>Descripción</Text>
                    <Text style={ styles.subText }>{condition[product.condition]}</Text>
                    <Text style={ styles.subText }>{product.description}</Text>
                </View>
                <View style={ styles.separator }/> 
                <View style={styles.containerButons}>
                    <View style={{ width:'33.33%', marginRight: 4 }}>
                        <TouchableOpacity style={styles.button} onPress={Chat}> 
                            <MaterialIcons name="chat" color={colors.PRIMARY} size={16}/>
                            <Text style={styles.buttonText}> Chat </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width:'33.33%', marginRight: 4 }}>
                        <TouchableOpacity style={styles.button} onPress={ReportProduct}> 
                            <MaterialIcons name="flag" color={colors.PRIMARY} size={16}/>
                            <Text style={styles.buttonText}> Reportar </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width:'33.33%' }}>
                        <TouchableOpacity style={styles.button} onPress={MakeOfferOrBuy}> 
                            <Text style={styles.buttonText}> Hacer oferta </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    scrollView: {
        backgroundColor: '#fff',
    },
    image:{
		width: '100%',
        height : 300,
        backgroundColor:colors.PRIMARY,
    },
    imageContainer:{
		width: '100%', height: '100%',
		resizeMode: 'contain',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation:2
    },
    containerButons: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		padding:10,
    },
    gradient:{
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 200,
    },
    product:{
		padding:10
    },
    title:{
        fontSize:25,
        fontWeight: 'bold'
    },
    subText:{
        color:colors.TEXT,
    },
	profileImg:{
		width:80,
		height:80,
		borderRadius: 40
	},
    userName:{
		fontSize:16,
		fontWeight:'bold'
    },
    separator:{
		borderTopWidth: 3,
		borderTopColor: colors.GRAY_ALT
    },
    description:{
		padding:10
	},
	descriptionText:{
		fontSize:16,
		fontWeight:'bold'
    },
    iconContainer: {
		flexDirection: "row",
		justifyContent: "space-evenly",
		width: 120
    },
    box: {
        flex: 1,
    },
    box2: {
        justifyContent:'center',
        alignItems:'center',
        padding:10
    },
    box3: {
        alignItems:'flex-end',
        justifyContent:'center',
        alignContent:'flex-end',
        padding:10
    },
    two: {
        flex: 2,
        padding:5
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    price:{
        height: 30, 
        width: 100, 
        backgroundColor: colors.PURPLE, 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderRadius: 20, 
        position: 'absolute', 
        bottom: 10, 
        left: 5,
        elevation:2
    },
    galleryImageContainer: {
        width: 75,
        height: 75,
        marginLeft: 10,
        borderRadius: 10
    },
    galleryImage: {
        width: 75,
        height: 75,
        borderRadius: 10,
    }
});
