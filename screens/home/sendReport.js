import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ImageBackground, KeyboardAvoidingView, Alert, AsyncStorage } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import { useHeaderHeight } from '@react-navigation/stack';
import { useForm, Controller } from 'react-hook-form';
import { ConfirmDialog } from 'react-native-simple-dialogs';

export default function Report({ route }) {

    //obtener parametros iduser cliente
    const { iduser } = route.params;
    //obtener parametros id producto
    const { idproduct } = route.params;
    //obtener nombre del usuario
    const { names } = route.params;
    //titulo de producto
    const { title } = route.params;
    //obtener uri imagen de producto o usuario
    const { uri } = route.params;
    //tipo de reporte
    const { type } = route.params;
    //id de categoria
    const { idreportcat } = route.params;
    
    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    const [data , setData ] = useState(null);
    const [dialogVisible , setDialogVisible ] = useState(false);
    const [isShow , setisShow ] = useState(false)
    const [user , setUser ] = useState('');

    navigation.setOptions({
        title: type == 0 ? 'Reportar usuario':'Reportar producto',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    useEffect(() => {
        getStorage();
    },[]);

    //setear data y mostrar dialog
    const onSubmit = async data => { 
        setData(data);
        setDialogVisible(true);
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    }

    //Reportar usuario
	const reportUser = () => {

        //Ocultar modal
        setDialogVisible(false);
		//Mostrar velo de loading
        setisShow(true);
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/insert-report/';

        const message = type == 0 ? 'Usuario reportado correctamente':'Producto reportado correctamente';
        const messageReported = type == 0 ? 'Ya has reportado este usuario':'Ya has reportado este producto'; 

        axios.post(url, {
            iduser:user._id,//usuario actual
            userreported: iduser,//usuario reportado
            idproduct,//producto
            type,//0:usuario/1:producto
            note:data.note,
            idreportcat,
        })
        .then((response) => {
            //Cancelar velo de loading
            setisShow(false);
            if(response.data.status){
                if(response.data.reported == 1){
                    Alert.alert(messageReported);
                }else{
                    Alert.alert(message);
                }  
                navigation.navigate('Home');
            }else{
                Alert.alert('Ocurrió un error, intente nuevamente');
            }
        })
        .catch((error) => {
            console.log(error);
            //Cancelar velo de loading
            setisShow(false);
            Alert.alert('Error de red, intente nuevamente')
        })
    }

    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding"
            keyboardVerticalOffset={
                Platform.OS === "ios" ? headerHeight : (headerHeight)
            }>
            <Loader loading={isShow} />
            <View style={styles.containerData}>
                <View style={styles.data}>
                <ImageBackground source={{ uri: uri }} style={styles.img} />
                {type == 0 ? (
                        <View> 
                            <Text numberOfLines={1} style= {styles.TextStyle} >{names}</Text>
                        </View>
                    ) : (
                        <View> 
                            <Text numberOfLines={1} style= {styles.ProductText} >{title}</Text>
                            <Text numberOfLines={1} style= {styles.TextStyle} >vendido por {names}</Text>
                        </View> 
                    )
                } 
                </View>
            </View>
            <Controller
                as={<TextInput style={styles.input} placeholder='Nota (Opcional)' multiline={true}
                numberOfLines={4} textAlignVertical='top' maxLength={300}/>}
                control={control}
                name="note"
                onChange={onChange}
                rules={{ required: false }}
                defaultValue=""
            />
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Enviar</Text>
                </TouchableOpacity>
            </View>
            <ConfirmDialog
                title={type == 0 ? 'Reportar usuario':'Reportar producto'}
                message="¿Seguro desea reportar este usuario?"
                visible={dialogVisible}
                onTouchOutside={() => setDialogVisible(false)}
                positiveButton={{
                    title: "Si",
                    onPress: () => reportUser()
                }}
                negativeButton={{
                    title: "Cancelar",
                    onPress: () => setDialogVisible(false)
                }}
            />
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
    },
    containerData: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom:20,
    },
    data:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        justifyContent:'center'
    },
    img: {
		justifyContent: 'flex-end', 
		width: 40,
		height: 40
    },
    TextStyle: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontSize:16,
    },
    ProductText:{
        color: colors.TEXT,
        marginLeft: 5,
        fontSize:16,
        fontWeight:'bold' 
    },
    question:{
        color: colors.TEXT,
		marginVertical: 15,
        marginLeft: 5,
        fontSize:16,
        fontWeight:'bold'
    },
    options:{
        marginVertical:15
    },
    TextOption: {
		color: colors.TEXT,
		marginVertical: 3,
        marginLeft: 5,
        fontSize:16,
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation: 2
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 150,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 5
    },
});
