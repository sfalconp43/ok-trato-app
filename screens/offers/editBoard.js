import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import { Ionicons } from '@expo/vector-icons';
import config from '../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import { useForm, Controller } from 'react-hook-form';
//loader
import Loader from '../loader/loader';
import { ConfirmDialog } from 'react-native-simple-dialogs';


export default function SellingScreen({ route }) {

    const { idboard } = route.params;
    const { nameBoard } = route.params;
    const { iduser } = route.params;

    const urlDelete = config.BASE_URL_API+'api/v1/delete-board';
    const url = config.BASE_URL_API+`api/v1/get-boards-user/`; //colocar id
    

    const navigation = useNavigation();

    navigation.setOptions({
        title: 'Editar Tablero',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerRight: () => (
			<TouchableOpacity  onPress={() => sendDelete()} style={{width: 40, height: 40, flexDirection: "column",justifyContent: "center", alignItems:'center'}}>
				<Ionicons name="md-trash" color="white" size={25} />
			</TouchableOpacity>
		),
    })

    const [refreshing , setRefreshing ] = useState(true);
    const [isShow , setisShow ] = useState(false);
    const [dialogVisible , setDialogVisible ] = useState(false);
    const [dataSource , setDataSource ] = useState({})


    useEffect(() => {

        //colocar dato en el input
        setValue('name',nameBoard);

        
    }, []);


    const deleteBoard = () => {
		
		axios.post(urlDelete, {
            idBoard: idboard,
            idUser: iduser
		},
		{
			
		}
		)
		.then((response) => {
			//Cancelar velo de loading
			setisShow(false)

			if(response.data.status){
				Alert.alert(
				'Tablero eliminado exitosamente'
				)
				navigation.navigate('Offers1');
		
			}else{
				Alert.alert(
					'Ocurrio un error, intente nuevamente'
				)
			}
		})
		.catch((error) => {
			console.log(error.response);
			//Cancelar velo de loading
			setisShow(false)
			
			Alert.alert(
				'Error eliminando tablero'
			)
		})
    }
    

    const sendDelete = () => {

        setDialogVisible(true)
    }
    
    const closeModal = () => {

        setDialogVisible(false)
    }


    //Guardar
    const onSubmit = data => {
        
        //Si la validacion falla data debe ser nulo
        if (data) { 
            //Mostrar velo de loading
            setisShow(true);

            let urlUpdate = config.BASE_URL_API+'api/v1/update-board';
            
            axios.post(urlUpdate, {
                name: data.name,
                id: idboard
            },
            {
                
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    navigation.navigate('Offers1');
                    Alert.alert('Tablero Actualizado Correctamente')

                }else{
                    //Error
                    Alert.alert('Error actualizando, intente nuevamente')
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error actualizando, intente nuevamente')
            })
        }
    }



    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
        value: args[0].nativeEvent.text,
        };
    };

    return (
        <View style={styles.container}>
        <Loader loading={isShow} />
        <Text>Nombre del Tablero</Text>
        <Controller
            as={<TextInput style={styles.input}/>}
            control={control}
            name="name"
            onChange={onChange}
            rules={{ required: 'Ingrese el nombre del tablero' }}
            defaultValue=""
        />
        {errors.name && <Text style={styles.error}>{errors.names.message}</Text>}
        <View style={styles.bottom}>
            <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                <Text style={styles.buttonText}>Guardar</Text>
            </TouchableOpacity>
        </View>

        <ConfirmDialog
                title="Eliminar Tablero"
                message="¿Seguro desea eliminar el Tablero?"
                visible={dialogVisible}
                onTouchOutside={() => setDialogVisible(false)}
                positiveButton={{
                    title: "Si",
                    onPress: () => deleteBoard()
                }}
                negativeButton={{
                    title: "Cancelar",
                    onPress: () => closeModal()
                    
                }}
                />
    </View>

    );
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    }
});

