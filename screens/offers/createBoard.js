import React, { useState, useEffect } from 'react';
import { StyleSheet, View, AsyncStorage, Text, Alert, TextInput, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from 'react-hook-form';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import axios from 'axios';

//loader
import Loader from '../loader/loader'

export default function BoardsCreateScreen() {

    const navigation = useNavigation();

    navigation.setOptions({
        title: 'Crear Tablero',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/create-board';

    const [isShow , setisShow ] = useState(false)
    const [user , setUser ] = useState('')

    // Obtener data de usuario del AsyncStorage
	const getUserStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
        console.log('user data', userData);
    }

    //Actualiza el asyncStorage en caso de algun cambio
	const onLoad = () => {

        getUserStorage();
    }

    useEffect(() => {
        onLoad();
    },[])

    const onSubmit = async data => { 

        if (data) {
            //Mostrar velo de loading
            setisShow(true);

            axios.post(url, 
            {
                name: data.name,
                iduser: user._id,
                idstate: 1
            })
            .then((response) => {
                console.log('esta entrando al axios')
                //Cancelar velo de loading
                setisShow(false)
                if(response.data.status){
                    Alert.alert(
                        'Tablero guardado exitosamente'
                    )

                    //Si esta registrado el email enviar a login
                    navigation.navigate('Offers1');
            
                }else{
                    Alert.alert(
                        'Ocurrio un error, intente nuevamente'
                    )
                }
            })
            .catch((err) => {
                console.log(err);
                //Ocultar velo de loading
                setisShow(false)
                Alert.alert(
                    'Ha ocurrido un error con la creacion del tablero'
                )
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    }

    return (

        <View style={styles.container2}>
            <Loader loading={isShow} />
            <Text style={styles.label}>Nombre del Tablero</Text>
            <Controller
                as={<TextInput style={styles.input}/>}
                control={control}
                name="name"
                onChange={onChange}
                rules={{ required: true }}
                defaultValue=""
            />
            {errors.name && <Text>Por favor ingrese el nombre del tablero.</Text>}
            <View style={styles.bottom}>
                <View style={styles.bottom}>
            <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                <Text style={styles.buttonText}>Guardar</Text>
            </TouchableOpacity>
        </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container2: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 0
    },
    error:{
        color:'red'
    },
    label:{
        marginTop:4
    }
});

