import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Alert, AsyncStorage, FlatList, Dimensions, TouchableOpacity, ImageBackground, RefreshControl, ActivityIndicator } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import colors from '../../assets/styles/colors';
import config from '../../config/config';
import axios from 'axios';

export default function SellingScreen() {

    const navigation = useNavigation();

    // Dimensiones de la pantalla
    const screenWidth = Math.round(Dimensions.get('window').width);

    navigation.setOptions({
        title: 'Ofertas',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [refreshing , setRefreshing ] = useState(false);
    const [dataSource , setDataSource ] = useState([]);
    const [user , setUser ] = useState('');
    //limite de productos
	const [limit , setLimit ] = useState(20);
	//paginacion
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
	const [endData, setEndData ] = useState(false);
	//reload para usar useEffect y recargar data
	const [reload , setReload ] = useState(false);


    //Obtener Tableros
    const getBoards = async () => {
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-boards-user/';
        axios.post(url,{
            iduser: user._id,
            limit,
            offset
        })
        .then((response) => {
            if(response.data.status){
                setRefreshing(false);
                setFooter(false);
				//Cantidad de tableros obtenidos
                let count = response.data.boards.length;
                //Nuevo offset
				setOffset(offset+count);
				//No consultar mas si ha llegado al final de la data
				if(count<limit){
					setEndData(true);
				}
                setDataSource(dataSource.concat(response.data.boards))
            }else{
                //Cancelar loading
                setRefreshing(false);
            }

        })
        .catch((error) => {
            //Cancelar velo de loading
            console.log(error)
            setRefreshing(false);
            Alert.alert('Error de red, intente nuevamente')
        })

    };

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    useEffect(() => {
        getStorage();
    },[]);

    useEffect(() => {
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            if (user) {
                getBoards();
            }
        });

        if (user) {
            getBoards();
        }
        return focused;
		
    },[user]);

    //Refresh
    useEffect(() => {
        if(offset == 0 && dataSource.length == 0 && reload){
			setReload(false);
            //Obtener data nuevamente
            getBoards();
        }
    }, [offset, dataSource, reload]);


    //Refrescar el FlatList
	const onRefresh = () => {
        setDataSource ([]);
        setRefreshing(true);
		setReload(true)
		setOffset(0);
		setEndData(false);
    }
    
    //Añadir mas elementos al flatlist
	const addMoreData = () => {
		//si no ha llegado al final de la data seguir consultando
		if(!endData){
			setFooter(true);
			getBoards();
		}
    }
    
    //Loading footer
	const renderFooter = () => {
		return (
			<View>
				{footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
			</View>
		)
    }
    
    
    const renderItem = ({ item }) => {
		const text = `${item.name}`;
		const items = `${item.num_products}`;
        const idBoard = `${item._id}`;
        const idUser = user._id;

        return (
            <View style={{marginHorizontal:(screenWidth-320)/5, marginBottom: 15, borderRadius: 5,  backgroundColor:'white', elevation:2}}>
                <TouchableOpacity onPress={ () => navigation.navigate('BoardsDetails', {idboard: idBoard, nameBoard: text, iduser:idUser})}>
                    <View>
                        {item.board_image ? (
                            <ImageBackground source={{ uri: config.BASE_URL_API+item.board_image }} style={styles.img} ></ImageBackground>
                        ) : (
                            <ImageBackground source={require('../../assets/images/gray_square.jpg')}  style={styles.img} ></ImageBackground>
                            
                        )}
                        <Text style={styles.title} numberOfLines={1}>{text}</Text>
                        <Text style={styles.text}>{items} items</Text>
                    </View>
                </TouchableOpacity>
            </View>	
        )
	};


    return (
        <View style={styles.container}>
            <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', marginTop: 20, marginBottom: 20}} onPress={() => {navigation.navigate("BoardsCreate")}}>
                <Ionicons name="md-add-circle" style= {{color: colors.PURPLE , width: 28,  height: 28, transform: [{ rotate: '180deg'}], position: 'absolute', right: 155}} size={25}/>
                <Text style= {{marginVertical:5, fontWeight: 'bold', fontSize: 16, textAlign: 'center', color: colors.TEXT}}>Crear nuevo tablero</Text>
            </TouchableOpacity>
            <FlatList
                data={dataSource}
                enableEmptySections={true}
                keyExtractor={item => String(item._id) }
                renderItem={renderItem}
                numColumns={2}
                //refresh control para recargar pantalla
                refreshControl={
                    <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                    />
                }
                showsVerticalScrollIndicator={false}
                onEndReached={addMoreData}
				onEndReachedThreshold={0.1}
				ListFooterComponentStyle={styles.flfooter}
				ListFooterComponent={renderFooter}
                />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fdfdfd',
        alignItems: 'center',
        justifyContent: 'center',
    },
    img: {
        borderRadius:10,
		width: 160,
		height: 160,
    },
    text:{
		color: colors.TEXT,
		marginLeft: 3,
		zIndex:1,
    },
    title:{
        color: colors.TEXT,
        fontSize:16,
        fontWeight:'bold',
		marginLeft: 3,
		zIndex:1,
    },
    flfooter:{
		padding:20
	},
});

