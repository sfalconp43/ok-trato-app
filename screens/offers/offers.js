import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import { TabView, SceneMap } from 'react-native-tab-view';
import { TabBar } from 'react-native-tab-view';
import Selling from './selling';
import Buying from './buying';
import Boards from './boards';
const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default function OffersScreen() {


    const navigation = useNavigation();

    const [screen, setScreen] = useState({
        index: 0,
        routes: [
            { key: 'selling', title: 'Vendiendo' },
            { key: 'buying', title: 'Comprando' },
            { key: 'boards', title: 'Tableros' },
        ]
    });

    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            (async () => {
                const userData = await AsyncStorage.getItem('userData');
                if(!userData){
                    navigation.navigate('User');
                }
            })();
        });

        // Unmount
        return focused;

	}, []);

    return (
        <TabView
        renderTabBar={props =>
            <TabBar {...props} 
			indicatorStyle={styles.indicator}
			activeColor={colors.PRIMARY}
            inactiveColor={colors.PURPLE}
			style={styles.tabbar}
			tabStyle={styles.tab}
			labelStyle={styles.label}
            />}
        navigationState={screen}
        renderScene={SceneMap({
            selling: Selling,
            buying: Buying,
            boards: Boards
            })}
            onIndexChange={index => setScreen({ ...screen, index })}
            initialLayout={{width: Dimensions.get('window').width}}
        />
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scene: {
        flex: 1,
    },
    tabItem: {
		flex: 1,
		alignItems: 'center',
		padding: 16,

	},
	tabbar: {
		backgroundColor: 'white',
	},
	tab: {
		width: winWidth/3,//  120,
	},
	indicator: {
		backgroundColor: colors.PRIMARY,
	},
	label: {
		fontWeight: '400',
	},
});
