import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TouchableOpacity, ActivityIndicator, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import { Ionicons } from '@expo/vector-icons';
import MasonryList from "react-native-masonry-list";
import axios from 'axios';
import config from '../../config/config';

export default function boarDetailScreen({ route }) {

    const { idboard } = route.params;
    const { nameBoard } = route.params;
    const { iduser } = route.params;

    const navigation = useNavigation();

    navigation.setOptions({
        title: 'Detalles del Tablero',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerRight: () => (
			<TouchableOpacity onPress={ () => navigation.navigate('BoardsEdit', {idboard: idboard, nameBoard: nameBoard, iduser: iduser})}  style={{width: 40, height: 40, flexDirection: "column",justifyContent: "center", alignItems:'center'}}>
				<Ionicons name="md-create" color="white" size={25} />
			</TouchableOpacity>

		)
    })

    const [refreshing , setRefreshing ] = useState(false);
    const [products , setProducts ] = useState([]);
    //limite de productos
	const [limit , setLimit ] = useState(30);
	//paginacion
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
	const [endData, setEndData ] = useState(false);
	//reload para usar useEffect y recargar data
	const [reload , setReload ] = useState(false);

    useEffect(() => {
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            if (iduser) {
                GetProductsBoards();
            }
        });

        if (iduser) {
            GetProductsBoards();
        }
        return focused;
		
    },[iduser]);


    //Obtener productos por filtros
	const GetProductsBoards = () => {
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-products-board/';
        setRefreshing(true);
        //Consultar productos 
        axios.post(url, {
            iduser, //Enviar id de usuario 
            idboard,
            limit,
            offset
        }).then((response) => {
            if(response.data.status){
                setRefreshing(false);
                setFooter(false);
				//Cantidad de tableros obtenidos
                let count = response.data.products.length;
                //Nuevo offset
				setOffset(offset+count);
				//No consultar mas si ha llegado al final de la data
				if(count<limit){
					setEndData(true);
				}
                setProducts(products.concat(response.data.products));
            }else{
                setRefreshing(false);    
                setFooter(false);
            }   
           
        })
        .catch((error) => {
            console.log(error)
            //Cancelar velo de loading
            setRefreshing(false);
            Alert.alert('Error de red, intente nuevamente');
        })
        
    };

    //Refresh
    useEffect(() => {
        if(offset == 0 && products.length == 0 && reload){
			setReload(false);
            //Obtener data nuevamente
            GetProductsBoards();
        }
    }, [offset, products, reload]);

    //Añadir mas elementos al flatlist
	const addMoreData = () => {
		//si no ha llegado al final de la data seguir consultando
		if(!endData){
			setFooter(true);
			GetProductsBoards();
		}
    }

    //Refrescar el FlatList
	const onRefresh = () => {
        setProducts ([]);
        setRefreshing(true);
		setReload(true)
		setOffset(0);
		setEndData(false);
    }

    //Abrir producto en nueva pantalla
	const onPressImage = product => {
		//Mostrar screen de detalle
		navigation.navigate('ProductDetail', { idproduct: product._id });
	};

    return (
        <View style={styles.container}>
           <MasonryList
                images={products}
                onPressImage={onPressImage}
                columns={2}
                refreshing = {refreshing}
                onEndReachedThreshold={0.01}
                onEndReached={addMoreData}
                onRefresh={onRefresh}
            />
            <View styles={styles.activityContainer}>
				{footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
			</View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    activityContainer: {
        marginTop:20,
        justifyContent: 'center', 
        flexDirection:'row',
        alignItems: 'center',
	},
});

