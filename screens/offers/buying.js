import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ActivityIndicator, Alert, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';;
import MasonryList from "react-native-masonry-list";
import axios from 'axios';
import config from '../../config/config';

export default function buyingScreen() {

    const navigation = useNavigation();

    const [user , setUser ] = useState('');
    const [refreshing , setRefreshing ] = useState(false);
    const [products , setProducts ] = useState([]);
    //limite de productos
	const [limit , setLimit ] = useState(20);
    //paginacion
    
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
	const [endData, setEndData ] = useState(false);
	//reload para usar useEffect y recargar data
	const [reload , setReload ] = useState(false);

    useEffect(() => {
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            if (user) {
                GetProductsChat();
            }
        });

        if (user) {
            GetProductsChat();
        }
        return focused;
		
    },[user]);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    useEffect(() => {
        getStorage();
    },[]);


    //Obtener productos por filtros
	const GetProductsChat = () => {
        //construir ruta de servidor
        const url = config.BASE_URL_API+'api/v1/get-products-chat/';
        setRefreshing(true);
        //Consultar productos 
        axios.post(url, {
            iduser:user._id, //Enviar id de usuario 
            limit,
            offset
        }).then((response) => {
            if(response.data.status){
                setRefreshing(false);
                setFooter(false);
				//Cantidad de tableros obtenidos
                let count = response.data.products.length;
                //Nuevo offset
				setOffset(offset+count);
				//No consultar mas si ha llegado al final de la data
				if(count<limit){
					setEndData(true);
				}
                setProducts(products.concat(response.data.products));
            }else{
                setRefreshing(false);    
                setFooter(false);
            }   
           
        })
        .catch((error) => {
            console.log(error)
            //Cancelar velo de loading
            setRefreshing(false);
            Alert.alert('Error de red, intente nuevamente');
        })
        
    };

    //Refresh
    useEffect(() => {
        if(offset == 0 && products.length == 0 && reload){
			setReload(false);
            //Obtener data nuevamente
            GetProductsChat();
        }
    }, [offset, products, reload]);

    //Añadir mas elementos al flatlist
	const addMoreData = () => {
		//si no ha llegado al final de la data seguir consultando
		if(!endData){
			setFooter(true);
			GetProductsChat();
		}
    }

    //Refrescar el FlatList
	const onRefresh = () => {
        setProducts ([]);
        setRefreshing(true);
		setReload(true)
		setOffset(0);
		setEndData(false);
    }

    //Abrir producto en nueva pantalla
	const onPressImage = product => {
		//Mostrar screen de detalle
		navigation.navigate('ProductDetail', { idproduct: product._id });
	};

    return (
        <View style={styles.container}>
           <MasonryList
                images={products}
                onPressImage={onPressImage}
                columns={2}
                refreshing = {refreshing}
                onEndReachedThreshold={0.01}
                onEndReached={addMoreData}
                onRefresh={onRefresh}
                //imageContainerStyle={{backgroundColor:colors.PURPLE}}
            />
            <View styles={styles.activityContainer}>
				{footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
			</View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    activityContainer: {
        marginTop:20,
        justifyContent: 'center', 
        flexDirection:'row',
        alignItems: 'center',
	},
});

