import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, FlatList, ImageBackground, TouchableOpacity, RefreshControl, ActivityIndicator, StatusBar } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';

import { useNavigation } from '@react-navigation/native';

export default function SellingScreen() {

    const [user , setUser ] = useState(null);
	const [refreshing , setRefreshing ] = useState(true);
	const [products , setProducts ] = useState([]);
	//limite de productos
	const [limit , setLimit ] = useState(20);
	//paginacion
	const [offset , setOffset ] = useState(0);
	//Loading footer
	const [footer , setFooter ] = useState(false);
	//identificador final de data para evitar mas consultas en la paginacion
	const [endData, setEndData ] = useState(false);
	//reload para usar useEffect y recargar data
	const [reload , setReload ] = useState(false);
    const navigation = useNavigation();

    useEffect(() => {
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            getStorage();
        });
        // Unmount
        return focused;
    }, []);

    //Obtener mis productos
    useEffect(() => {
        if(user){
            GetProducts();
        }
	}, [user]);
	
	//Refresh
    useEffect(() => {
        if(offset == 0 && products.length == 0 && reload){
			setReload(false);
				//Obtener data nuevamente
            GetProducts();
        }
    }, [offset, products, reload]);

    // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    //Obtener productos del usuario
	const GetProducts = async () => {

        const url = config.BASE_URL_API+'api/v1/get-user-products/';
		//Consultar productos de usuario 
		axios.post(url,{
			iduser: user._id,
			limit:limit,
			offset:offset
		})
		.then((response) => {
			if(response.data.status){
				setRefreshing(false);
				setFooter(false);
				//Cantidad de productos obtenidos
                let count = response.data.products.length;
                //Nuevo offset
				setOffset(offset+count);
				//No consultar mas si ha llegado al final de la data
				if(count<limit){
					setEndData(true);
				}
                setProducts(products.concat(response.data.products));
			}else{
				//Cancelar loading
                setRefreshing(false);
			}   
		})
		.catch((error) => {
			console.log(error);
			//Cancelar loading
            setRefreshing(false);
			Alert.alert('Error de red, intente nuevamente')
		}) 
    };

    //Ir al screen del producto del cliente para editar
    const gotoProduct = (_id) =>{
        console.log(_id);
    } 
    
    //Productos listado
	const renderItem = ({ item }) => {
		const title = `${item.title}`;
		const views = `${item.views} Vistas`
		return (
			<View>
			<TouchableOpacity onPress={()=>gotoProduct(item._id)}>
				<View style={styles.row}>
					<View style={styles.box}>
						<ImageBackground source={{ uri: item.URI }} style={styles.img} >
							<Text style={styles.viewsproduct}>{views}</Text>
							<LinearGradient
							colors={['transparent',colors.PURPLE]}
							style={styles.gradient}
							/>
						</ImageBackground>
					</View>
					<View style={styles.box2}>
						<Text numberOfLines={1} style={styles.title}>{title}</Text>
					</View>
					<View style={styles.box3}>
						<Ionicons name="ios-arrow-forward" color={colors.PURPLE} size={25} />
					</View>
				</View>
			</TouchableOpacity>
			<View style={{ height: 1, width: '100%', backgroundColor: colors.PURPLE }} />
			</View>
		);
    };
    
    //Refrescar el FlatList
	const onRefresh = async () =>{
        //Limpiar data para recargar data
        setProducts([]);
		setRefreshing(true);
		setReload(true)
		setOffset(0);
		setEndData(false);
	}

	//Añadir mas elementos al flatlist
	const addMoreData = () => {
		//si no ha llegado al final de la data seguir consultando
		if(!endData){
			setFooter(true);
			GetProducts();
		}
	}

	//Loading footer
	const renderFooter = () => {
		return (
			<View styles={styles.activityContainer}>
				{footer && <ActivityIndicator size="large" color={colors.PRIMARY}/>}
			</View>
		)
    }

    return (
        //Retornar el flatlist
        <View style={styles.MainContainer}>
            <FlatList
				data={products}
				enableEmptySections={true}
				keyExtractor={item => String(item._id) }
				renderItem={renderItem}
				//refresh control para recargar pantalla
				refreshControl={
					<RefreshControl
					refreshing={refreshing}
					onRefresh={onRefresh}
					/>
				}
				onEndReached={addMoreData}
				onEndReachedThreshold={0.1}
				ListFooterComponentStyle={styles.flfooter}
				ListFooterComponent={renderFooter}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	activityContainer: {
		flex: 1,
		justifyContent: 'center',
	
	},
	flfooter:{
		padding:20
	},
	gradient:{
		position: 'absolute',
		left: 0,
		right: 0,
		bottom: 0,
		height: 20,
	},
	img: {
		justifyContent: 'flex-end',
		alignItems:'center',
		width: 62,
		height: 62,
	},
	viewsproduct:{
		color: 'white',
		marginLeft: 3,
		zIndex:1,
		fontSize:12,
	},
	MainContainer: {
		justifyContent: 'center',
		flex: 1,
		marginTop: 10,
	},
	rowViewContainer: {
		fontSize: 20,
		padding: 10,
	},
	row: {
        flex: 1,
        flexDirection: 'row',
		padding: 10
    },
	box: {
		flex: 1,
	},
	box2: {
		flex:3,
		alignSelf: 'stretch',
	},
	title:{
		fontSize:16,
		fontWeight: 'bold',
		color:colors.TEXT
	},
    box3: {
		flex: 1,
        alignItems:'flex-end',
        justifyContent:'center',
        alignContent:'flex-end',
        padding:10
	},
	
});

