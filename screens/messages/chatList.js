import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, FlatList, RefreshControl, Image, TouchableOpacity } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import TimeAgo from 'react-native-timeago';
import io from "socket.io-client";

let moment = require('moment');
require('moment/locale/es'); //importar el lenguaje
moment.locale('es');//espanol

import { useNavigation } from '@react-navigation/native';

export default function ChatList() {

    const navigation = useNavigation();
    const [user , setUser ] = useState(null)
    const [refreshing , setRefreshing ] = useState(false);
	const [chats , setChats ] = useState([]);
	const socket = io(config.BASE_URL_API);

    useEffect(() => {

        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        // Unmount
        return focused;

	}, []);
	
	//Obtener listado de chats y escuchar mensajes entrantes
	useEffect(() => {
		if(user){
			listenLastMessage();
			GetChats();
			goOnline();
		}
		
		//cerrar socket al salir (unmount)
		return () => {
			listenLastMessage();
			socket.off('list chat');
			socket.off('clist');
			socket.close();
		}

	}, [user]);


	//Conectarse a io socket para escuchar mensajes entrantes
	const listenLastMessage = () =>{
	
		var allChats = [...chats];

		socket.on("list chat", data => {

			if(allChats.length == 0){
				GetChats();
			}else{

				//buscar el id del chat para actualizar
				let index = allChats.findIndex(el => el._id === data.idchat);

				//no encontrado actualizar la lista
				if(index == -1){
					GetChats();
				}else{
					//actualizar estado 
					allChats[index] = {...allChats[index], message: data.message, message_date: data.createdate, seen:false };
					setChats(allChats);
				}
				
			}
		});
	}

	//Conectarse a io socket basado en el namespace
	const goOnline = () => {
		//conectarse al namespace del chat
		socket.emit('clist','user' + user._id);
	}

	// Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
        setUser(JSON.parse(userData));
    };

    //Obtener productos por cliente
	const GetChats = async () => {
        const url = config.BASE_URL_API+'api/v1/get-list-chat/';
        //Consultar productos de usuario 
        setRefreshing(true);

		axios.post(url,{
            iduser: user._id,
        })
		.then((response) => {
            //Cancelar loading
            setRefreshing(false);

			if(response.data.status){
				//console.log(response.data.chats)
                setChats( response.data.chats );
			}else{
                Alert.alert('Error obteniendo lista de chats')
			}   
		})
		.catch((error) => {
			console.log(error)
			//Cancelar loading
			setRefreshing(false);
			Alert.alert('Error de red, intente nuevamente')
		}) 
	};
	
	//Enviar a la pantalla de detalle del chat
	const goToChat = (item) => {

		//informacion del vendedor
		const userId = user._id;
		let user1 = item.iduser1._id;
		let userdata = {
			id: user1 == userId ? item.iduser2._id : item.iduser1._id,
			image: user1 == userId ? item.iduser2.image : item.iduser1.image,
			names: user1 == userId ? item.iduser2.names : item.iduser1.names,
			lastnames: user1 == userId ? item.iduser2.lastnames : item.iduser1.lastnames,
			imageproduct: item.idproduct.images[0],
			price: item.idproduct.price,
			productaddress: item.idproduct.productaddress
		}
		//cerrar sockets
		socket.close();
		navigation.navigate('Chat', { chatid: item._id, product:null, userdata });
	}
    
    //Productos listado
	const renderItem = ({ item }) => {

		//verificar usuario conectado
        const userId = user._id;
        const idsender = item.idsender;
		let user1 = item.iduser1._id;
		let image_user_message = '';
		let name_user = '';
        let last_message = '';
        let isNew = false;
		//caso usuario mensajes personales
		if(user1 == userId){
			image_user_message = config.BASE_URL_API+'users/'+item.iduser2.image;
			name_user = item.iduser2.names;
		//caso usuario mensajes de la otra persona
		}else{
			image_user_message = config.BASE_URL_API+'users/'+item.iduser1.image;
			name_user = item.iduser1.names;
        }

        if(idsender == userId){
            last_message = `Tú:${item.message}`;
        }else{
            last_message = item.message;
            if( item.seen == false)
                isNew = true;
        }
        //imagen de producto
        let imageProd = `${config.BASE_URL_API}${item.idproduct.images[0]}`;

		return (
			<View>
				<TouchableOpacity style={styles.item} onPress={ ()=>goToChat(item) }>
					<View style={styles.containerProd}>
						<View>
						    { isNew && <View style={styles.circle}/>}
						    <Image source={{ uri: image_user_message }} style={styles.img} />
						</View>
						<View style={styles.middleMsg}>
							<Text numberOfLines={1} style={styles.username}>{ name_user }</Text>
							<Text numberOfLines={1} style={styles.lastmessage}>{ last_message	}</Text>
							<TimeAgo style={styles.messagedate} time={item.message_date} interval={20000}/>
						</View>
						<Image source={{ uri: imageProd }} style={styles.imgProd} />
					</View>
				</TouchableOpacity>
				<View style={{ height: 1, width: '100%', backgroundColor: colors.GRAY }} />
			</View>
		);
    }

    //Refrescar el FlatList
	const onRefresh = () => {
		if(user){
			//Limpiar data vieja y mostrar loading
			setRefreshing(true);
			setChats([]);
			//Obtener data nuevamente
			GetChats();
		}
	}

	const RegisterNotification = () =>{
		if(!user){
			return(
				<TouchableOpacity onPress={ ()=>navigation.navigate('User') }>
					<View style={{backgroundColor: 'white',flexDirection: 'row',justifyContent: 'space-between', padding: 10}}>
						<View>
							<Image source={require('../../assets/images/ok_logo_round.png')} borderColor={colors.PURPLE} borderWidth={2} borderRadius={30}
								 style={{ width: 62,height: 62, borderRadius:30 }} />
						</View>
						<View style={styles.notification}>
							<Text style={styles.messageOkTrato}>Bienvenido a la comunidad de <Text style={{color:colors.PURPLE, fontWeight:'bold'}}>Ok Trato!</Text>. Regístrate.</Text>
						</View>
					</View>
				</TouchableOpacity>
			)
		}else{
			return(<View></View>)
		}
	}
    
    return (
        <View style={styles.MainContainer}>
			<RegisterNotification/>
			<FlatList
			data={chats}
			enableEmptySections={true}
			keyExtractor={item => String(item._id)}
			renderItem={renderItem}
			//refresh control para recargar pantalla
			refreshControl={
				<RefreshControl
				refreshing={refreshing}
				onRefresh={onRefresh}
				/>
			}
			/>
      	</View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    middleMsg:{
		marginLeft:10,
		padding:4,
		flex: 1,
		justifyContent: 'center',
	},
	containerProd: {
		backgroundColor: 'white',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10
	},
	gradient:{
		position: 'absolute',
		left: 0,
		right: 0,
		bottom: 0,
		height: 20,
	},
	item:{
		marginBottom:2
	},
	img: {
		justifyContent: 'flex-end', 
		width: 62,
		height: 62,
		borderRadius: 30,
	},
	imgProd: {
		justifyContent: 'flex-end', 
		width: 62,
		height: 62,
		borderRadius:10
	},
	MainContainer: {
		justifyContent: 'center',
		flex: 1,
	},
	loading: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
	},
	activityIndicatorWrapper: {
		height: 100,
		width: 100,
		borderRadius: 10,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-around'
	},
	username:{
		fontSize:18,
		fontWeight:'bold',
	},
	lastmessage: {
		color:colors.TEXT
	},
	messagedate:{
		fontSize:10,
		color:'#877979'
	},
	circle: {
		top: 0,
		position:'absolute',
		width: 8,
		height: 8,
		borderRadius: 8/2,
		backgroundColor: 'red'
	},
	notification:{
		marginLeft:10,
		padding:4,
		flex: 1,
		justifyContent: 'flex-start',
	},
	messageOkTrato:{
		fontSize:14,
		color:colors.TEXT,
	}
});
