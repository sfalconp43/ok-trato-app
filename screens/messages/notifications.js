import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import Loader from '../loader/loader';

import { useNavigation } from '@react-navigation/native';

export default function Notifications() {

    const navigation = useNavigation();
    const [user , setUser ] = useState('')
    const [isShow , setisShow ] = useState(false)

    useEffect(() => {
        getStorage();
        
        const focused = navigation.addListener('focus', () => {
            // The screen is focused
            // Refresh storage
            getStorage();
        });

        // Unmount
        return focused;

    }, []);
    
     // Obtener data de usuario del AsyncStorage
	const getStorage = async () => {
        const userData = await AsyncStorage.getItem('userData');
		setUser(JSON.parse(userData));
    };

    return (
        <View style={styles.container}>
            <Text>Notifications</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
