import React, { useState } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import colors from '../../assets/styles/colors';
import { TabView, SceneMap } from 'react-native-tab-view';
import { TabBar } from 'react-native-tab-view';
import chat from './chatList';
import notifications from './notifications';
const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default function MessagesScreen() {

    const navigation = useNavigation();

    navigation.setOptions({
        title: 'Mensajes',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [screen, setScreen] = useState({
        index: 0,
        routes: [
            { key: 'chat', title: 'Mensajes' },
			{ key: 'notifications', title: 'Notificaciones' },
        ]
    });

    return (
        <TabView
        renderTabBar={props =>
            <TabBar {...props} 
			indicatorStyle={styles.indicator}
			activeColor={colors.PRIMARY}
            inactiveColor={colors.PURPLE}
			style={styles.tabbar}
			tabStyle={styles.tab}
			labelStyle={styles.label}
            />}
        navigationState={screen}
        renderScene={SceneMap({
            chat: chat,
            notifications: notifications,
            })}
            onIndexChange={index => setScreen({ ...screen, index })}
            initialLayout={{width: Dimensions.get('window').width}}
        />
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scene: {
        flex: 1,
    },
    tabItem: {
		flex: 1,
		alignItems: 'center',
		padding: 16,

	},
	tabbar: {
		backgroundColor: 'white',
	},
	tab: {
		width: winWidth/2,//  120,
	},
	indicator: {
		backgroundColor: colors.PRIMARY,
	},
	label: {
		fontWeight: '400',
	},
});
