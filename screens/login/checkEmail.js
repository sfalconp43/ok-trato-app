import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Alert, KeyboardAvoidingView, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import config from '../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form'
//loader
import Loader from '../loader/loader';
import { useHeaderHeight } from '@react-navigation/stack';

export default function CheckEmail() {
    
    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();
    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/check-email';

    navigation.setOptions({
        title: 'Regístrate o inicia sesión',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false)
    const [email , setEmail ] = useState('')

    //Guardar en sesión el email
    const saveEmailStorage = async () => {
        await AsyncStorage.setItem('email', email);
    }

    const onSubmit = data => {
        //Si la validacion falla data debe ser nulo
        if (data.email) { 
            //Mostrar velo de loading
            setisShow(true);

            axios.post(url, {
                email: data.email.toLowerCase(),
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);
                saveEmailStorage(); 

                if(response.data.status && response.data.register){
                    //Si esta registrado el email enviar a login
                    navigation.navigate('Login');
                }else{
                    //Si no encuentra el email mostrar el registro
                    navigation.navigate('Register', { emailToSave: email});
                }   
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, handleSubmit, errors } = useForm();

    const onChange = args => {
        setEmail(args[0].nativeEvent.text);
        return {
          value: args[0].nativeEvent.text,
        };
    };

    return (
        <KeyboardAvoidingView style={styles.container}>
            <Loader loading={isShow} />
            <View style={{backgroundColor: 'white',justifyContent: 'center', alignItems:'center', height:157}}>
                <Image source={require('../../assets/images/ok_trato_logo.png')} resizeMode='contain' style={{ width: 228, borderRadius:30 }} />
            </View>
            <Text style= {styles.label}>Email</Text>
            <Controller
                as={<TextInput style={styles.input} autoCapitalize = 'none'/>}
                control={control}
                name="email"
                onChange={onChange}
                rules={{ required: true, pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: "Ingrese un email válido"
                }}}
            />
            {errors.email && errors.email.type === 'required' && <Text style={styles.error}>Ingrese email</Text>}
            {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    label: {
		color: colors.TEXT,
		marginVertical: 3,
        fontSize:14,
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation:2
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    error:{
        color:'red'
    }
});
