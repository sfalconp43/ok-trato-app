import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Alert, TouchableOpacity, KeyboardAvoidingView, SafeAreaView, TouchableWithoutFeedback, Platform, Keyboard, Image } from 'react-native';
//loader
import Loader from '../loader/loader';
import config from '../../config/config';
import colors from '../../assets/styles/colors';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form';
import AuthContext from '../authContext';
import { useHeaderHeight } from '@react-navigation/stack';

export default function Register({ route, navigation }) {

    const headerHeight = useHeaderHeight();

    //obtener parametros
    const { emailToSave } = route.params;

    const { signIn } = useContext(AuthContext);

    navigation.setOptions({
        title: 'Registro',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false);

    //construir ruta de servidor registro
    const url = config.BASE_URL_API+'api/v1/user-register';

    useEffect(() => {
        
    }, []);

    //Guardar
    const onSubmit = data => {

        if (data) { //Si la validacion falla data debe ser nulo
            //Mostrar velo de loading
            setisShow(true);

            axios.post(url, 
            {
                idcountry: 1,//default
                names: data.names,
                lastnames: data.lastnames,
                email: data.email.toLowerCase(),
                password: data.userpassword,
            })
            .then((response) => {
                //Ocultar velo de loading
                setisShow(false);

                if(response.data.status){
                    //Mostrar mensaje de perfil
                    let userData = response.data.user;
                    AsyncStorage.setItem('userData', JSON.stringify(userData));
                    signIn(response.data.user.token);
                }else{
                    Alert.alert('Ocurrio un error, intente nuevamente')
                }
            })
            .catch((err) => {
                //Ocultar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, errors, handleSubmit, watch } = useForm({});
  
    const onChange = args => {
        return { value: args[0].nativeEvent.text };
    };

    return (
        <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : null}
        style={{ flex: 1,  backgroundColor: '#ffffff', }}
    >
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ScrollView style={{marginBottom:10}}>
                <View style={styles.inner}>
                    <View style={{backgroundColor: 'white',justifyContent: 'center', alignItems:'center', height:157}}>
                        <Image source={require('../../assets/images/ok_trato_logo.png')} resizeMode='contain' style={{ width: 228, borderRadius:30 }} />
                    </View>
                    <Loader loading={isShow} />
                    <Text style={styles.label}>Email</Text>
                    <Controller
                    as={<TextInput style={styles.input} autoCapitalize = 'none' editable={false} />}
                    control={control}
                    name="email"
                    onChange={onChange}
                    rules={{ required: true, pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "Ingrese un email válido"
                    }}}
                    defaultValue={ emailToSave }
                    />
                    {errors.email && errors.email.type === 'required' && <Text style={styles.error}>Ingrese email</Text>}
                    {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}
                    <Text style={styles.label}>Nombres</Text>
                    <Controller
                    as={<TextInput style={styles.input} autoFocus />}
                    control={control}
                    name="names"
                    onChange={onChange}
                    rules={{ 
                            required: "Ingrese nombres", 
                            minLength: {
                                value: 3,
                                message: "Los nombres debe tener al menos 3 caracteres"
                            }
                        }} 
                    defaultValue=""
                    />
                    {errors.names && <Text style={styles.error}>{errors.names.message}</Text>}
                    <Text style={styles.label}>Apellidos</Text>
                    <Controller
                    as={<TextInput style={styles.input} />}
                    control={control}
                    name="lastnames"
                    onChange={onChange}
                    rules={{ 
                            required: "Ingrese apellidos", 
                            minLength: {
                                value: 3,
                                message: "El apellido debe tener al menos 3 caracteres"
                            }
                        }} 
                    defaultValue=""
                    />
                    {errors.names && <Text style={styles.error}>{errors.names.message}</Text>}
                    <Text style={styles.label}>Contraseña</Text>
                    <Controller
                        as={<TextInput style={styles.input} secureTextEntry={true}/>}
                        control={control}
                        name="userpassword"
                        onChange={onChange}
                        rules={{ 
                            required: "Ingrese contraseña nueva", 
                            minLength: {
                                value: 6,
                                message: "El contraseña debe tener al menos 6 caracteres"
                            }
                        }} 
                        defaultValue=""
                    />
                    {errors.userpassword && <Text style={styles.error}>{errors.userpassword.message}</Text>}
                    <Text style={styles.label}>Confirmar contraseña</Text>
                    <Controller
                        as={<TextInput style={styles.input} secureTextEntry={true}/>}
                        control={control}
                        name="reenterpassword"
                        onChange={onChange}
                        rules={{ 
                            required: "Ingrese confirmación de contraseña", 
                            minLength: {
                                value: 6,
                                message: "El contraseña debe tener al menos 6 caracteres"
                            },
                            validate : (value) => value === watch('userpassword') || "Las contraseñas no coinciden"
                        }} 
                        defaultValue=""
                    />
                    {errors.reenterpassword && <Text style={styles.error}>{errors.reenterpassword.message}</Text>}
                    <View style={styles.btnContainer}>
                        <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                            <Text style={styles.buttonText}>GUARDAR</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex : 1 }} />
                </View>
                </ScrollView>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    inner: {
        padding: 20,
        flex: 1,
        justifyContent: "flex-end",
    },
    header: {
        fontSize: 36,
        marginBottom: 20,
        color:colors.TEXT
    },
    input: {
        height: 40,
        borderColor: "#000000",
        borderBottomWidth: 1,
        marginBottom: 36,
    },
    btnContainer: {
        backgroundColor: "white",
        marginTop: 20,
        justifyContent:'flex-end'
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation:2
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    error:{
        color:'red'
    },
    label:{
        color:colors.TEXT,
        marginTop:10
    }
});
