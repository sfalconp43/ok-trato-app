import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Alert, KeyboardAvoidingView, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import config from '../../config/config';
import { TextInput } from 'react-native-gesture-handler';
import colors from '../../assets/styles/colors';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form';
import AuthContext from '../authContext'
//loader
import Loader from '../loader/loader';
import { useHeaderHeight } from '@react-navigation/stack';

export default function Login() {
    
    const navigation = useNavigation();
    const headerHeight = useHeaderHeight();

    const { signIn } = useContext(AuthContext);

    //construir ruta de servidor
    const url = config.BASE_URL_API+'api/v1/login';

    navigation.setOptions({
        title: 'Login',
        headerStyle: {
            backgroundColor: colors.PRIMARY,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    const [isShow , setisShow ] = useState(false)
    const [email, setEmail ] = useState('')

    useEffect(() => {
		checkData();
	});

    //Obtener email del AsyncStorage para colocarlo en el input
    const checkData = async () => {
        try {
            const storage = await AsyncStorage.getItem('email');
            if (storage !== null) {
                setEmail(storage);
                setValue('email',storage)
                await AsyncStorage.removeItem('email');
            }
        } catch (error) {
            console.log(error);
        }
    };

    //Guardar en sesión el token y otros datos
    const signInAsync = async (data) => {
        await AsyncStorage.setItem('userData', JSON.stringify(data));
    }

    const onSubmit = async data => {

        //Si la validacion falla data debe ser nulo
        if (data) { 
            //Mostrar velo de loading
            setisShow(true);

            axios.post(url, {
                email: data.email,
                password: data.password,
            })
            .then((response) => {
                //Cancelar velo de loading
                setisShow(false);

                if(response.data.status){
                    signIn(response.data.user.token);
                    signInAsync(response.data.user);
                }else{
                    Alert.alert('Datos incorrectos')
                }
            })
            .catch((error) => {
                console.log(error);
                //Cancelar velo de loading
                setisShow(false);
                Alert.alert('Error de red, intente nuevamente')
            })
        }
    }

    const { control, handleSubmit, errors, setValue  } = useForm();

    const onChange = args => {
        return {
            value: args[0].nativeEvent.text,
        };
    };

    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={{backgroundColor: 'white',justifyContent: 'center', alignItems:'center', height:157}}>
                <Image source={require('../../assets/images/ok_trato_logo.png')} resizeMode='contain' style={{ width: 228, borderRadius:30 }} />
            </View>
            <Loader loading={isShow} />
            <Text style={styles.label}>Email</Text>
            <Controller
                as={<TextInput style={styles.input}/>}
                control={control}
                name="email"
                onChange={onChange}
                rules={{ 
                    required: "Ingrese email", 
                    pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "Ingrese un email válido"
                    }
                }}
                defaultValue={email}
            />
            {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}
            <Text style={styles.label}>Contraseña</Text>
            <Controller 
                as={<TextInput style={styles.input} secureTextEntry={true}/>} 
                control={control} 
                onChange={onChange}
                name="password" 
                rules={{ 
                    required: "Ingrese contraseña", 
                    minLength: {
                        value: 6,
                        message: "El contraseña debe tener al menos 6 caracteres"
                    }
                }} 
                defaultValue=""
            />
            {errors.password && <Text style={styles.error}>{errors.password.message}</Text>}
            <View style={styles.bottom}>
                <TouchableOpacity style={styles.button} onPress={handleSubmit(onSubmit)} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
        elevation:2
    },
    input: {
        backgroundColor: '#EEEEEE',
        borderColor: '#EEEEEE',
        borderWidth:1,
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    error:{
        color:'red'
    },
    label: {
		color: colors.TEXT,
		marginVertical: 3,
        fontSize:14,
    },
});
