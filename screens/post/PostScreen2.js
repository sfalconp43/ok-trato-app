import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Slider, TextInput, AsyncStorage, Dimensions, Image, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { MaterialIcons } from '@expo/vector-icons';
import Toast from 'react-native-tiny-toast';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);

export default function PostScreen2() {
    const navigation = useNavigation();

    const [category, setCategory] = useState(null);
    const [indexCondition, setIndexCondition] = useState(1);
    const [description, setDescription] = useState('');
    const [condition, setCondition] = useState({
        1: 'Otro',
        2: 'Para reparación',
        3: 'Usado',
        4: 'Caja Abierta (nunca usado)',
        5: 'Reacondicionado/Certificado',
        6: 'Nuevo',
    });



    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        navigation.addListener('focus', () => {
            (async () => {
                let categoryStorage = await AsyncStorage.getItem('productCategory');
                setCategory(JSON.parse(categoryStorage));
            })();
            (async () => {
                let descriptionStorage = await AsyncStorage.getItem('productDescription');
                if (descriptionStorage != null) {
                    setDescription(descriptionStorage); 
                }
            })();
            (async () => {
                let conditionnStorage = await AsyncStorage.getItem('productCondition');
                if (conditionnStorage != null) {
                    setIndexCondition(conditionnStorage);
                }
            })();
        })

    }, []);

    let saveStepTwo = async () => {
        const regularExpression = /[A-Za-z0-9]{2,300}/;

        if (!regularExpression.test(description) && category == null) {
            Toast.show('Se debe seleccionar una categoría para el producto. Ademas, la descripción del producto no puede estar vacía, debe contener dos o más caracteres validos.', { duration: 4000 });
        } else {
            if (category == null) {
                Toast.show('Se debe seleccionar una categoría para el producto.', { duration: 3000 });
            }

            if (!regularExpression.test(description)) {
                Toast.show('La descripción del producto no puede estar vacía, debe contener dos o más caracteres validos.', { duration: 3000 });
            }

            if (regularExpression.test(description) && category != null) {
                try {
                    await AsyncStorage.setItem('productCondition', indexCondition.toString());
                    await AsyncStorage.setItem('productDescription', description);
                } catch (error) {
                    console.log(error);
                }
                navigation.navigate('Post3');
            }
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ width: '100%', backgroundColor: 'transparent', justifyContent: 'center', borderBottomWidth: 1, borderColor: colors.GRAY, paddingVertical: 10 }}>
                <TouchableOpacity
                    style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 10, paddingRight: 3 }}
                    onPress={() => {
                        navigation.navigate('Categories');
                    }}
                >
                    <Text style={{ color: colors.TEXT, fontSize: 14, fontWeight: 'bold' }}>
                        Categoría: {category ? category.name : 'No se ha selecionado aun...'}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>Selecionar</Text>
                        <MaterialIcons name="keyboard-arrow-right" size={20} color={colors.PURPLE} />
                    </View>
                    
                </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 1, borderColor: colors.GRAY, paddingVertical: 10 }}>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                    <Text style={{ fontWeight: 'bold', color: colors.TEXT, fontSize: 14 }}>Condición</Text>
                    <Text style={{ color: colors.PURPLE, fontSize: 14, fontWeight: 'bold' }}>{condition[indexCondition]}</Text>
                </View>
                <Slider
                    thumbTintColor={colors.PURPLE}
                    minimumTrackTintColor={colors.PURPLE}
                    step={1}
                    minimumValue={1}
                    maximumValue={6}
                    value={indexCondition}
                    onValueChange={(indexCondition) => { setIndexCondition(indexCondition) }}
                ></Slider>
            </View>

            <View style={{ borderBottomWidth: 1, borderColor: colors.GRAY, backgroundColor: 'white', width: '100%', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#757575', marginBottom: 5 }}>
                    Descripción del producto
                </Text>
                <TextInput
                    style={{ height: 20 * 4, backgroundColor: colors.GRAY, borderRadius: 10, paddingLeft: 10, color: colors.TEXT }}
                    onChangeText={description => setDescription(description)}
                    value={description}
                    multiline={true}
                    numberOfLines={4}
                    maxLength={300}
                />
            </View>
            <View style={{ flex: 1, backgroundColor: 'white', width: screenWidth, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 20 }}>
                <Image
                    style={{ width: 300 }}
                    source={require('../../assets/wizard02.png')}
                    resizeMode={'contain'}
                />
                <TouchableOpacity
                    style={{ justifyContent: 'center', height: 30, width: 0.9 * screenWidth, backgroundColor: colors.PURPLE_BTN, borderRadius: 15, elevation: 2 }}
                    onPress={() => {
                        saveStepTwo();
                    }}
                >
                    <Text style={{ fontSize: 14, color: 'white', alignSelf: 'center' }}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
