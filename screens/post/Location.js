import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, AsyncStorage, ImageBackground, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import Loader from '../loader/loader';
import Toast from 'react-native-tiny-toast';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);

export default function PostScreen() {
    const navigation = useNavigation();

    const [locationPermission, setLocationPermission] = useState(false);
    const [zipcode, setZipcode] = useState('');
    const [showLoader, setShowLoader] = useState(false);

    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        (async () => {
            const { status } = await Permissions.askAsync(Permissions.LOCATION);
            setLocationPermission(status === 'granted');
        })();
    }, []);

    useEffect(() => {
        if (zipcode.length === 5) {
            const regularExpression = /[0-9]{5,5}/;

            if (!regularExpression.test(zipcode)) {
                Toast.show('El código postal ingresado no es valido, debe estar conformado por cinco caracteres numéricos.', { duration: 4000 });
            } else {
                (async () => {
                    setShowLoader(true);
                    let location = await Location.geocodeAsync(zipcode)

                    if (location.length > 0) {
                        let address = await Location.reverseGeocodeAsync({ latitude: location[0].latitude, longitude: location[0].longitude });

                        let addressAux = {
                            latitude: location[0].latitude,
                            longitude: location[0].longitude,
                            address: `${address[0].city}, ${address[0].region}`,
                            zipcode: address[0].postalCode
                        }

                        AsyncStorage.setItem('productAddress', JSON.stringify(addressAux)).then(() => {
                            setShowLoader(false);
                            navigation.goBack();
                        }).catch((error) => {
                            setShowLoader(false);
                            Alert.alert(
                                'Eror',
                                'Ha ocurrido un error al obtener la ubicación',
                                [{ text: 'OK' }],
                                { cancelable: false }
                            );
                        });
                    } else {
                        setShowLoader(false);
                        Toast.show('No se obtuvo un resultado satisfactorio para el código postal ingresado.', { duration: 4000 });
                    }
                })();
            }
        }
    }, [zipcode]);

    let getCurrentLocation = async () => {
        setShowLoader(true);
        let location = await Location.getCurrentPositionAsync({});
        let address = await Location.reverseGeocodeAsync({ latitude: location.coords.latitude, longitude: location.coords.longitude });

        let addressAux = {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            address: `${address[0].city}, ${address[0].region}`,
            zipcode: address[0].postalCode
        }

        AsyncStorage.setItem('productAddress', JSON.stringify(addressAux)).then(() => {
            setShowLoader(false);
            navigation.goBack();
        }).catch((error) => {
            setShowLoader(false);
            Alert.alert(
                'Eror',
                'Ha ocurrido un error al obtener la ubicación',
                [{ text: 'OK' }],
                { cancelable: false }
            );
        });
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <ImageBackground
                style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center' }}
                source={require('../../assets/map.png')}
            >
                <TouchableOpacity
                    style={{ borderRadius: 20, marginTop: 20 }}
                    onPress={() => { getCurrentLocation() }}
                >
                    <View style={{ width: 0.7 * screenWidth, height: 40, backgroundColor: colors.PURPLE, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Obtener la ubicación actual</Text>
                    </View>
                </TouchableOpacity>
                <Text style={{ fontSize: 14, color: colors.TEXT, fontWeight: 'bold', marginTop: 10 }}>
                    O
                </Text>
                <TextInput
                    style={{ backgroundColor: 'white', width: 0.7 * screenWidth, height: 40, fontSize: 14, color: colors.TEXT, fontWeight: 'bold', marginTop: 10, borderRadius: 4, borderWidth: 1, borderColor: colors.PURPLE }}
                    textAlign={'center'}
                    keyboardType={'numeric'}
                    maxLength={6}
                    onChangeText={zipcode => setZipcode(zipcode)}
                >

                </TextInput>
            </ImageBackground>
            <Loader loading={showLoader} />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
