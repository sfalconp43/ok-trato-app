import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TouchableOpacity, TextInput, Image, Modal, ActivityIndicator, FlatList, ImageBackground, Alert, KeyboardAvoidingView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Dimensions } from 'react-native';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import DraggableFlatList from "react-native-draggable-flatlist";
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';
import Toast from 'react-native-tiny-toast';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);

export default function PostScreen1() {
    const navigation = useNavigation();

    // Hooks Para la vista de post
    const [title, setTitle] = useState('');
    const [captures, setCaptures] = useState([{ key: '0', uri: 'https://reactnative.dev/img/tiny_logo.png' }]);
    const [imageSelected, setImageSelected] = useState(0);

    //Hooks para la camara
    const [cameraPermission, setCameraPermission] = useState(false);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [flashMode, setFlashModes] = useState(Camera.Constants.FlashMode.off);
    const [cameraVisible, setCameraVisible] = useState(false);

    // Hooks para el image Picker
    const [imagePickerCameraRollPermission, setImagePickerCameraRollPermission] = useState(false);

    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setCameraPermission(status === 'granted');
        })();
        (async () => {
            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            setImagePickerCameraRollPermission(status === 'granted');

        })();
        onLoad();
    }, []);

    let onLoad = () => {
        navigation.addListener('focus', () => {
            getStorageRoll();
            getProductTitle();
        })
    };

    let getProductTitle = async () => {
        const productTitle = await AsyncStorage.getItem('productTitle');

        if (productTitle != null) {
            setTitle(productTitle);
        }
    }

    let getStorageRoll = async () => {
        const productImages = await AsyncStorage.getItem('productImages');

        if (productImages != null) {
            setCaptures(JSON.parse(productImages));
        } else {
            setCaptures([{ key: '0', uri: 'https://reactnative.dev/img/tiny_logo.png' }]);
        }
    }

    let takePicture = async () => {

        if (captures.length < 14) {

            let photo = await this.camera.takePictureAsync({
                quality: 0.3,//calidad de la foto
            });

            if (captures[0].uri == 'https://reactnative.dev/img/tiny_logo.png') {
                setCaptures([{
                    key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), // Se genera un key aleatorio
                    uri: photo.uri
                }]);
            } else {
                setCaptures(captures.concat({
                    key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), // Se genera un key aleatorio
                    uri: photo.uri
                }));
            }
        } else {
            Alert.alert(
                'Máximo de imágenes alcanzado',
                'El máximo de 14 imágenes por producto ha sido alcanzado. Por favor, elimine las imágenes que considere no importantes para poder agregar nuevas imágenes.',
                [{ text: 'OK' }],
                { cancelable: false }
            );
        }
    }

    let processCameraImages = async () => {
        await AsyncStorage.setItem('productImages', JSON.stringify(captures));
        setCameraVisible(false)
    }

    let deleteImageSelected = () => {
        let newCaptures = captures.filter((item, index) => { if (index != imageSelected) return item });
        if (newCaptures.length > 0) {
            setImageSelected(0)
            setCaptures(newCaptures);
        } else {
            setCaptures([{ key: '0', uri: 'https://reactnative.dev/img/tiny_logo.png' }])
        }

    }

    let saveStepOne = async () => {
        const regularExpression = /[A-Za-z0-9]{2,40}/;

        if (!regularExpression.test(title) && captures[0].uri == 'https://reactnative.dev/img/tiny_logo.png') {
            Toast.show('El nombre del producto no puede esta vacío, debe contener dos o más caracteres validos. Además, debe agregar al menos una imagen al producto.', { duration: 4000 });
        } else {
            if (!regularExpression.test(title)) {
                Toast.show('El nombre del producto no puede esta vacío, debe contener dos o más caracteres validos.', { duration: 3000 });
            }

            if (captures[0].uri == 'https://reactnative.dev/img/tiny_logo.png') {
                Toast.show('Se debe agregar al menos una imagen al producto.', { duration: 3000 });
            }

            if (regularExpression.test(title) && captures[0].uri != 'https://reactnative.dev/img/tiny_logo.png') {
                try {
                    await AsyncStorage.setItem('productTitle', title);
                    await AsyncStorage.setItem('productImages', JSON.stringify(captures));
                } catch (error) {
                    console.log(error);
                }

                navigation.navigate('Post2');
            }
        }
    }

    // Funcion que rederiza el ImagePicker y devuelve la seleccion del usuario
    let renderImagePicker = async () => {
        if (captures.length < 14) {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                // aspect: [4, 3],
                quality: 0.3
            });

            if (result.cancelled == true) {
                console.log('Se cancelo la seleccion');
            } else {
                if (captures[0].uri == 'https://reactnative.dev/img/tiny_logo.png') {
                    setCaptures([{
                        key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), // Se genera un key aleatorio
                        uri: result.uri
                    }]);
                } else {
                    setCaptures(captures.concat({
                        key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), // Se genera un key aleatorio
                        uri: result.uri
                    }));
                }
            }
        } else {
            Alert.alert(
                'Máximo de imágenes alcanzado',
                'El máximo de 14 imágenes por producto ha sido alcanzado. Por favor, elimine las imágenes que considere no importantes para poder agregar nuevas imágenes.',
                [{ text: 'OK' }],
                { cancelable: false }
            );
        }
    }

    // Funcion que renderiza elementos del Drag and Drop
    let renderItem = ({ item, index, drag, isActive }) => {
        if (item.key === captures[0].key) {

            return (
                <TouchableOpacity
                    style={{
                        height: 80,
                        width: 80,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 8,
                        marginVertical: 10,
                        marginHorizontal: 5
                    }}
                    onLongPress={drag}
                    onPress={() => { setImageSelected(index) }}
                >
                    <ImageBackground
                        style={{ width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}
                        imageStyle={{ borderRadius: 8 }}
                        // source={{ uri: item.uri }}
                        source={item.uri != 'https://reactnative.dev/img/tiny_logo.png' ? { uri: item.uri } : require('../../assets/noPhoto.png')}
                    >
                        <View style={{ width: 80, height: 80, borderRadius: 8, backgroundColor: 'rgba(0,0,0, 0.4)', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>
                                PORTADA
                    </Text>
                        </View>

                    </ImageBackground>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity
                    style={{
                        height: 80,
                        width: 80,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 8,
                        marginVertical: 10,
                        marginHorizontal: 5
                    }}
                    onLongPress={drag}
                    // onPressOut={moveEnd}
                    onPress={() => { setImageSelected(index) }}
                >
                    <Image
                        style={{ width: 80, height: 80, borderRadius: 8 }}
                        // source={{ uri: item.uri }}
                        source={item.uri != 'https://reactnative.dev/img/tiny_logo.png' ? { uri: item.uri } : require('../../assets/noPhoto.png')}

                        resizeMode={'cover'}
                    />
                </TouchableOpacity>
            )
        }
    }

    // Funcion que renderiza los elemento del carrete de imagenes de la camara
    let renderCametaItem = (item) => {
        if (item.key === captures[0].key) {
            return (

                <ImageBackground
                    style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginHorizontal: 5, marginVertical: 10 }}
                    imageStyle={{ borderRadius: 8 }}
                    // source={{ uri: item.uri }}
                    source={item.uri != 'https://reactnative.dev/img/tiny_logo.png' ? { uri: item.uri } : require('../../assets/noPhoto.png')}
                >
                    <View style={{ width: 60, height: 60, borderRadius: 8, backgroundColor: 'rgba(255,255,255, 0.2)', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>
                            COVER
                        </Text>
                    </View>

                </ImageBackground>


            );
        } else {
            return (
                <Image
                    style={{ width: 60, height: 60, borderRadius: 8, marginHorizontal: 5, marginVertical: 10 }}
                    // source={{ uri: item.uri }}
                    source={item.uri != 'https://reactnative.dev/img/tiny_logo.png' ? { uri: item.uri } : require('../../assets/noPhoto.png')}
                />
            );
        }

    };

    // Funcion que renderiza la camara mediante un modal para asi ocultar la barra de pestañas
    let RenderCamera = () => {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={cameraVisible}
                onRequestClose={() => {
                    setCameraVisible(false);
                }}>
                <View style={{ flex: 1 }}>
                    <Camera
                        ref={ref => { this.camera = ref }}
                        style={{ flex: 1 }}
                        type={type}
                        ratio={'16:9'}
                    >
                        <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row', justifyContent: 'flex-start', height: 50, alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => { processCameraImages() }}
                            >
                                <View style={{ height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <MaterialIcons name="arrow-back" size={30} color="white" />
                                </View>
                            </TouchableOpacity>
                            <Text></Text>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>Tomar fotografías del producto</Text>
                        </View>
                        <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row', justifyContent: 'space-around' }}>
                            <View style={{ backgroundColor: 'transparent', height: 180, width: screenWidth, alignSelf: 'flex-end' }}>
                                <View style={{ backgroundColor: 'rgba(0,0,0,0.7)', height: 80 }}>
                                    <FlatList
                                        horizontal={true}
                                        data={captures}
                                        renderItem={({ item }) => renderCametaItem(item)}
                                        keyExtractor={item => item.key}
                                    />
                                </View>
                                <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: 100, flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <View style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}>
                                        <View style={{ backgroundColor: 'transparent', height: 100, width: 100 }}></View>
                                    </View>
                                    <TouchableOpacity
                                        style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}
                                        onPress={async () => {
                                            if (this.camera) {
                                                takePicture();
                                            }
                                        }}>
                                        <View style={{ backgroundColor: 'transparent', height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                style={{ width: 60, height: 60 }}
                                                source={require('../../assets/images/cameraIcon.png')}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}
                                        onPress={async () => {
                                            setCameraVisible(false);
                                        }}>
                                        <View style={{ backgroundColor: 'transparent', height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                style={{ width: 40, height: 40 }}
                                                source={require('../../assets/images/checkIcon.png')}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </Camera>
                </View>
            </Modal>
        );
    }

    if (cameraPermission === null || imagePickerCameraRollPermission === null) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color="#0000ff" />
                <Text>Revisión de los permisos de la cámara y galería</Text>
            </View>
        )
    }
    if (cameraPermission === null || imagePickerCameraRollPermission === false) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>No hay acceso a la cámara y/o galería</Text>
            </View>
        )
    }

    return (
        // <KeyboardAvoidingView style={{flex:1}}>
        <View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'flex-start' }}>
            <View style={{ flex: 1, backgroundColor: colors.GRAY, width: screenWidth, justifyContent: 'space-around', alignItems: 'center' }}>
                <ImageBackground
                    style={{ width: screenWidth, height: '140%', justifyContent: 'flex-start', alignItems: 'flex-end' }}
                    source={captures[imageSelected].uri != 'https://reactnative.dev/img/tiny_logo.png' ? { uri: captures[imageSelected].uri } : require('../../assets/noPhoto.png')}
                    resizeMode={'cover'}
                >

                    {captures[imageSelected].uri != 'https://reactnative.dev/img/tiny_logo.png' &&
                        <TouchableOpacity style={{ borderRadius: 20, elevation: 2, marginTop: 40, marginRight: 5 }}
                            onPress={() => {
                                // setCameraVisible(true);
                                deleteImageSelected();
                            }}
                        >
                            <View style={{ backgroundColor: colors.PURPLE, width: 40, height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <MaterialIcons name="delete" size={30} color="white" />
                            </View>
                        </TouchableOpacity>
                    }

                </ImageBackground>
            </View>
            <View style={{ height: 100, backgroundColor: 'rgba(67, 0, 114, 0.36)', width: screenWidth, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flex: 1 }}>
                    <DraggableFlatList
                        data={captures}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => item.key}
                        onDragEnd={({ data }) => setCaptures(data)}
                        horizontal={true}
                    />
                </View>
            </View>
            <View style={{ flex: 0.5, backgroundColor: 'white', width: screenWidth, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableOpacity style={{ borderRadius: 4, elevation: 2 }}
                    onPress={() => {
                        setCameraVisible(true);
                    }}
                >
                    <View style={{ backgroundColor: colors.PURPLE, width: 70, height: 70, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                        <MaterialIcons name="add-a-photo" size={30} color="white" />
                        <Text style={{ fontSize: 9, color: 'white', marginTop: 5 }}>
                            TOMAR FOTO
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ borderRadius: 4, elevation: 2 }}
                    onPress={async () => { renderImagePicker() }}
                >
                    <View style={{ backgroundColor: colors.PURPLE, width: 70, height: 70, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                        <MaterialCommunityIcons name="image-plus" size={30} color="white" />
                        <Text style={{ fontSize: 9, color: 'white', marginTop: 5 }}>
                            AGREGAR
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 0.5, backgroundColor: 'white', width: screenWidth, justifyContent: 'center' }}>
                <Text style={{ marginLeft: 0.05 * screenWidth, fontSize: 14, color: '#757575', fontWeight: 'bold' }}>
                    Nombre del producto
                </Text>
                <TextInput
                    style={{ height: 32, marginHorizontal: 0.05 * screenWidth, backgroundColor: colors.GRAY, borderRadius: 4, paddingLeft: 10, color: colors.TEXT }}
                    onChangeText={title => setTitle(title)}
                    value={title}
                    maxLength={40}
                />
            </View>

            <View style={{ flex: 1, backgroundColor: 'white', width: screenWidth, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 20 }}>
                <Image
                    style={{ width: 300 }}
                    source={require('../../assets/wizard01.png')}
                    resizeMode={'contain'}
                />
                <TouchableOpacity
                    style={{ justifyContent: 'center', height: 30, width: 0.9 * screenWidth, backgroundColor: colors.PURPLE_BTN, borderRadius: 15, elevation: 2 }}
                    onPress={() => {
                        saveStepOne();
                    }}
                >
                    <Text style={{ fontSize: 14, color: 'white', alignSelf: 'center' }}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
            {RenderCamera()}
        </View>
        // </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
