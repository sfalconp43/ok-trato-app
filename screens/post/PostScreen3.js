import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Slider, TextInput, AsyncStorage, Dimensions, Image, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { CheckBox } from 'react-native-elements'
import Toast from 'react-native-tiny-toast';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);

export default function PostScreen3() {
    const navigation = useNavigation();

    const [price, setPrice] = useState('');
    const [firmOnPrice, setFirmOnPrice] = useState(false);



    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        navigation.addListener('focus', () => {
            (async () => {
                let productPriceStorage = await AsyncStorage.getItem('productPrice');
                let productFirmOnPriceStorage = await AsyncStorage.getItem('productFirmOnPrice');
                setPrice(productPriceStorage);
                if (productFirmOnPriceStorage != null) {
                    setFirmOnPrice(JSON.parse(productFirmOnPriceStorage));
                }
            })();
        })

    }, []);

    let saveStepThree = async () => {
        const regularExpression = /^\d*(\.\d{1})?\d{0,1}$/;

        // console.log()

        if (!regularExpression.test(price)) {
            Toast.show('El precio no es valido, debe contener una parte entera numérica y solo dos decimales. La parte decimal debe ir denotada por un punto (.).', { duration: 4000 });
        } else {
            try {
                await AsyncStorage.setItem('productPrice', price);
                await AsyncStorage.setItem('productFirmOnPrice', firmOnPrice.toString());
            } catch (error) {
                console.log(error);
            }
            navigation.navigate('Post4');
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ backgroundColor: 'white', width: '100%', height: 130, justifyContent: 'flex-start', paddingHorizontal: 10, marginTop: 20 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#757575', marginBottom: 5 }}>
                    Precio del articulo
                </Text>
                <View style={{ flexDirection: 'row', height: 70, width: 200, backgroundColor: colors.GRAY, borderRadius: 10, color: colors.TEXT, borderWidth: 1, borderColor: colors.PURPLE, alignSelf: 'center', fontSize: 30, marginTop: 30 }}>
                    <Text style={{ height: 68, width: 33, backgroundColor: 'transparent', fontSize: 30, textAlign: 'center', textAlignVertical: 'center', color: colors.PURPLE, fontWeight: 'bold', borderRightWidth: 1, borderRightColor: colors.PURPLE }}>$</Text>
                    <TextInput
                        style={{ height: 68, width: 165, backgroundColor: colors.GRAY, borderRadius: 10, color: colors.TEXT, alignSelf: 'center', fontSize: 30 }}
                        onChangeText={price => setPrice(price)}
                        value={price}
                        textAlign={'center'}
                        keyboardType={'numeric'}
                        maxLength={9}
                    />
                </View>
            </View>
            <CheckBox
                containerStyle={{ marginTop: 20, backgroundColor: 'white', width: '100%', marginLeft: 0, marginRight: 0, borderColor: colors.GRAY, borderLeftWidth: 0, borderRightWidth: 0 }}
                title='¿Firme en el precio?'
                checked={firmOnPrice}
                onPress={() => { setFirmOnPrice(!firmOnPrice) }}
                checkedColor={colors.PURPLE}
            />
            <View style={{ flex: 1, backgroundColor: 'white', width: screenWidth, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 20 }}>
                <Image
                    style={{ width: 300 }}
                    source={require('../../assets/wizard03.png')}
                    resizeMode={'contain'}
                />
                <TouchableOpacity
                    style={{ justifyContent: 'center', height: 30, width: 0.9 * screenWidth, backgroundColor: colors.PURPLE_BTN, borderRadius: 15, elevation: 2 }}
                    onPress={() => {
                        saveStepThree();
                    }}
                >
                    <Text style={{ fontSize: 14, color: 'white', alignSelf: 'center' }}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
