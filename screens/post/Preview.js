import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, AsyncStorage, Dimensions, Image, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import Loader from '../loader/loader';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);
const config = require('../../config/config');

export default function PostScreen4() {
    const navigation = useNavigation();

    const [address, setAddress] = useState({ address: '' });
    const [title, setTitle] = useState(null);
    const [price, setPrice] = useState(null);
    const [description, setDescription] = useState(null);
    const [user, setUser] = useState(null);
    const [productImages, setProductImages] = useState(null);
    const [category, setCategory] = useState(null);
    const [firmOnPrice, setFirmOnPrice] = useState(null);
    const [condition, setCondition] = useState(null);
    const [showLoader, setShowLoader] = useState(false);

    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        navigation.addListener('focus', () => {
            (async () => {
                let addressStorage = await AsyncStorage.getItem('productAddress');
                let priceStorage = await AsyncStorage.getItem('productPrice');
                let titleStorage = await AsyncStorage.getItem('productTitle');
                let descriptionStorage = await AsyncStorage.getItem('productDescription');
                let userData = await AsyncStorage.getItem('userData');
                let productImagesStorage = await AsyncStorage.getItem('productImages');
                let productCategoryStorage = await AsyncStorage.getItem('productCategory');
                let productFirmOnPriceStorage = await AsyncStorage.getItem('productFirmOnPrice');
                let productConditionStorage = await AsyncStorage.getItem('productCondition');

                setAddress(JSON.parse(addressStorage));
                setTitle(titleStorage);
                setPrice(priceStorage);
                setDescription(descriptionStorage);
                setUser(JSON.parse(userData));
                setProductImages(JSON.parse(productImagesStorage));
                setCategory(JSON.parse(productCategoryStorage));
                setCategory(JSON.parse(productCategoryStorage));
                setFirmOnPrice(JSON.parse(productFirmOnPriceStorage));
                setCondition(JSON.parse(productConditionStorage))
            })();
        })

    }, []);

    let cleanStorage = async () => {
        await AsyncStorage.multiRemove(['productAddress','productPrice','productTitle','productDescription','productImages','productCategory','productFirmOnPrice','productCondition']);
        navigation.navigate('Offers');
    }

    let postproduct = async () => {
        if (user == null) {
            Alert.alert(
                'Sea parte de OK Trato',
                'Debe iniciar sesion para poder publicar productos',
                [{ text: 'OK', onPress: () => navigation.navigate('User') }],
                { cancelable: false }
            );
        } else {
            setShowLoader(true);

            var formData = new FormData();
            formData.append('iduser', user._id);
            formData.append('idcategory', category._id);
            formData.append('title', title);
            formData.append('description', description);
            formData.append('price', price);
            formData.append('flexible', firmOnPrice);
            formData.append('lat', address.latitude);
            formData.append('lng', address.longitude);
            formData.append('condition', condition);
            formData.append('productaddress', address.address);
            formData.append('zipcode', address.zipcode);

            productImages.forEach(element => {
                let filename = element.uri.split("/").pop();
                formData.append('productimages', { uri: element.uri, name: filename, type: 'image/jpg' })
            });

            axios({
                method: 'post',
                url: `${config.BASE_URL_API}api/v1/insert-product`,
                data: formData,
                headers: { 'Content-Type': 'multipart/form-data', 'token': user.token }
            })
                .then(function (response) {
                    //handle success
                    setShowLoader(false);
                    Alert.alert(
                        'Publicación exitosa',
                        'El producto se ha publicado de forma exitosa',
                        [{ text: 'OK', onPress: () => cleanStorage() }],
                        { cancelable: false }
                    );
                })
                .catch(function (response) {
                    //handle error
                    setShowLoader(false);
                    Alert.alert(
                        'Error al publicar',
                        'Ha ocurrido un error al poblicar el producto',
                        [{ text: 'OK' }],
                        { cancelable: false }
                    );
                });
        }

    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Image
                style={{ width: '100%', height: 200 }}
                source={{ uri: 'https://facebook.github.io/react-native/img/tiny_logo.png' }}
                source={productImages != null ? { uri: productImages[0].uri } : require('../../assets/noPhoto.png')}
                resizeMode={'cover'}
            />
            <View style={{ height: 40, width: 100, backgroundColor: colors.PURPLE, justifyContent: 'center', alignItems: 'center', borderRadius: 20, position: 'absolute', top: 150, left: 5 }}>
                <TextInput style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{`$${price}`}</TextInput>
            </View>
            <View style={{ padding: 10, borderBottomWidth: 1, borderBottomColor: colors.GRAY }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{title}</Text>
            </View>
            <View style={{ padding: 10, borderBottomWidth: 1, borderBottomColor: colors.GRAY }}>
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{address.address}</Text>
            </View>
            <View style={{ padding: 10, borderBottomWidth: 1, borderBottomColor: colors.GRAY }}>
                <Text style={{ fontSize: 15 }}>{description}</Text>
            </View>
            <View style={{ flex: 1, backgroundColor: 'white', width: screenWidth, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 20 }}>

                <TouchableOpacity
                    style={{ justifyContent: 'center', height: 30, width: 0.9 * screenWidth, backgroundColor: colors.PURPLE_BTN, borderRadius: 15, elevation: 2 }}
                    onPress={() => {
                        postproduct();
                    }}
                >
                    <Text style={{ fontSize: 14, color: 'white', alignSelf: 'center' }}>PUBLICAR</Text>
                </TouchableOpacity>
            </View>
            <Loader loading={showLoader} />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
