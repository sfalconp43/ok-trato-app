// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default function CameraScreen() {
//     return (
//         <View style={styles.container}>
//             <Text>Camera Screen</Text>
//         </View>
//     );
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
// });

import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Camera } from 'expo-camera';

export default function App() {
    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [flashMode, setFlashModes] = useState(Camera.Constants.FlashMode.off);
    const [captures, setCaptures] = useState([]);
    const [capturing, setCapturing] = useState(null);
    const [snap, setSnap] = useState(0);
    const [maxSnap, setMaxSnap] = useState(14);

    // Aca se estructura el useEffect de esta manera para evitar que el asyn comun retorne una promesa y genere un warning
    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return (
            <View style={{ flex: 1 }}>
                <Text>Revisión de los permisos de la cámara</Text>
            </View>
        )
    }
    if (hasPermission === false) {
        return (
            <View style={{ flex: 1 }}>
                <Text>No hay acceso a la cámara</Text>
            </View>
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <Camera
                ref={ref => { this.camera = ref }}
                style={{ flex: 1 }}
                type={type}
                ratio={'16:9'}
            >
                <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity
                        style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}
                        onPress={async () => {
                            if (this.camera) {
                                let photo = await this.camera.takePictureAsync();
                                console.log(photo);
                            }
                        }}>
                        <View style={{ backgroundColor: 'blue', height: 100, width: 100 }}></View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}
                        onPress={async () => {
                            if (this.camera) {
                                let photo = await this.camera.takePictureAsync();
                                console.log(photo);
                            }
                        }}>
                        <View style={{ backgroundColor: 'transparent', height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                style={{ width: 60, height: 60 }}
                                source={require('../../assets/images/cameraIcon.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flex: 0.1, alignSelf: 'flex-end', alignItems: 'center' }}
                        onPress={async () => {
                            if (this.camera) {
                                let photo = await this.camera.takePictureAsync();
                                console.log(photo);
                            }
                        }}>
                        <View style={{ backgroundColor: 'transparent', height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                style={{ width: 40, height: 40 }}
                                source={require('../../assets/images/checkIcon.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </Camera>
        </View>
    );
}
