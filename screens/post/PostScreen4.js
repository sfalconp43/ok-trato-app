import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Dimensions, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { MaterialIcons } from '@expo/vector-icons';
import Toast from 'react-native-tiny-toast';

const colors = require('../../assets/styles/colors');
const screenWidth = Math.round(Dimensions.get('window').width);

export default function PostScreen4() {
    const navigation = useNavigation();

    const [address, setAddress] = useState(null);

    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {
        navigation.addListener('focus', () => {
            (async () => {
                let addressStorage = await AsyncStorage.getItem('productAddress');
                setAddress(JSON.parse(addressStorage));
            })();
        })

    }, []);

    let saveStepFour = async () => {
        if (address == null) {
            Toast.show('Se debe asignar una ubicación al producto.', { duration: 3000 });
        } else {
            navigation.navigate('Preview');
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ width: '100%', backgroundColor: 'transparent', justifyContent: 'center', borderBottomWidth: 1, borderColor: colors.GRAY, paddingVertical: 10 }}>
                <TouchableOpacity
                    style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 10, paddingRight: 3 }}
                    onPress={() => {
                        navigation.navigate('Location');
                    }}
                >
                    <Text style={{ color: colors.TEXT, fontSize: 14, fontWeight: 'bold' }}>
                        Ubicacíon: {address ? address.address : 'No se ha asignado aun...'}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>Asignar</Text>
                        <MaterialIcons name="keyboard-arrow-right" size={20} color={colors.PURPLE} />
                    </View>
                    
                </TouchableOpacity>
            </View>
            <View style={{ width: '100%', backgroundColor: 'transparent', justifyContent: 'center', borderBottomWidth: 1, borderColor: colors.GRAY, paddingVertical: 10 }}>
                <TouchableOpacity
                    style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 10, paddingRight: 3 }}
                    onPress={() => {
                        navigation.navigate('Categories');
                    }}
                >
                    <Text style={{ color: colors.TEXT, fontSize: 14, fontWeight: 'bold' }}>
                        Envío: {'No se ha configurado aun...'}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>Configurar</Text>
                        <MaterialIcons name="keyboard-arrow-right" size={20} color={colors.PURPLE} />
                    </View>
                    
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, backgroundColor: 'white', width: screenWidth, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 20 }}>
                <Image
                    style={{ width: 300 }}
                    source={require('../../assets/wizard04.png')}
                    resizeMode={'contain'}
                />
                <TouchableOpacity
                    style={{ justifyContent: 'center', height: 30, width: 0.9 * screenWidth, backgroundColor: colors.PURPLE_BTN, borderRadius: 15, elevation: 2 }}
                    onPress={() => {
                        saveStepFour();
                    }}
                >
                    <Text style={{ fontSize: 14, color: 'white', alignSelf: 'center' }}>SIGUIENTE</Text>
                </TouchableOpacity>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
