import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import Loader from '../loader/loader';

const colors = require('../../assets/styles/colors');
const config = require('../../config/config');

export default function PostScreen() {
    const navigation = useNavigation();

    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState(null);
    const [showLoader, setShowLoader] = useState(false);

    navigation.setOptions({
        headerStyle: {
            backgroundColor: colors.PRIMARY,
            elevation: 0,//sin border bottom     
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    useEffect(() => {

        (async () => {
            categoryStorage = await AsyncStorage.getItem('productCategory');
            setCategory(JSON.parse(categoryStorage));
        })();

        setShowLoader(true);

        axios.get(`${config.BASE_URL_API}api/v1/get-categories/`)
            .then((response) => {
                setCategories(response.data.categories);
                setShowLoader(false);
            })
            .catch((error) => {
                setShowLoader(false);
                Alert.alert(
                    'Eror',
                    'Ha ocurrido un error al obtener las categorías de los productos',
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
            })
    }, []);

    let selectCategory = async (category) => {
        let categoryAux = {
            _id: category._id,
            name: category.name,
        }
        try {
            await AsyncStorage.setItem('productCategory', JSON.stringify(categoryAux));
        } catch (error) {
            console.log(error);
        }
        navigation.goBack();
    }

    let renderCategory = (item) => {

        if (category) {
            if (category._id == item._id) {
                return (
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                        onPress={() => { selectCategory(item) }}
                    >
                        <View style={{ backgroundColor: colors.PURPLE, height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', marginTop: 10, borderRadius: 20 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                );
            } else {
                return (
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                        onPress={() => { selectCategory(item) }}
                    >
                        <View style={{ backgroundColor: 'white', height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: colors.PURPLE, marginTop: 10, marginBottom: 1, borderRadius: 20 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        } else {
            return (
                <TouchableOpacity
                    style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}
                    onPress={() => { selectCategory(item) }}
                >
                    <View style={{ backgroundColor: 'white', height: 40, width: '90%', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: colors.PURPLE, marginTop: 10, marginBottom: 1, borderRadius: 20 }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.PURPLE }}>{item.name}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <FlatList
                data={categories}
                renderItem={({ item }) => renderCategory(item)}
                keyExtractor={item => item._id}
            />
            <Loader loading={showLoader} />
        </SafeAreaView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
