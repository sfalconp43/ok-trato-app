import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import colors from '../assets/styles/colors';
import TimeAgo from 'react-native-timeago';
import { MaterialCommunityIcons } from '@expo/vector-icons';
export default function Message ({ message, side, date, seen }) {

const isLeftSide = side === 'left'
//Definir estilos de acuerdo a la ubicacion del mensaje
const containerStyles = isLeftSide ? styles.container : flattenedStyles.container
const dateStyles = isLeftSide ? flattenedStyles.timeLeft : flattenedStyles.timeRight
const textContainerStyles = isLeftSide ? styles.containerChat: flattenedStyles.textContainer

return (
    <View>
        <View style={containerStyles}>
            <View style={textContainerStyles}>
                <Text style={styles.text}>
                    {message}
                </Text>
                {
                    !isLeftSide ?
                            <View style={styles.imageContainer}>
                            { seen ? (
                            <View style={styles.checkAll}>
                                 <MaterialCommunityIcons name="check-all" color="white" size={12}/>
                            </View>
                            ): 
                            <View style={styles.check}>
                                <MaterialCommunityIcons name="check" color="#877979" size={12}/> 
                            </View>
                            }
                            </View>   
                    :null
                }
            </View>
        </View>
       
        <View style={dateStyles}>
            <TimeAgo style={styles.messagedate} time={date} interval={20000}/>
        </View> 
    </View>
  )
}

const styles = StyleSheet.create({
    containerChat:{
        elevation:2,
        backgroundColor: colors.CHAT_RES,
        flexDirection:'row', 
        justifyContent:'space-between',
        alignItems:'center',
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 0,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,
    },
    rightContainerChat: {
        backgroundColor: colors.CHAT,
        borderBottomEndRadius: 0,
        borderBottomStartRadius: 10,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,  
    },
    text: {
        margin:10,  
        flexShrink: 1,
        fontSize: 14
    },
    dateMessage:{
        paddingVertical: 3,
        paddingHorizontal: 18,
    },
    dateRight:{
        alignItems:'flex-end',
    },
    dateLeft:{
        alignItems:'flex-start',
    },
    container: {
        marginTop:10,
        marginVertical: 3,
        marginHorizontal: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    rightContainer: {
        justifyContent: 'flex-end'
    },
    messagedate:{
		fontSize:10,
		color:'#877979'
    },
    check:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
        height:20,
        width: 20,
        borderRadius: 10
    },
    checkAll:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#00C853',
        height:20,
        width: 20,
        borderRadius: 10
    },
    imageContainer: {
        marginRight:8,
        justifyContent:'center',
        alignItems:'center',
    },
})
  
const flattenedStyles = {
    container: StyleSheet.flatten([styles.container, styles.rightContainer]),
    timeRight: StyleSheet.flatten([styles.dateMessage, styles.dateRight]),
    textContainer: StyleSheet.flatten([styles.containerChat, styles.rightContainerChat]),
    timeLeft: StyleSheet.flatten([styles.dateMessage, styles.dateLeft]),
}