import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from "react-native";
import posed from "react-native-pose";
import colors from '../assets/styles/colors';
import Icon from './Icon';

const windowWidth = Dimensions.get("window").width;
const tabWidth = windowWidth / 5;

//Mover SpotLight de posición de acuerdo al index 
const SpotLight = posed.View({
    0: { x: 0 },
    1: { x: tabWidth },
    2: { x: tabWidth * 2 },
    3: { x: tabWidth * 3 },
    4: { x: tabWidth * 4 }
});

//Tamano de icono cuando está activo o inactivo
const Scaler = posed.View({
    active: { scale: 1.55 },
    inactive: { scale: 1.30 }
});

const TabBar = ({ state, descriptors, navigation }) => {
    
    return (
        <View style={S.container}>
            <View style={StyleSheet.absoluteFillObject}>
                <SpotLight style={S.spotLight} pose={state.index.toString()}>
                    <View style={S.spotLightInner} />
                </SpotLight>
            </View>
            {state.routes.map((route, routeIndex) => {
                const isRouteActive = routeIndex === state.index;
                const activeTintColor = colors.PRIMARY;
                const inactiveTintColor = '#929292';
                const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

                const { options } = descriptors[route.key];
                const label =
                options.tabBarLabel !== undefined
                    ? options.tabBarLabel
                    : options.title !== undefined
                    ? options.title
                    : route.name;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                    });
    
                    if (!isRouteActive && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };
    
                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };
    
                return (
                    <TouchableOpacity
                    key={routeIndex}
                    style={S.tabButton}
                    onPress={onPress}
                    onLongPress={onLongPress}
                    accessibilityLabel={options.tabBarAccessibilityLabel}
                    >
                        {isRouteActive ?
                        <Scaler style={S.scaler} pose={isRouteActive ? "active" : "inactive"}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Icon name={label} color={tintColor} />
                            </View>
                        </Scaler>
                        : 
                        <View style={S.scaler}>
                            <View style={S.emptyLigth}>
                                <Scaler style={S.scaler} pose={isRouteActive ? "active" : "inactive"}>
                                    <Icon name={label} color={tintColor} />
                                </Scaler>
                            </View>
                        </View>
                        }

                    </TouchableOpacity>
                );
            })}
        </View>
      );
};

const S = StyleSheet.create({
    container: {
        flexDirection: "row",
        height: 52,
        elevation: 2,
        alignItems: "center",
        backgroundColor:"#EEEEEE"
    },
    tabButton: { flex: 1 },
    spotLight: {
        width: tabWidth,
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    spotLightInner: {
        width: 42,
        height: 42,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 21
    },
    emptyLigth: {
        flexDirection:'row', 
        alignItems:'center',
        justifyContent:'center',
        width: 42,
        height: 42,
        backgroundColor: '#d2d2d2',
        borderRadius: 21
    },
    scaler: { flex: 1, alignItems: "center", justifyContent: "center" }
});

export default TabBar;