import React from "react";
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';

const Icon = ({ name, color, style, ...props }) => {
    switch(name) {
        case "Home":
            return <MaterialIcons name="home" color={color} size={16}/>;
        case "Messages1":
            return <MaterialIcons name="chat" color={color} size={16}/>;
        case "Post":
            return <MaterialIcons name="add-a-photo" color={color} size={16}/>;
        case "Offers":
            return <MaterialCommunityIcons name="tag" color={color} size={16}/>;
        case "User":
            return <MaterialCommunityIcons name="account" color={color} size={16}/>;
        default:
            return <MaterialIcons name="home" color={color} size={16}/>;
    }
};

export default Icon;