import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import colors from '../assets/styles/colors';

export default function Error ({onRefresh}) {

return (
    <View style={styles.container}>
        <Text style={styles.message}>Por favor refreque la búsqueda. Tenemos problemas obteniendo nuevos productos</Text>
        <TouchableOpacity style={styles.button} onPress={onRefresh}> 
            <Text style={styles.buttonText}> Refrescar </Text>
        </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    message:{
        color: colors.TEXT,
        fontSize: 14,
        marginBottom:8,
        textAlign: 'center'
    },
    buttonText: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        padding:20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        height: 30,
        backgroundColor: colors.PURPLE_BTN,
        borderRadius: 16,
    },
})
  
