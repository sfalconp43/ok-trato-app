import React, {  useEffect, useReducer, useMemo } from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Ionicons } from '@expo/vector-icons';
// Importar Vistas
// Principales
import HomeScreen from './screens/home/HomeScreen';
import MessagesScreen from './screens/messages/MessagesScreen';
import PostScreen1 from './screens/post/PostScreen1';
import OffersScreen from './screens/offers/offers';
import AccountScreen from './screens/account/AccountScreen';
// Post
import PostScreen2 from './screens/post/PostScreen2';
import CategoriesScreen from './screens/post/Categories';
import PostScreen3 from './screens/post/PostScreen3';
import PostScreen4 from './screens/post/PostScreen4';
import LocationScreen from './screens/post/Location';
import PreviewScreen from './screens/post/Preview';
//Cuenta
import ProfileScreen from './screens/account/profile';
import SettingsScreen from './screens/account/settings';
import ConfirmEmailScreen from './screens/account/confirmEmail';
import ProfileImgScreen from './screens/account/profileImg';
import ConfirmPicScreen from './screens/account/confirmPic';
import ValidatePhoneScreen from './screens/account/validatePhone';
import ConfirmSmsScreen from './screens/account/confirmSmsCode';
//Data
import NamesScreen from './screens/account/data/names';
import SurnameScreen from './screens/account/data/lastNames';
import AddressScreen from './screens/account/data/address';
import PhoneScreen from './screens/account/data/phone';
// import PhoneScreen2 from './screens/account/data/phoneTest'
import PasswordScreen from './screens/account/data/pwd';
//Login - Register
import CheckEmailScreen from './screens/login/checkEmail';
import RegisterScreen from './screens/login/register';
import LoginScreen from './screens/login/login';
//Offers
import BoardsCreateScreen from './screens/offers/createBoard';
import BoardDetailScreen from './screens/offers/boardDetail';
import BoardEditScreen from './screens/offers/editBoard';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
import AuthContext from './screens/authContext'
import AuthLoading from './screens/account/authLoading';
import TabBar from './components/TabBar'
//home
import ProductDetailScreen from './screens/home/productDetail';
import ChatScreen from './screens/home/chat';
import UserDetailScreen from './screens/home/userDetail';
import SavetoBoardScreen from './screens/home/saveToBoard';
import BoardProductScreen from './screens/home/createBoardProduct';
import ReportScreen from './screens/home/report';
import SendReportScreen from './screens/home/sendReport';
import FilterCategoryScreen from './screens/home/filterCategory';
import FilterLocationScreen from './screens/home/filterLocation';
import FilterTextScreen from './screens/home/filterText';

export default function App() {
	const [state, dispatch] = useReducer(
		(prevState, action) => {
		switch (action.type) {
				case 'RESTORE_TOKEN':
					return {
						...prevState,
						userToken: action.token,
						isLoading: false,
					};
				case 'SIGN_IN':
					return {
						...prevState,
						isSignout: false,
						userToken: action.token,
					};
				case 'SIGN_OUT':
					return {
						...prevState,
						isSignout: true,
						userToken: null,
					};
		}
		},
		{
		isLoading: true,
		isSignout: false,
		userToken: null,
		}
	);
	useEffect(() => {
		// Obtener token del storage y navegar al lugar adecuado
		const bootstrapAsync = async () => {

		let userToken;
			try {
				userToken = await AsyncStorage.getItem('userToken');
			} catch (e) {
				// Restoring token failed
			}
			dispatch({ type: 'RESTORE_TOKEN', token: userToken });
		};
		bootstrapAsync();
	}, []);
	const authContext = useMemo(
		() => ({
			//Login
			signIn: async data => {
				await AsyncStorage.setItem('userToken', data);
				dispatch({ type: 'SIGN_IN', token: data });
			},
			//Salir
			signOut: () => dispatch({ type: 'SIGN_OUT' }),
			//Registro
			signUp: async data => {
				await AsyncStorage.setItem('userToken', data);
				dispatch({ type: 'SIGN_IN', token: data });
			},
		}),[]
	);
	//Loading mientras busca el token de usuario
	if (state.isLoading) {
		return <AuthLoading />;
	}


	const NavigationTabs = () => {
		return (
			<AuthContext.Provider value={authContext}>
			<Tab.Navigator tabBar={props => <TabBar {...props} />}>
				<Tab.Screen name="Home">
				{() => (
					<Stack.Navigator>
						<Stack.Screen name="Home1" component={HomeScreen} />
					</Stack.Navigator>
				)}
				</Tab.Screen>
				<Tab.Screen name="Messages1">
				{() => (
					<Stack.Navigator>
						<Stack.Screen name="Messages" component={MessagesScreen} options={{ title: 'Mensajes'}}/>	
					</Stack.Navigator>
				)}
				</Tab.Screen>
				<Tab.Screen name="Post">
				{() => (
					<Stack.Navigator>
						<Stack.Screen name="Post1" component={PostScreen1}  options={{ title: 'Publicar producto' }}/>
					</Stack.Navigator>
				)}
				</Tab.Screen>
				<Tab.Screen name="Offers">
				{() => (
					<Stack.Navigator>
						<Stack.Screen name="Offers1" component={OffersScreen} options={{ title: 'Ofertas'}}/>
					</Stack.Navigator>
				)}
				</Tab.Screen>
				<Tab.Screen name="User">
				{() => (
					<Stack.Navigator>
					{/*Account */}
					{state.userToken == null ? (
						<>
							<Stack.Screen name="Email" component={CheckEmailScreen} />
							<Stack.Screen name="Register" component={RegisterScreen} />
							<Stack.Screen name="Login" component={LoginScreen} />
						</>
						) : (
						<>
							<Stack.Screen name="Account" component={AccountScreen} />
							<Stack.Screen name="Profile" component={ProfileScreen} />
							<Stack.Screen name="Settings" component={SettingsScreen} />
							<Stack.Screen name="ConfirmEmail" component={ConfirmEmailScreen} />
							<Stack.Screen name="ValidatePhone" component={ValidatePhoneScreen} />
							<Stack.Screen name="ConfirmSms" component={ConfirmSmsScreen} />
							<Stack.Screen name="ProfileImg" component={ProfileImgScreen} />
							<Stack.Screen name="ConfirmImg" component={ConfirmPicScreen} />
							<Stack.Screen name="Names" component={NamesScreen} />
							<Stack.Screen name="Surname" component={SurnameScreen} />
							<Stack.Screen name="Address" component={AddressScreen} />
							<Stack.Screen name="Phone" component={PhoneScreen} />
							<Stack.Screen name="Password" component={PasswordScreen} />
						</>
						)
					}
					</Stack.Navigator>
				)}
				</Tab.Screen>
			</Tab.Navigator>
			</AuthContext.Provider>
		);
	}

	const RootStack = createStackNavigator();

	return (
		<NavigationContainer>
			<RootStack.Navigator>
				<RootStack.Screen name="NavigationTabs"  component={NavigationTabs} options={{headerShown: false}}/>
				{/*Home */}
				<RootStack.Screen name="ProductDetail" component={ProductDetailScreen} />
				<RootStack.Screen name="UserDetail" component={UserDetailScreen} />
				<RootStack.Screen name="SaveToBoard" component={SavetoBoardScreen} />
				<RootStack.Screen name="SaveBoardProduct" component={BoardProductScreen} />
				<RootStack.Screen name="Report" component={ReportScreen} />
				<RootStack.Screen name="SendReport" component={SendReportScreen} />
				<RootStack.Screen name="FilterCategory" component={FilterCategoryScreen} />
				<RootStack.Screen name="FilterLocation" component={FilterLocationScreen} />
				<RootStack.Screen name="FilterText" component={FilterTextScreen} />

				{/*Messages */}
				<RootStack.Screen name="Chat" component={ChatScreen} />

				{/*Post */}
				<RootStack.Screen name="Post2" component={PostScreen2}  options={{ title: 'Describe tu articulo' }}/>
				<RootStack.Screen name="Categories" component={CategoriesScreen}  options={{ title: 'Seleccione una categoría' }}/>
				<RootStack.Screen name="Post3" component={PostScreen3}  options={{ title: 'Fija el precio' }}/>
				<RootStack.Screen name="Post4" component={PostScreen4}  options={{ title: 'Ubicación y envío' }}/>
				<RootStack.Screen name="Location" component={LocationScreen}  options={{ title: 'Asignar ubicación' }}/>
				<RootStack.Screen name="Preview" component={PreviewScreen}  options={{ title: 'Publicar producto' }}/>

				{/*Offers */}
				<Stack.Screen name="BoardsCreate" component={BoardsCreateScreen}/>
				<Stack.Screen name="BoardsDetails" component={BoardDetailScreen}/>
				<Stack.Screen name="BoardsEdit" component={BoardEditScreen}/>
	
			</RootStack.Navigator>
		</NavigationContainer>
	);
}